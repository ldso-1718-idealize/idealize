drop table user;
drop table session;
drop table idea;
drop table idea_keyword;
drop table idea_share;
drop table idea_favorite;
drop table share_hash;
drop table team;
drop table team_member;
drop table team_invite;
drop table team_idea;
drop table activation;
drop table password_reset;
drop event session_delete_event;
drop event password_reset_delete_event;
drop event share_hash_delete_event;
drop event activation_delete_event;