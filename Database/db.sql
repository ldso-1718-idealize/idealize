create table user
(
	id serial,
	username varchar(40) not null unique,
	firstname varchar(40) not null,
	lastname varchar(40) not null,
	email varchar(40) not null unique,
	password varchar(256) not null,
	active boolean default false
);

create table activation
(
	id int not null,
	hash varchar(256) not null,
	creationdate timestamp
);

create table password_reset
(
	id int not null,
	hash varchar(256) not null,
	creationdate timestamp
);

create table session
(
	token varchar(256) not null,
	id int not null,
	creationdate timestamp
);

create table idea
(
	id serial,
	title varchar(30),
	idauthor int not null,
	synopsis varchar(500) not null,
	keyword varchar(20) not null,
	trends text not null,
	creationdate timestamp,
	country varchar(30)
);

create table idea_keyword
(
	ididea int not null,
	keyword varchar(500) not null
);

create table idea_share
(
	ididea int not null,
	iduser int not null
);

create table idea_favorite
(
	ididea int not null,
	iduser int not null,
	UNIQUE KEY (ididea, iduser)
);

create table share_hash
(
	hash varchar(256) not null,
	ididea int not null,
	creationdate timestamp
);

create table team
(
	id serial,
	idfounder int not null,
	name varchar(30)
);

create table team_member
(
	idteam int not null,
	idmember int not null,
	UNIQUE KEY (idteam, idmember)
);

create table team_invite
(
  id serial,
  idteam int not null,
  iduser int not null,
  accepted boolean default false,
  UNIQUE KEY (idteam, iduser)
);

create table team_idea
(
  idteam int not null,
  ididea int not null,
  UNIQUE KEY (idteam, ididea)
);

CREATE EVENT session_delete_event
    ON SCHEDULE EVERY 1 HOUR
    ON COMPLETION PRESERVE

    DO DELETE from session WHERE creationdate < NOW() - INTERVAL 5 HOUR;
	
CREATE EVENT password_reset_delete_event
    ON SCHEDULE EVERY 1 HOUR
    ON COMPLETION PRESERVE

    DO DELETE from password_reset WHERE creationdate < NOW() - INTERVAL 5 HOUR;

CREATE EVENT share_hash_delete_event
    ON SCHEDULE EVERY 1 HOUR
    ON COMPLETION PRESERVE

    DO DELETE from share_hash WHERE creationdate < NOW() - INTERVAL 5 HOUR;
	
delimiter |
	
CREATE EVENT activation_delete_event
    ON SCHEDULE EVERY 1 HOUR
    ON COMPLETION PRESERVE

    DO
      BEGIN
		DECLARE user_id INT;
		DECLARE cur CURSOR FOR SELECT id from activation WHERE creationdate < NOW() - INTERVAL 10 HOUR;
        
		OPEN cur;
		
		read_loop: LOOP
			FETCH cur INTO user_id;
			DELETE from user where id = user_id;
			DELETE from activation where id = user_id;
		END LOOP;

		CLOSE cur;
    END |
	
delimiter ;