create table user
(
	id serial,
	username varchar(40) not null unique,
	firstname varchar(40) not null,
	lastname varchar(40) not null,
	email varchar(40) not null,
	password varchar(256) not null,
	active boolean default false
);

create table session
(
	token varchar(256) not null,
	id int not null,
	creationdate timestamp
);

create table idea
(
	id serial,
	title varchar(30),
	idauthor int not null,
	synopsis varchar(500) not null,
	keyword varchar(20) not null,
	trends text not null
);

create table idea_keyword
(
	ididea int not null,
	keyword varchar(500) not null
);

CREATE EVENT delete_event
    ON SCHEDULE EVERY 1 HOUR
    ON COMPLETION PRESERVE

    DO
          DELETE from session WHERE creationdate < NOW() - INTERVAL 5 HOUR;