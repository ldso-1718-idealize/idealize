%define _unpackaged_files_terminate_build 0

Name:		idealizeweb
Version:	1.0
Release:	1%{?dist}
Summary:	Idealize Web Server

Group:		Applications/System			
License:	MIT	
URL:		https://projectidealize.me
Source0:	%{name}-%{version}-rpm.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
Requires: nginx

%description
Web Server

%prep
%setup -q

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_sysconfdir}/%{name}
mkdir -p %{buildroot}/%{_sysconfdir}/%{name}/conf.d
mkdir -p %{buildroot}/%{_initddir}
mkdir -p %{buildroot}/%{_datadir}/%{name}
mkdir -p %{buildroot}/%{_localstatedir}/log/%{name}
mkdir -p %{buildroot}/%{_datadir}/%{name}/ssl
cp config/nginx.conf %{buildroot}/%{_sysconfdir}/%{name}/conf.d
cp config/idealize.key %{buildroot}/%{_datadir}/%{name}/ssl
cp config/idealize_chain.crt %{buildroot}/%{_datadir}/%{name}/ssl
cp config/apiidealize.key %{buildroot}/%{_datadir}/%{name}/ssl
cp config/api_idealize_chain.crt %{buildroot}/%{_datadir}/%{name}/ssl
cp config/passwords %{buildroot}/%{_datadir}/%{name}/ssl
cp -r build %{buildroot}/%{_datadir}/%{name}/

%files
%defattr(-,%{name},%{name},-)
%{_datadir}/%{name}/*
%{_localstatedir}/log/*
%config(noreplace) %attr(600,%{%{name}},%{name}) %{_sysconfdir}/%{name}/*

%pre
getent group idealizeweb > /dev/null || groupadd -r idealizeweb
getent passwd idealizeweb > /dev/null || useradd -r -g idealizeweb idealizeweb -s /sbin/nologin

%post
cp -r %{_datadir}/%{name}/build %{_datadir}/nginx/
cp %{_sysconfdir}/%{name}/conf.d/nginx.conf %{_sysconfdir}/nginx/conf.d
cp -r %{_datadir}/%{name}/ssl %{_datadir}/nginx/