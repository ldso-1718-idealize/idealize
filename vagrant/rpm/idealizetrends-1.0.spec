%define _unpackaged_files_terminate_build 0

Name:		idealizetrends
Version:	1.0
Release:	1%{?dist}
Summary:	Idealize Trends Server

Group:		Applications/System			
License:	MIT	
URL:		https://projectidealize.me
Source0:	%{name}-%{version}-rpm.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
Requires: nodejs
Requires(post): chkconfig

%description
Web Server

%prep
%setup -q

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_sysconfdir}/%{name}
mkdir -p %{buildroot}/%{_sysconfdir}/%{name}/conf.d
mkdir -p %{buildroot}/%{_sysconfdir}/rc.d/init.d
mkdir -p %{buildroot}/%{_datadir}/%{name}
mkdir -p %{buildroot}/%{_localstatedir}/log/%{name}
cp -r getGoogleTrends %{buildroot}/%{_datadir}/%{name}
cp init/idealizetrends %{buildroot}/%{_sysconfdir}/rc.d/init.d

%files
%defattr(-,%{name},%{name},-)
%{_datadir}/%{name}/*
%attr(755,root,root) %{_sysconfdir}/rc.d/init.d/*
%{_localstatedir}/log/*
%config(noreplace) %attr(600,%{name},%{name}) %{_sysconfdir}/%{name}/*

%pre
getent group idealizetrends > /dev/null || groupadd -r idealizetrends
getent passwd idealizetrends > /dev/null || useradd -r -g idealizetrends idealizetrends -s /sbin/nologin

%post
/sbin/chkconfig --add idealizetrends
cd %{_datadir}/%{name}/getGoogleTrends
npm install