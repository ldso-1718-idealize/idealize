var https = require('https');
var fs = require('fs');
var cors = require('cors');
var bodyParser = require('body-parser');
var hskey = fs.readFileSync('idealize.key');
var hscert = fs.readFileSync('projectidealize_me.crt')

var options = {
    key: hskey,
    cert: hscert,
	passphrase: 'idealizeldso'
};

var express = require('express'),
app = express(),
port = process.env.PORT || 3001;

app.use(cors({origin: '*'}));
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

var routes = require('./api/routes/trendsRoutes'); //importing route
routes(app); //register the route
https.createServer(options,app).listen(port);

module.exports= app;