const googleTrends = require('google-trends-api');
const translate = require('google-translate-api');

exports.getTrends = function(req, res) {
    let startTime;
    let endTime;
    if(req.params.startDate===undefined)
        startTime = new Date('2004-01-01');
    else startTime = new Date(req.params.startDate);
    if(req.params.endDate===undefined)
        endTime = new Date(Date.now());
    else endTime = new Date(req.params.endDate);
    
    googleTrends.interestOverTime({keyword: req.params.keywords.split(','),startTime:startTime,endTime:endTime})
    .then(function(results){
      res.json(results);
    })
    .catch(function(err){
      res.json(err);
    });   
};

function formatDate(date) {
    var monthNames = [
      "Jan", "Feb", "Mar",
      "Apr", "May", "Jun", "Jul",
      "Aug", "Sep", "Oct",
      "Nov", "Dec"
    ];
  
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear().toString().substr(-2);
  
    return day + '-' + monthNames[monthIndex] + '-' + year;
}

exports.getTrendsMultiple = function(req, res) {
    let startTime;
    let endTime;
    let country;
    let translation;

    if(req.params.startDate===undefined)
        startTime = new Date('2004-01-01');
    else startTime = new Date(req.params.startDate);
    if(req.params.endDate===undefined)
        endTime = new Date(Date.now());
    else endTime = new Date(req.params.endDate);
    if(req.body.country===undefined)
        country = "";
    else country = req.body.country;
    if(req.body.translation===undefined)
    translation=0;
    else translation = req.body.translation;
    let keywords = req.params.keywords.split(',');

    let finaljson =[];
    let promises =[];
    if(translation>0){
        let translations=[];
        let translatedKeywords=[];
        for(let i=0;i<keywords.length;i++){
            translations.push(translate(keywords[i], {from: 'en', to: country})
                .then(res => {
                    translatedKeywords[i]=res.text;
                }).catch(err => {
                    translatedKeywords[i]=keywords[i];
                })
            );
         }

         Promise.all(translations).then(() => {
            results = makeTrendsRequests(translatedKeywords,startTime,endTime,country,keywords);
            Promise.all(results.promises).then(() => {
                res.json(results.finaljson);
            }) 
        })  
  
    }
    else{ 
        results = makeTrendsRequests(keywords,startTime,endTime,country,keywords);
        Promise.all(results.promises).then(() => {
             res.json(results.finaljson);
        })   
    }

};

makeTrendsRequests = function(keywords,startTime,endTime,country,originals){
    let promises =[];
    let finaljson =[];
    for(let i=0; i<keywords.length; i++){
        promises.push(
            googleTrends.interestOverTime({keyword:keywords[i],startTime:startTime,endTime:endTime,geo:country})
            .then(function(results){
                let parsed;       
                parsed = JSON.parse(results);
                let data=[];
                data.push([]);
                for(let i = 0; i < parsed['default']['timelineData'].length; i++) {   
                    data[0].push(
                        {
                            x: formatDate(new Date(parsed['default']['timelineData'][i]['time']*1000)),
                            y: parsed['default']['timelineData'][i]['value'][0]
                        }
                    );
                }
                finaljson.push(
                    {
                        original:originals[i],
                        keyword:keywords[i],
                        data: data
                    }
                );
            })
        );
    }
    return {promises:promises,finaljson:finaljson};
}

