'use strict';
module.exports = function(app) {
  var controller = require('../controllers/trendsController');

  app.route('/trends/:keywords')
    .get(controller.getTrends)

  app.route('/trends/:keywords/:startDate')  //date must be "YYYY-MM-DD" object
    .get(controller.getTrends)
  
  app.route('/trends/:keywords/:startDate/:endDate')
    .get(controller.getTrends)
  
  app.route('/trendgraphs/:keywords')
    .post(controller.getTrendsMultiple)
  app.route('/trendgraphs/:keywords/:startDate')
    .post(controller.getTrendsMultiple)
  app.route('/trendgraphs/:keywords/:startDate/:endDate')
    .post(controller.getTrendsMultiple)

    
};