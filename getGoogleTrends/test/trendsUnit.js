process.env.NODE_ENV = 'test';

let chai = require('chai');
let server = require('../server.js');
let chaiHttp = require('chai-http');
let should = chai.should();
chai.use(chaiHttp);

var getTrends = require('../api/controllers/trendsController')

describe("/trends/:keywords", function() {
  describe("GET Trends - One graph per keyword", function() {
      it("should get graphdata for each keyword without date params", function(done) {
            let params={
                keywords:"test,play",
                
            }
            chai.request(server)
            .get('/trends/'+params.keywords)
            .end((err, res) => {
                let parsed_res=JSON.parse(res.body);
                res.should.have.status(200);
                parsed_res.should.be.a('object');
                parsed_res.default.should.be.a('object');
                done();
            });
      }).timeout(30000);    
      it("should get graphdata for each keyword with date params", function(done) {
        let params={
            keywords:"test,play",
            startDate:"2010-01-01",
            endDate:"2017-01-01"
        }
        chai.request(server)
        .get('/trends/'+params.keywords+'/'+params.startDate+'/'+params.endDate)
        .end((err, res) => {
            let parsed_res=JSON.parse(res.body);
            res.should.have.status(200);
            parsed_res.should.be.a('object');
            parsed_res.default.should.be.a('object');
            done();
        });
       }).timeout(30000);  
  });   
});

describe("/trendgraphs/:keywords/:startDate", function() {
    describe("POST Trends - One graph with values for all keywords", function() {
        it("should get one graph with data for all keywords", function(done) {
            
            let params={
                keywords:"test,play",
                startDate:"2010-01-01"
            }
            chai.request(server)
            .post('/trendgraphs/'+params.keywords+'/'+params.startDate)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(2);
                done();
            });
        }).timeout(30000);     
    });
  });

  describe("/trendgraphs/:keyword/:startDate with country/translation", function() {
    describe("POST Trends - One graph with values for all keywords, translation and country included", function() {
        it("should get one graph with data for all keywords", function(done) {
            let params={
                keywords:"test,car",
                startDate:"2010-01-01"
            }
            chai.request(server)
            .post('/trendgraphs/'+params.keywords+'/'+params.startDate)
            .set('content-type', 'application/x-www-form-urlencoded')
            .send({translation:1,country:"PT"})
            .end((err, res, req) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(2); 
                res.body[0].data.should.be.a('array'); 
                res.body[1].data.should.be.a('array');
                if(res.body[0].original==="car"){
                    res.body[0].keyword.should.be.eql("carro");
                    res.body[1].keyword.should.be.eql("teste");
                }
                else{
                    res.body[0].keyword.should.be.eql("teste");
                    res.body[1].keyword.should.be.eql("carro");
                }   
                done();
            });
        }).timeout(30000);     
    });
  });