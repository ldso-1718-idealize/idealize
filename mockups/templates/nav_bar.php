
    <!-- Navigation  with background photo-->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" id="nav">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- navbar-brand is hidden on larger screens, but visible when the menu is collapsed -->
                <a class="navbar-brand" href="index.html">Idealize</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div  class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" id="nav-options">
                    <li>
                        <a href="index.html">About</a>
                    </li>
                    
                    <li class="dropdown">
                       <a class="dropdown-toggle" type="button" data-toggle="dropdown" id="Account">My Account
    <span class="caret"></span></a>
                      <ul class="dropdown-menu" >
                        <li><a href="#">Log In</a></li>
                        <li><a href="#">Sign Up</a></li>
                      </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
