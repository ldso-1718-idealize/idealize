
 
	<div class="container">	
	<div class="panel panel-default">
	  
	  <div class="panel-body">
	    
		<div id="custom-search-input">
			    <div class="col-sm-12 panel-title">
			    	<h3>Insert a Keyword </h3>
			    </div>	
			<hr class="col-sm-12">   
           
            <div class="input-group col-sm-push-2 col-sm-8" style="margin-bottom: 50px">
                <input type="text" class="  search-query form-control" placeholder="Search" />
                <span class="input-group-btn">
                    <button class="btn btn-danger" type="button">
                        <span class=" glyphicon glyphicon-search"></span>
                    </button>
                </span>
            </div>
              
        </div>
		</div>
        <div class="panel-footer">
            <button type="button" class="btn btn-success btn-lg">Search</button>
        </div>
	  </div>
	</div>
  	
