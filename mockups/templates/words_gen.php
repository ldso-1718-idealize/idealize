
 
	<div class="container">	
	<div class="panel panel-default">
	   <div class="panel-heading">
        <h3 class="panel-title">Words Generated</h3>
      </div>
	  <div class="panel-body" id="keywords">		 
		 
			
            <h3 class="keyword" id="keyword1">Lorem ipsum</h3>
            <h4 class="keyword">Praesent</h4>
            <h4 class="keyword">Consequat</h4>
            <h4 class="keyword">Volutpat</h4>
            <h4 class="keyword">Vehicula</h4>
            <h4 class="keyword">Leo</h4>
            <h4 class="keyword">Etiam</h4>
            
		 
	  </div>
        <div class="panel-footer col-xs-12" id="reg_id">
				<div class="col-xs-6">
					<button type="button" class="btn btn-success btn-lg">Regenerate</button>					
				</div>
				<div class="col-xs-6">
					<button type="button" class="btn btn-success btn-lg">Idealize</button>
				</div>				
        </div>
        
        
	</div>
  </div>	
 

<script>
    
    var fullWidth = document.getElementById('keywords').offsetWidth;
    var fullHeight = 370;
    var widthKeyword = document.getElementById('keyword1').offsetWidth;
    
    var centerX = (fullWidth/2 - Math.round(widthKeyword/2)) + "px";
    
    var elements = document.getElementsByClassName("keyword");
    
    var parts = Math.round(fullWidth/elements.length);
    
    elements[0].style.top = Math.round(fullHeight/2) + "px";
    elements[0].style.left = centerX;
    
    for(var i = 1; i < elements.length; i++) {
        
        elements[i].style.left = i * parts + "px";
        
        if( i > elements.length/2 - 2 && i < elements.length/2 + 2) {
            
            var random = fullHeight/2;

            do {
                 
                random = Math.random() * fullHeight;    
                 
            }while(random > fullHeight/2-15 && random < fullHeight/2+15)   
            
            elements[i].style.top = random + "px";    
                
        }else {
            elements[i].style.top = Math.random() * fullHeight + "px";    
        }  
         
        
        
    }
    
    
</script>