
<div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Business idea Title</h3>
      </div>
      <div class="panel-body">
          <div class="col-xs-12">
          <div class="col-xs-12 keywords">
            <h3><span class="label label-default">KeyWord1</span></h3>
            <h3><span class="label label-default">KeyWord2</span></h3>
            <h3><span class="label label-default">KeyWord3</span></h3>
            <h3><span class="label label-default">KeyWord4</span></h3>   
            <h3><span class="label label-default">KeyWord5</span></h3>   
            <h3><span class="label label-default">KeyWordN</span></h3>
          </div>
          <hr class="col-xs-12">
            <div class="description col-xs-12 col-md-6 col-md-push-3">
                <h2>Description:</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vel laoreet metus. Curabitur at rutrum felis. Cras ultrices efficitur placerat. Maecenas placerat euismod vehicula. Maecenas in quam viverra, dignissim metus nec, lobortis ex. Cras gravida ex arcu, et fringilla dui molestie at. Integer eu orci porttitor eros tincidunt luctus non vitae massa. Donec sit amet elementum velit. Fusce consectetur tortor sollicitudin, dignissim mauris eu, interdum lacus. Nulla et pellentesque lorem. Pellentesque tempor pellentesque eros consectetur tempor. In gravida hendrerit sem mattis placerat. Maecenas dignissim bibendum odio ac ultrices. Maecenas sollicitudin tortor nisi.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vel laoreet metus. Curabitur at rutrum felis. Cras ultrices efficitur placerat. Maecenas placerat euismod vehicula. Maecenas in quam viverra, dignissim metus nec, lobortis ex. Cras gravida ex arcu, et fringilla dui molestie at. Integer eu orci porttitor eros tincidunt luctus non vitae massa. Donec sit amet elementum velit. Fusce consectetur tortor sollicitudin, dignissim mauris eu, interdum lacus. Nulla et pellentesque lorem. Pellentesque tempor pellentesque eros consectetur tempor. In gravida hendrerit sem mattis placerat. Maecenas dignissim bibendum odio ac ultrices. Maecenas sollicitudin tortor nisi.</p>
            </div>       
        
        
        </div>
        </div>
      <div class="panel-footer">
        <button type="button" class="btn btn-success btn-lg">Analyse</button>
        </div>
        
    </div>

</div>
