import React from 'react';
import LoginForm from '../src/MyComponents/login_form';

describe('Login item', () => {
   
  const wrap = mount(<LoginForm updateUser={(userValue) => {} }/>);
  const wrap2 = mount(<LoginForm updateUser={(userValue) => {} }/>);
  const button = wrap.find('#login-btn');
  const button2 = wrap2.find('#login-btn');	

  it('Login button is inicial disabled', () => {  	
  	expect(button.prop('disabled')).to.equal(true);
  });
 
    it('Login button is disabled until user insert a password ', () => {
        
	wrap.find('#formControlsUsername').simulate('change', {target: {value: 'UserName'}});
        
        wrap.update();
 	 
      	expect(button.prop('disabled')).to.equal(true);
  }); 
  it('Login button is enable after 5 characters where inserted at password line', () => {
	wrap2.find('#formControlsUsername').simulate('change', {target: {value: 'UserName'}});
        wrap2.find('#formControlsPassword').simulate('change', {target: {value: 'ppppp'}});
     	
        wrap2.update();
 	 
      	expect(button2.prop('disabled')).to.equal(false);	       
  }); 


});
