import React from 'react';
import NavB from '../src/MyComponents/navbar';

describe('NavBar item', () => {
   
  const wrap = mount(<NavB  updateLogin={()=>{}}/>);
  //const wrap = mount(<Profile />);
  
	
  it('All links must have a valid url', () => {
	
	for(var i = 0; i < 4; i++)
		expect(wrap.find('a').at(i).prop('href')).to.not.include('#');
	
		
  });

 

});
