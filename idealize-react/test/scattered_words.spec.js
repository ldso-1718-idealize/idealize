import React from 'react';
import Words from '../src/MyComponents/scattered_words';
require("babel-polyfill");
import { spy } from 'sinon';
var submited = false;	
var submitedWords;

spy(Words.prototype, 'componentDidMount');

describe('Scattered Words item', () => {
   
  
  const wrap = mount(<Words centerWord="WORD"
                                  brainstormHandler={(newPage, newlist) => {submited = true; submitedWords = newlist;} }/>, { lifecycleExperimental: true });
  
  var words;
  
	
  it("It should generate 10 words", async function() {
	    
	   this.timeout(3000); 
	   var testPromise = new Promise(function(resolve, reject) {
		setTimeout(function() {
		   resolve("Hello");
		}, 2000);
	    });
	    
	 	
	    var result = await testPromise;
      	setImmediate(() => {	
	    expect(Words.prototype.componentDidMount.calledOnce).to.equal(true);
	    wrap.update();
	    	
	    expect(wrap.state('words')).to.have.length(10);
	    words = wrap.state('words');
            wrap.find('.stop-4').simulate('click');

		});
   });

   it("When regenerate button is pressed, words should be different", async function() {
	  

	  var testPromise = new Promise(function(resolve, reject) {
		setTimeout(function() {
		
		   resolve("Hello");
		}, 1100);
	    });
	    	
	    var result = await testPromise;
	setImmediate(() => { 
	    expect(wrap.state('words')).to.have.length(10);
	    
	    expect(wrap.state('words')).to.not.eql(words); 
	    words = wrap.state('words');	
	});
   });

   it("When Pen Idea button is pressed words should transmitted to the next fase", () => {
	setImmediate(() => {	
	wrap.find('.stop-5').simulate('click');
	expect(submited).to.equal(true);
	//expect(submitedWords).to.equal(words);	
	});
  });




});
