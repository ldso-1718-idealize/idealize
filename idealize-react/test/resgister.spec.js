import React from 'react';
import RegisterForm from '../src/MyComponents/register_form';

describe('Register item', () => {
   const wrapper = mount(<RegisterForm updateUser={() => {} } />);
   const button = wrapper.find('#register-btn');
  	


  it('Register button should be disabled inicially', () => {
	
	expect(button.prop('disabled')).to.equal(true);	
  });
  
  
  
  it('Register button should not be enabled when all fields are correct and the user did not clicked in recaptcha', () => {
	 

	var formControlsUsername = wrapper.find('#formControlsUsername');	
	var formControlsEmail = wrapper.find('#formControlsEmail');
	var formControlsPassword = wrapper.find('#formControlsPassword');
	var formControlsPassword2 = wrapper.find('#formControlsPassword2');
	
	formControlsUsername.simulate('change', {target: {value: 'user'}});
	formControlsEmail.simulate('change', {target: {value: 'hotmail@hotmail.com'}});
	formControlsPassword.simulate('change', {target: {value: 'password'}});
	formControlsPassword2.simulate('change', {target: {value: 'password'}});  	
	wrapper.update();	

	expect(button.prop('disabled')).to.equal(true);
  });
  

});
