import React from 'react';
import Form from '../src/MyComponents/Form';

var transition = false;
var synopsisRec;

describe('Form item', () => {
   
  const wrap = mount(<Form  keywordN={"center"}  errorMessage={""} synopsis={""} listWords={["word1", "word2"]} synopsisHandler={(page, synopsis) => {transition = true; synopsisRec = synopsis;}} keywordN={"word0"} fromPage="Home" />);
	
  
 it('Keywords and listWords passed should be displayed correctly', () => {	
	 
	expect(wrap.find('.form-words')).to.have.length(2);	
  });
 it('Users can insert a description', () => {
	var textArea = wrap.find('.form-description');

	wrap.find('.form-description').simulate('change', {target: {value: 'My new value'}});
        expect(textArea.props().value).to.equal("My new value");
  });
/*
  it('Component should pass synopsis to his parent handler', () => {
	wrap.find('#btn-analyze').at(0).simulate('click'); 
        expect(transition).to.equal(true);
        expect(synopsisRec).to.equal("My new value");
  });
  
 */ 
  		
  


});
