import React from 'react';
require("babel-polyfill");
import { spy } from 'sinon';
import Profile from '../src/pages/Profile/Profile';

 
    



describe('Profile item: (testing with all mockies commented)', () => {
   
  const wrap = mount(<Profile />);
  const ideas = wrap.find('.ideasS');
  const invites = wrap.find('.invites');	


  it('Users should have 3 ideas in his portfolio', () => {
	 
	
     setImmediate(() => {

        expect(ideas.update()).to.have.length(3);
        done();
      });
	
	
	
  });

  it('Users should have 3 invites', () => {
	setImmediate(() => {
	expect(invites).to.have.length(3);
	});
  });
 
  it('Users should not be able to erase other users ideas that they have access', () => {
	 setImmediate(() => { 	
		wrap.find(".deleteIdea").at(2).simulate('click');
		wrap.update();
		expect(ideas).to.have.length(3);
	 });
  });	
  it('Users should be able to erase their ideas', () => {
	setImmediate(() => {  	
	wrap.find(".deleteIdea").at(0).simulate('click');
	wrap.update();
	expect(ideas).to.have.length(2);
	});
  });
  it('Users should be accept/reject invites', () => {
	setImmediate(() => { 		
		wrap.find(".deleteInvite").at(0).simulate('click');
		wrap.update();
		expect(ideas).to.have.length(2);
	 });
  });

 


});
