import { expect, assert } from 'chai';

import { mount, render, shallow } from 'enzyme';

global.expect = expect;
global.assert = assert;

global.mount = mount;
global.render = render;
global.shallow = shallow;
