import React from 'react';
import SearchWord from '../src/MyComponents/home_page';

describe('Home Page item', () => {
  const wrapper = shallow(<SearchWord  />);
  const wrap = mount(<SearchWord />);
  const buttons = wrapper.find('.btn'); 	
  const inputField = wrap.find('.search-query');	
	
   it('should have two buttons for search', () => {
	
	expect(buttons).to.have.length(2);	
  });

  it('all buttons should be disabled until search word has more than 3 characters', () => {
	expect(buttons.at(0).prop('disabled')).to.equal(true);
	expect(buttons.at(1).prop('disabled')).to.equal(true);	
		    	
  });

  it('all buttons should be enabled after 3 characters were inserted', () => {
      inputField.simulate('change', {target: {value: 'My new value'}});
      expect(inputField.props().value).to.equal('My new value');	
      
      wrap.update();
	
		
      expect(wrap.find('.btn').at(1).prop('disabled')).to.equal(false);
  });	
 
  
 
 
});
