var path = require('path')
var webpack = require('webpack');
module.exports = {
    entry: [
	'babel-polyfill',
        'whatwg-fetch',
        'react-hot-loader/patch',
        'webpack-dev-server/client?http://localhost:3000',
        'webpack/hot/only-dev-server',
        './src/App.js'
    ],
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, 'public'),
        publicPath: '/'
    },
    module:{
        rules:[{
            test: /\.jsx?$/,
            include:[path.resolve(__dirname, 'src')],
            exclude: [path.resolve(__dirname,"node_modules")],
            loader: "babel-loader"
        },
        {
            test: /\.css$/,
            loaders: [ 'style-loader', 'css-loader' ]
        },
        {
            test: /\.(png|jpe?g|woff|woff2|eot|ttf|svg)$/,
            use: [
              {
                loader: 'file-loader',
                options: {}  
              }
            ]
          }       
              
              ],
        loaders: [
          // telling webpack which loaders we want to use.  For now just run the
          // code through the babel-loader.  'babel' is an alias for babel-loader
          { test: /\.js$/, loaders: ['babel'], exclude: /node_modules/ },
          { test: /\.css$/, loaders: ['style-loader', 'css-loader'], exclude: /node_modules/ },

        { test: /\.css$/, loaders: ['style-loader', 'css-loader'], include: /node_modules/ },
           
        {
  test: /\.(gif|png|jpe?g|svg)$/i,
  use: [
    'file-loader',
    {
      loader: 'image-webpack-loader',
      options: {
        bypassOnDebug: true,
      },
    },
  ],
},
	{
  test: /\.(jpg|png|svg)$/,
  use: {
    loader: "url-loader",
    options: {
      limit: 25000,
    },
  },
},

            { test: /\.html$/, loader: 'html-loader' },
            { test: /\.json$/, loader: 'json-loader' },
		
        ],
    },
    devtool: "source-map",
    devServer: {
        hot: true,
        contentBase: path.resolve(__dirname, 'public'),
        publicPath: '/',
        port:3000
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
    ],
}
