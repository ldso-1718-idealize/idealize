import React, { Component } from 'react';

export class Expire extends Component {
    constructor() {
        super();

        this.state = {
            delay: 1000,
            visible: true
        }

    }

    componentWillReceiveProps(nextProps) {
        // reset the timer if children are changed
        if (nextProps.children !== this.props.children) {
            this.setTimer();
            this.setState({visible: true});
        }
    }

    componentDidMount() {
        this.setTimer();
    }

    setTimer() {
        // clear any existing timer
        // eslint-disable-next-line
        this._timer !== null ? clearTimeout(this._timer) : null;

        // hide after `delay` milliseconds
        this._timer = setTimeout(function(){
            this.setState({visible: false});
            this._timer = null;
        }.bind(this), this.props.delay);
    }

    componentWillUnmount() {
        clearTimeout(this._timer);
    }

    render() {
        return this.state.visible
            ? <div>{this.props.children}</div>
            : <span />;
    }
}