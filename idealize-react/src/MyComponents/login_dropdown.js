import  React, { Component } from 'react';
import { Tooltip, Modal, MenuItem} from 'react-bootstrap';
import SwitchButton from 'react-switch-button/lib/react-switch-button.js';
import { LoginForm } from "./login_form";
import { RegisterForm } from "./register_form";
import { Expire } from "./Expire";
import {api_url} from "../App";

const NO_SESSION =      0;
const SESSION =         1;
const JUST_LOGGED_IN =  2;
const JUST_REGISTERED = 3;
const JUST_LOGGED_OUT = 4;


export class LoginDropdown extends Component {
    constructor(props) {
        super(props);

        let user = "Login";
        let logState = false;
        if (this.props.loginUpdated !== undefined){
            user = this.props.loginUpdated;
            logState = true;
        }


        this.state = {
            stateID: NO_SESSION,
            loggedIn: logState,
            registered: true,
            menuOpen: false,
            username: user,
            showToolTip: true,
            message: '',
        }
    }
    render() {
        var form = (
            <div className={"enclosing-div"}>
                <span
                    id="nav-button"
                    className="stop-3 navlinkto"
                    onClick={() => this.open()}
                >{this.state.username}</span>
                <Modal
                    show={this.state.menuOpen}
                    onHide={() => this.close()}
                    id="mymodal"
                >
                    <SwitchButton
                        className={"login-switch"}
                        name="switch-4"
                        labelRight="I don't have an account yet"
                        defaultChecked={true}
                        onChange={() => this.onClick()}
                    />
                    {this.state.registered ?
                        <LoginForm
                            updateUser={(userValue, closeModal) => this.handleUserUpdate(userValue, closeModal)}/>
                        :
                        <RegisterForm updateUser={() => this.handleRegister()}/>
                    }
                </Modal>
            </div>
        );

        var formPlusTooltip = (
            <div className="enclosing-div">
                {form}
                {this.state.showToolTip ?

                    <Expire delay={2000}>
                        <Tooltip placement="none" className="in" id="tooltip-bottom">
                            {this.state.message}
                        </Tooltip>
                    </Expire> :
                    null
                }
            </div>
        );

        var loggedInUserMenu = (
            <div className="enclosing-div">
                <span
                    id="nav-button"
                    className="stop-3 navlinkto"
                    onClick={() => this.open()}
                >{this.state.username}</span>
                <Modal
                    show={this.state.menuOpen}
                    onHide={() => this.close()}
                    id="mymodal"
                    dialogClassName="profile-modal"
                >
                    <ul className="profile-menu">
                        <MenuItem href="/teams" className="profile">My Teams</MenuItem>
                        <MenuItem href="/profile" className="profile">Profile</MenuItem>
                        <MenuItem className="profile" onClick={() => this.logout()} >Logout</MenuItem>
                    </ul>
                </Modal>
            </div>
        );

        var loggedInUserMenuPlusTooltip = (
            <div className={"enclosing-div"}>
                {loggedInUserMenu}
                {this.state.showToolTip ?

                    <Expire delay={2000}>
                        <Tooltip placement="none" className="in" id="tooltip-bottom">
                            {this.state.message}
                        </Tooltip>
                    </Expire> :
                    null
                }
            </div>
        );

        if (this.state.stateID === NO_SESSION) {
            return form;
        }
        else if (this.state.stateID === SESSION) {
            return loggedInUserMenu;
        }
        else if (this.state.stateID === JUST_LOGGED_IN) {
            return loggedInUserMenuPlusTooltip;
        }
        else if (this.state.stateID === JUST_REGISTERED) {
            return formPlusTooltip;
        }
        else if (this.state.stateID === JUST_LOGGED_OUT) {
            return formPlusTooltip;
        }
    }

    onClick() {
        this.setState({registered: !this.state.registered,
                        showToolTip: false
        });
    }

    close() {
	    this.setState({menuOpen: false});
    }

    open() {
	    this.setState({menuOpen: true,
	                    showToolTip: false,
	    });
    }

    handleUserUpdate(userValue, closeModal) {
	    if (userValue !== '') {
            this.setState({username: userValue,
                loggedIn: true,
                showToolTip: true,
                stateID: JUST_LOGGED_IN,
                message: 'You are now logged in!'
            });
        }

        if (closeModal){
            this.close();
        }

        this.props.updateLogin(true);
    }

    handleRegister() {
        this.setState({showToolTip: true,
                        registered: true,
                        stateID: JUST_REGISTERED,
                        message: 'Registered! Go to your email and activate your account',
        });
        this.close();
    }

    handleUserHasSession(userValue) {
        this.setState({username: userValue,
                        loggedIn: true,
                        stateID: SESSION,
        });

		this.props.updateLogin(true);
    }

    handleUserLogout() {
        this.setState({username: 'Login',
                        loggedIn: false,
                        registered: true,
                        showToolTip: true,
                        menuOpen: false,
                        stateID: JUST_LOGGED_OUT,
                        message: 'You have logged out successfully',
        });

        this.props.updateLogin(false);
        window.location.replace("/");
    }

    componentWillMount() {
        let res = 'undefined';

        fetch(api_url + 'getSession', {
        //fetch('http://www.mocky.io/v2/59f9e78c370000920d66b895', { //401
        //fetch('http://www.mocky.io/v2/59fa062c370000d51366b8ee', {   //200
			credentials: 'include'
		})
        .then(function(response) {
			if(response.ok)
			{
				return response.text();
			}
			else
			{
				throw new Error("No session");
			}
		}).then(function(resp) {
			res = resp;
        }).then(() => this.handleUserHasSession(res))
		  .catch((error) => {
		      this.setState({stateID: NO_SESSION});
		      console.log("Unexpected response: " + error.message);
        });
    }

    logout(){
        fetch(api_url + 'logout', {
        //fetch('http://www.mocky.io/v2/59f9e78c370000920d66b895', { //401
        //fetch('http://www.mocky.io/v2/59f9eae13700001d0e66b8a6', { //200
            credentials: 'include'
        })
            .then(function(response) {
                if(response.ok || response.status === 400)
                {
                    return response.text();
                }
                throw new Error("No session");
            }).then(function(resp) {
        }).then(() => this.handleUserLogout())
            .catch(() => this.handleUserLogout(), function(error) {
                console.log("Unexpected response: " + error.message);
            })
    }
}
