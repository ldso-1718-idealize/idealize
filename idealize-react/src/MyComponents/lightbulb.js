import React, { Component } from 'react';
import Tour from "react-user-tour";


export class LightBulb extends Component {
  constructor(props) {
    super(props);
    let tips=localStorage.getItem('tips')===null ? false : localStorage.getItem('tips')==="true";
    this.state = {
      isTourActive: false,
      tourStep: 1,
      showBulb: true,
      tips: tips,
      page: props.page
    }
    this.handleInputChange = this.handleInputChange.bind(this);
  }
  componentDidMount() {
    this.setState({
      isTourActive: true
    });
  }
  componentWillReceiveProps(nextProps) {
 
    if (nextProps.page !== this.state.page) {
      this.setState({ page: nextProps.page, isTourActive: true, showBulb: true, tourStep:1 });
    }
  }

  render() {
    const tourTitleStyle = {
      fontWeight: 700,
      fontSize: 18,
      paddingTop: 10,
      paddingBottom: 10,
      paddingLeft: 10,
      backgroundColor: '#a0ebff'
    };

    const tourMessageStyle = {
      fontSize: 14,
      paddingLeft: 10,
      paddingRight:10
    };

    let nextBulb;
    let myTour;

    switch(this.state.page) {
      case "initial":
      nextBulb =<div
        style={{width: 128, height: 128, left: "2%", top: "10%", position: "absolute"}}
        className="stop-1"
        >
        { this.state.showBulb ? <img src={require("./lightbulb.png")} alt="logo" /> : null }
      </div>;

      myTour=  <Tour
        arrowColor='#a0ebff'
        active={this.state.isTourActive}
        step={this.state.tourStep}
        onNext={(step) => this.setState({tourStep: step})}
        onBack={(step) => this.setState({tourStep: step})}
        onCancel={() => { let tips=localStorage.getItem('tips')===null ? false : localStorage.getItem('tips')==="true";
          this.setState({isTourActive: false, showBulb: false, tips:tips})
        }}

        steps={[
          {
            step: 1,
            selector: ".stop-1",
            title: <div className="title_font" style={tourTitleStyle}>Idealize User Tour</div>,
            body:
            <div>
              <br></br>
              <div className="normal_text" style={tourMessageStyle}>Welcome to Idealize. Use this tool to get a guided tour of the website.</div>
              <br></br>
              <label style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                <span style={{paddingRight: '5px'}}>Hide forever: </span>
                <input
                  name="hide"
                  type="checkbox"
                  checked={this.state.isChecked}
                  onChange={this.handleInputChange}/>
              </label>
            </div>,
            position: "right"
          },
          {
            step: 2,
            selector: ".stop-2",
            title: <div style={tourTitleStyle}>Search</div>,
            body:
            <div>
              <br></br>
              <div className="normal_text" style={tourMessageStyle}>
                Insert the root of your idea and click Search.
              </div>
            </div>
          },
          {
            step: 3,
            selector: ".stop-3",
            title: <div style={tourTitleStyle}>Login</div>,
            body:
            <div>
              <br></br>
              <div className="normal_text" style={tourMessageStyle}>
                Click here to login or create a new account.
              </div>
            </div>
          }
        ]}

        />;
      break;

      case "words":
      nextBulb =<div
        style={{width: 128, height: 128, left: "2%", top: "10%", position: "absolute"}}
        className="stop-1"
        >
        { this.state.showBulb ? <img src={require("./lightbulb.png")} alt="logo" /> : null }
      </div>;
      myTour=  <Tour
        arrowColor='#4c8ef7'
        active={this.state.isTourActive}
        step={this.state.tourStep}
        onNext={(step) => this.setState({tourStep: step})}
        onBack={(step) => this.setState({tourStep: step})}
        onCancel={() => this.setState({isTourActive: false, showBulb: false})}

        steps={[
          {
            step: 1,
            selector: ".stop-1",
            title: <div style={tourTitleStyle}>Idealize User Tour</div>,
            body:
            <div>
              <br></br>
              <div className="normal_text" style={tourMessageStyle}>
                Random words will be presented to you, choose up to two associations,connect the concepts to come up with new ideas, brainstorm away!
              </div>
            </div>,
            position: "bottom"
          },
          {
            step: 2,
            selector: ".stop-4",
            title: <div style={tourTitleStyle}>Regenerate</div>,
            body: <div className="normal_text" style={tourMessageStyle}>Regenerate the words to be presented.</div>
        },
        {
          step: 3,
          selector: ".stop-5",
          title: <div style={tourTitleStyle}>Brainstorm</div>,
          body: <div className="normal_text" style={tourMessageStyle}>Click here to describe your business idea so we can analyze it for you.</div>
      }
    ]}
    />;
  break;

  case "synopsis":
  nextBulb =<div
    style={{width: 128, height: 128, left: "2%", top: "10%", position: "absolute"}}
    className="stop-1"
    >
    { this.state.showBulb ? <img src={require("./lightbulb.png")} alt="logo" /> : null }
  </div>;
  myTour=  <Tour
    arrowColor='#4c8ef7'
    active={this.state.isTourActive}
    step={this.state.tourStep}
    onNext={(step) => this.setState({tourStep: step})}
    onBack={(step) => this.setState({tourStep: step})}
    onCancel={() => this.setState({isTourActive: false, showBulb: false})}

    steps={[
      {
        step: 1,
        selector: ".stop-1",
        title: <div style={tourTitleStyle}>Idealize User Tour</div>,
        body: <div className="normal_text" style={tourMessageStyle}>Type in a description for your idea and click de Analyze icon.</div>,
        position: "bottom"
      }
    ]}
    />;
  break;

  default:
}

if(window.innerWidth < 800 )
return null;

return (

  <div>
    {this.state.tips===false &&
      <div className="App">

        {nextBulb}

        <div style={{position: "absolute", top: 0}}>
          {myTour}
        </div>
      </div>
    }
  </div>
);
}

handleInputChange(event){
  localStorage.setItem('tips',event.target.checked);
}

}
