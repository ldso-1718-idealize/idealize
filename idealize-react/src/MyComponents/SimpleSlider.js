import React, {Component} from 'react';
import Slider from 'react-slick';


export class SimpleSlider extends Component {

  render() {

    var settings = {
      dots: true,
      draggable: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay:true,
      autoplaySpeed:2500
    };
    return (
      <div>     
      <div className="home_title_div" >
        <h2 className="home_title" style={{fontWeight:700}}> Brainstorming for entrepreneurs</h2>
      </div>
     <div style={{position: 'absolute', top: '25%',zIndex:2}}>
      <p className="home_desc" style={{fontWeight:800}}> Idealize is a platform to be used by entrepreneurs to brainstorm ideas. After inserting the root of the idea (for example, "cars" or "children" or any other theme you are thinking...), random words will be presented so that you can create a new concept (combine your root concept or word with one or two random words to build an original idea). We will give you various metrics to determine the market value of your idea.</p>
     </div>
      <Slider {...settings}>
        <div><img style={{opacity: '0.1',zIndex:-2}} width="100%" height="350vh" src={require("../images/carousel1.jpg")} alt="logo" /></div>
        <div><img style={{opacity: '0.2',zIndex:-2}} width="100%" height="350vh" src={require("../images/carousel2.png")} alt="logo" /></div>
        <div><img style={{opacity: '0.2',zIndex:-2}} width="100%" height="350vh" src={require("../images/carousel3.jpg")} alt="logo" /></div>
        <div><img style={{opacity: '0.2',zIndex:-2}} width="100%" height="350vh" src={require("../images/carousel4.jpg")} alt="logo" /></div>
      </Slider>
      </div>
    );
  }
}
