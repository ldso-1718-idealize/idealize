import React, { Component } from 'react';
import 'whatwg-fetch';
import { FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';
import {api_url} from "../App";

function FieldGroup(props) {
    return (
        <FormGroup controlId={props.id}>
            <ControlLabel>{props.label}</ControlLabel>
            <FormControl {...props} />
        </FormGroup>
    );
}

export class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            formValid: false,
            usernameFilledOut: false,
            loginUnsuccessful: false,
            changePassword: false,
            message: "We'll send you an email so you can choose a new password. Are you sure you want to do this?",
            emailSent: false,
            serverMessage:"Wrong Credentials",
            buttonString: "No",
        }
    }
    render() {
        var placeholder = "";
        var state = null;

        if (this.state.loginUnsuccessful){
            placeholder = this.state.serverMessage;
            state = "error";
        }

        var message = "Forgot password? (Fill username out first)";
        if (this.state.usernameFilledOut){
            message = "Forgot password?";
        }

        var form = (
            <form className={"login-form"}>
                <FieldGroup
                    id="formControlsUsername"
                    type="text"
                    label="Username"
                    placeholder="Enter your username"
                    value={this.state.username}
                    onChange={(e) => this.handleInput(e, 'username')}
                />
                <FormGroup controlId="formControlsPassword"
                           validationState={state}
                >
                    <ControlLabel>Password</ControlLabel>
                    <FormControl
                        type="password"
                        placeholder={placeholder}
                        value={this.state.password}
                        onChange={(e) => this.handleInput(e, 'password')}
                        onKeyPress={(event) => this.handleKeyPress(event)}
                    />
                    <FormControl.Feedback />
                </FormGroup>
                <Button id={"login-btn"}
                        onClick={() => this.onSubmit()}
                        disabled={!this.state.formValid}
                >
                    Log In
                </Button>
                <Button id="reset-password"
                        onClick={() => this.setState({changePassword: true})}
                        disabled={!this.state.usernameFilledOut}
                >
                    {message}
                </Button>

            </form>
        );

        var confirmReset = (
            <div>
                <h4>{this.state.message}</h4>
                {!this.state.emailSent ?
                    <Button id={"login-btn"}
                            onClick={() => this.forgotPassword()}
                    >
                        Yes
                    </Button> :
                    null
                }
                {!this.state.emailSent ?
                    <Button id="login-btn"
                            onClick={() => this.setState({changePassword: false})}>
                        {this.state.buttonString}
                    </Button> :
                    <Button id="login-btn"
                            onClick={() => this.props.updateUser('', true)}>
                        {this.state.buttonString}
                    </Button>
                }

            </div>
        );

        if (this.state.changePassword) {
            return confirmReset;
        } else {
            return form;
        }
    }



    forgotPassword() {
        var formData = "username=" + this.state.username;

        fetch(api_url + 'reset', {
        //fetch('http://www.mocky.io/v2/59f9e78c370000920d66b895', { //401
        //fetch('http://www.mocky.io/v2/5a1e9f982f00001727ee2eb4', {   //200
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
            }),
            body: formData
        }).then(function(response) {
            if (response.ok){
                return response.text();
            }
            throw new Error('Network response was not ok.');
        }).then(() => this.reset())
        .catch(() => this.unsuccessful(),
            function(error) {
                console.log('Problem with fetch: ' + error.message);
            });
    }

    reset() {
        this.setState({message: "Email sent!",
                        emailSent: true,
                        buttonString: "Dismiss"});
    }

    handleInput(e, field) {
        let value = e.target.value;

        this.setState({[field]: value},
            () => {this.validateForm()});
    }

    handleKeyPress(event) {
        if (event.key === 'Enter'){
            this.onSubmit();
        }
    }

    validateForm() {
        if (this.state.username !== ''){
            this.setState({usernameFilledOut: true});
        } else {
            this.setState({usernameFilledOut: false});
        }

        if (this.state.password !== '' && this.state.username !== ''){
            this.setState({formValid: true});
        } else {
            this.setState({formValid: false});
        }
    }

    unsuccessful() {
        this.setState({loginUnsuccessful: true,
                        username: '',
                        password: '',
                        formValid: false
        });
    }

    onSubmit() {
        var formData = "username=" + this.state.username + "&password=" + this.state.password;

        fetch(api_url + 'login', {
        //fetch('http://www.mocky.io/v2/5a3a7bb931000046148e95b8', {   //400
        //fetch('http://www.mocky.io/v2/59f9eae13700001d0e66b8a6', {   //200
            method: 'POST',
			credentials: 'include',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
            }),
            body: formData
        }).then(function(response) {
            if (response.ok)
                return "";
            if(response.status===400)
                return response.text();
            throw new Error('Network response was not ok.');
        }).then((text) => {
            if(text==="")
                this.props.updateUser(this.state.username, true)
            else {
                this.setState({
                    serverMessage:text
                })
                throw new Error('Network response was not ok.');
            }
        })
        .catch(() => this.unsuccessful(),
              function(error) {
                console.log('Problem with fetch: ' + error.message);
        });
    }
}

export default LoginForm;