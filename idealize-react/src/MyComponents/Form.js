import React, {Component} from 'react';
import {CountryDropdown} from 'react-country-region-selector';
import {ShareIdea} from "./share_idea";
import {Reanalyze} from "./reanalyze";
import Ads from "./Ads";
import './Form.css';
import {captcha_site} from "../App";
import ReCAPTCHA from "react-google-recaptcha";
var captcha;

function getRandomColor(props) {
    const word = props.word;
    var rand = require('random-seed').create({word});
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (var i = 0; i < 6; i++) {
        var randValue = rand.random();
        color += letters[Math.floor(randValue * 16)];
    }
    return color;
}
function getBrightness(color){
    let c = color.substring(1);      // strip #
    let rgb = parseInt(c, 16);   // convert rrggbb to decimal
    let r = (rgb >> 16) & 0xff;  // extract red
    let g = (rgb >>  8) & 0xff;  // extract green
    let b = (rgb >>  0) & 0xff;  // extract blue

    let luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709

    return luma;
}
//AIzaSyCdlbRNVVS2r-WRHTGjVm3c54PEcduS-DQ
function getStyle(props){
    const word = props.word;
    let color = getRandomColor({word});
    let font;

    if(getBrightness(color)> 200)
        font = "	#778899";
    else font = "#ffffff";

    let style = {
        background: color,
        color: font,
        borderRadius: 5,
        padding:10,
        fontSize: "1.2em"
    };
    return style;
}

function ListWords(props) {
    const words = props.listWords;
    const listItems = words.map((word) =>
        <li key={word} className="list-group-item form-words title_font" style={getStyle({word})}>{word}{'  '}<span className="glyphicon glyphicon-arrow-right"></span>{'  '}{props.keyword}</li>
    );
    return (
        <ul className="list-group form-words-ul">{listItems}</ul>
    );
}
function ErrorMessageHandler(props) {
    const errorMessage = props.errorMessage;
    if (errorMessage.length===0) return (null); else return (<div className="errormessage"><p className="normal_text" style={{color:'red'}}>{errorMessage}</p></div>);
}

export class Form extends Component {

    constructor(props) {
       
        let translation=localStorage.getItem('translation')===null ? 0 : localStorage.getItem('translation');
        let country = props.country;
        if(props.fromPage==="Home")
            country=localStorage.getItem('country')===null ? "" : localStorage.getItem('country');
        super(props);
        this.state = {
            listWords: [],
            keywordN: props.keywordN,
            value: props.synopsis,
            errorMessage: "",
            chars_left: 500-props.synopsis.length,
            country: country,
            translation:translation,
            fromPage: props.fromPage,
            editMode: false,
            hasTeam: false,
            teams: [],
            selectedTeam: '',
            emailValid: false,
            shareToEmail: '',
            menuOpen: false,
            validCaptcha: false
        };
        this.handleChange=this.handleChange.bind(this);
        this.selectCountry=this.selectCountry.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.captchaChange = this.captchaChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if(this.props.errorMessage!==nextProps.errorMessage)
            this.setState({
                errorMessage:nextProps.errorMessage
            });
    }

    handleChange(event) {
        var input = event.target.value;
        var myColor = 'black';
        if(input.length === 500){
            myColor = 'red';
        }
        this.setState({
            value:input,
            chars_left: 500 - input.length,
            color: myColor
        });
        console.log(this.state.value);
        console.log(input);

    }

    captchaChange(value){
        if(value!==null){
            this.setState({
                validCaptcha:true
            });
        }
        else captcha.reset();
    };
    
   
    
    
    editMode(e){
        e.preventDefault();
        this.setState({editMode: !this.state.editMode})
    }

    selectCountry (val) {
        if(val==="")
            this.setState({ country: val, translation:0 });    
        else this.setState({ country: val });
       
    }

    handleInputChange(event){
        this.setState({ translation: +event.target.checked });
    }

    updateErrorMessage(message) {
        this.setState({errorMessage: message});
    }

    render() {
        var editLabel = (
            <label className="form-label-edit normal_text" htmlFor="description">
                Description:
                <button className="edit-button" onClick={(e) => this.editMode(e)}>
                    <img
                        src={require("../images/edit-button.png")}
                        style={{width: '30px', height: '30px'}}
                        alt=""/>
                </button>
            </label>
        );

        var normalLabel = (
            <label className="form-label normal_text" htmlFor="description">
                Description:
            </label>
        );

        return (
        <div >   
        {this.props.fromPage==="Home" &&
          <Ads />
        }
        <div className="formContainer" style={{paddingBottom:window.innerHeight/10}}>
            <div className="form">
                <div className="form-title">
                    <h3>
                        {this.props.fromPage === "Idea" ?
                            this.props.title :
                            "Business Idea"
                        }
                    </h3>
                </div>
                <div>
                    <div><ListWords listWords={this.props.listWords} keyword={this.props.keywordN}/></div>
                    {this.props.fromPage === "Idea" ?
                        <ErrorMessageHandler errorMessage={this.state.errorMessage} /> :
                        <ErrorMessageHandler errorMessage={this.props.errorMessage} />
                    }
                    <hr className="form-hr" />
                    <form className="goAnalise">
                        <div>
                            {this.props.fromPage === "Idea" ?
                                editLabel :
                                normalLabel
                            }

                            {this.props.fromPage === "Idea" && !this.state.editMode ?
                                <textarea className="form-description"  rows="5" value={this.state.value}
                                          onChange={this.handleChange} maxLength="500" disabled/> :
                                <textarea className="form-description"  rows="5" value={this.state.value}
                                          onChange={this.handleChange} maxLength="500" />
                            }
                            <div className="charcount" style={{color:this.state.color}}>Characters Left: {this.state.chars_left}</div>
                        </div>
                    <div className="recaptcha_div">
                    <ReCAPTCHA
                    sitekey={captcha_site}  
                    ref={(el) => { captcha = el; }}
                    onChange={this.captchaChange}
                    />
                    </div>
                    <div className="filter_results">
                    <h3>Filter Results:</h3>
                    <CountryDropdown
                    className="normal_text"
                    value={this.state.country}
                    onChange={this.selectCountry}
                    defaultOptionLabel="Filter by country(optional)"
                    valueType="short" />
                    {this.state.country!=="" &&
                    <span>
                    <p className="normal_text">
                        Analyze in chosen country language{' '}{' '}
                        <input
                        name="translate"
                        type="checkbox"
                        checked={this.state.isChecked}
                        defaultChecked={!!+this.state.translation}
                        onChange={this.handleInputChange}/>
                    </p>
                    </span> 
                    }
                   
                    </div>

                    <div className="panel-footer">
              
                    { this.props.fromPage==="Idea" && 
                        <div className="col-xs-12 col-xs-offset-1">
                            <Reanalyze
                                synopsis={this.state.value}
                                title={this.props.title}
                                listWords={this.props.listWords}
                                keywordN={this.props.keywordN}
                                country={this.state.country}
                                translation={this.state.translation}
                                validCaptcha={this.state.validCaptcha}
                                errorMessage={(message) => this.updateErrorMessage(message)}
                            />
                            <ShareIdea ideaid={this.props.ideaId}/>
                        </div>
                    }
                    {this.props.fromPage==="Home" &&
                    <div>
                        <div className="col-xs-6">
                            <button className="btn btn-lg title_font" onClick={this.props.synopsisHandler.bind(this,"words",this.state.value,"",0)}>New Words</button>
                        </div>
                        <div className="col-xs-6">
                            <button type="submit" disabled={!this.state.validCaptcha} className="btn btn-lg title_font" id="btn-analyze" onClick={this.props.synopsisHandler.bind(this,"results",this.state.value,this.state.country,this.state.translation)}>Analyze</button>
                        </div>
                        </div>
                    }
                    </div>
                    </form>
                </div>
            </div>
        </div>
      
        </div>
        );
    }
}

export default Form;
