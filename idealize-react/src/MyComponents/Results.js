import React, {Component} from 'react';
import './Results.css';
import {api_url} from "../App";
import Ads from "./Ads";
import ReactLoading from 'react-loading';
import {TrendsGraph} from './TrendsGraph';

function saveResultsInfo(props){
    if(!props.isLogged){
        return (
            <div className='saveresultsinfo'><p>You need to be
                <span> logged in </span>
                to save the idea in your personal portfolio, if you don't have an account
                <span> register here</span>
            </p></div>);
    } else {
        return "";
    }

}

function saveIdeaTitle(props) {
    if (props) {
        return (
            <div className='saveIdeaTitle'>
                <label>
                    Idea Title :
                    <input onClick={hideInfo.bind(this)} type="text" name="ideatitle" placeholder=" Write a Title For Your Idea" id="ideatitle" size="40" />
                </label>
                <p id="ideatitleinfo">Please enter Idea Title - At Least 3 Characters with Limit of 30</p>
            </div>);
    } else {
        return "";
    }

}
function hideInfo(){
    var ideatitleinfo = document.getElementById("ideatitleinfo");
    ideatitleinfo.style.display = "none";
}

//eslint-disable-next-line
function handleLoggedIn(props){
    var dropdown = document.getElementById("nav-dropdown");
    dropdown.setAttribute("aria-expanded","true");
    var dropdownClass = dropdown.parentNode.classList;
    dropdownClass.add('open');
}

export function ListGraphs(props) {
    const graphdata1 = props.graphs.trends;
	const indexesdata = props.graphs.indexes;
    let listGraphs = [];
    if(graphdata1===undefined)
        return null;
    const graphdata = JSON.parse(graphdata1);
   
	if(indexesdata!==undefined)
	{
		const indexes = JSON.parse(indexesdata);
		listGraphs.push(<h1 key={indexes.response.avgindice}>Average Index: {indexes.response.avgindice}/10</h1>);
	}
    for(let i=0;i<graphdata.length;i++){
         if(graphdata[i]['data'][0].length>0)
            listGraphs.push(<TrendsGraph key={graphdata[i]['keyword']} data={graphdata[i]['data']} name={graphdata[i]['keyword']} />)
			if(indexesdata!==undefined)
			{
				const indexes2 = JSON.parse(indexesdata);
				listGraphs.push(<h3 key={i}>Index: {JSON.parse(indexes2.response.keysindice[i])}/10</h3>);
			}
    }
    return (
        <div style={{paddingBottom:100}}>{listGraphs}</div>
    );
}



export class Results extends Component {
    constructor(props) {
        super(props);

        let logged = props.isLogged;
		let synopsis = props.synopsis;
		let keywordN = props.keywordN;
		let listWords = props.listWords;
        let country = props.country;
        let translation = props.translation;

        this.state = {
            graphdata: [],
            isFetching: true,
            isLogged: logged,
            htmlSave: "",
			synopsis: synopsis,
			keywordN: keywordN,
            listWords: listWords,
            country:country,
            translation:translation,
            windowHeight: window.innerHeight,
            windowWidth:window.innerWidth
        };
        this.handleChange= props.resultsHandler.bind(this);
        this.resultsSaveHandler = function(){
            if(this.state.isLogged) {
                if (this.state.htmlSave === "") {
                    this.setState({
                        htmlSave: saveIdeaTitle(true)
                    });
                } else {
                    var value = document.getElementById("ideatitle").value;
                    if(value.length < 3){
                        var ideatitleinfo = document.getElementById("ideatitleinfo");
                        ideatitleinfo.style.display = "inline-block";

                    } else {
                        if(value.length > 30){
                            var res = value.substring(0, 31);
                            this.sendTitle(res);
                        } else {this.sendTitle(value);}
                    }


                }
            } else {
                this.setState({
                    htmlSave: saveResultsInfo(false)
                });
            }
        }
    }
  

    componentWillMount(){
        this.fetchTrends();
    }  
   
    componentWillReceiveProps(nextProps){
        if(this.props.isLogged!==nextProps.isLogged)
            this.setState({
                isLogged: nextProps.isLogged,
                htmlSave:""
            });
    }

        sendTitle(myIdeaTitle){  //rename to sendIdea
		let graphdata = JSON.stringify(this.state.graphdata);
		let title = myIdeaTitle;
		let synopsis = this.state.synopsis;
		let keywords = this.state.listWords;
        let keyword = this.state.keywordN;
        let country = this.state.country;
        let translation = this.state.translation;

		let toSend = {
			"title": title,
			"synopsis": synopsis,
			"main_keyword": keyword,
			"other_keywords": keywords,
            "trends": graphdata,
            "country": country,
            "translation":translation
		}
		
		fetch(api_url + 'idea', {
					method: 'POST',
					headers: new Headers({
						'Accept': 'application/json',
						'Content-Type': 'application/json'
					}),
					body: JSON.stringify(toSend),
					credentials: 'include'
				})
					.then(function(response) {
						if(response.status===400 || response.status === 404 ){
							//Resend???
						}
						else return response.json();
                    })
                    .then((json) => window.location.replace('/idea?id=' + json['id']))
					.catch(function(err){
						console.log(err);
					})
    }

    fetchTrends(){
        let synopsis = this.props.synopsis;
		let country = this.state.country;
        let translation = this.state.translation;
        let formData = "text_rank=" + synopsis + "&country=" + country + "&translation=" + translation;
        let self = this;
        //fetch('http://www.mocky.io/v2/5a29006e2e0000e211a09964',{
	    fetch(api_url + 'getStats', {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
            }),
            body: formData
        })
            .then(function(response) {
                if(response.status===204){
                    self.handleChange(synopsis,"No Results. Please add more to your text, use concise and relevant language.");
                }
                else if(response.status===404){
                    self.handleChange(synopsis,"404 Error - Try again");
                }
                else if(response.status===500){
                    self.handleChange(synopsis,"500 Internal Server Error - Try again");
                }
                else return response.json();
            })
            .catch(function(err){
                console.log(err);
                self.handleChange(synopsis,"Connection Error - Try again");                
            })
            .then((json) => this.refreshData(json))
    }

    refreshData(data) {
        this.setState({
            graphdata: data,
            isFetching: false,
        });
    }

    render() {

        return (
        
            <div className ="resultsContainer" style={{paddingBottom:window.innerHeight/10}}>
            <Ads />
            <div className="results">
                <div className="results-title"><h3>Results</h3></div>
                {this.state.isFetching &&
                <div style={{ paddingTop:100,paddingBottom:100}} >
                    <ReactLoading type="spin" color="#42A5F5" height={window.innerHeight/3} width={window.innerHeight/3} className="center-block" />
                </div>
                }
                <ListGraphs graphs={this.state.graphdata} />

                <div className="panel-footer" style={{position:'absolute',bottom:0,width:'100%'}}>
                    {this.state.htmlSave}
                    <div className="col-xs-6">
                        <button className="btn btn-lg title_font" onClick={this.props.resultsHandler.bind(this,this.props.synopsis,"")} >
                            Brainstorm
                        </button>
                    </div>
                    <div className="col-xs-6">
                        <button className="btn btn-lg title_font" onClick={this.resultsSaveHandler.bind(this)}>
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
       
        );
    }
}

export default Results;