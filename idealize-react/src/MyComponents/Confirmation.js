import React from 'react';
import { Button, Modal} from 'react-bootstrap';


export class Confirmation extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {showModal: props.showModal,  id: props.ID, type: props.type};

       
        this.close = this.close.bind(this);
        this.confirm = this.confirm.bind(this);
      }
    
     componentWillReceiveProps(props) {
         this.setState({showModal: props.showModal,  id: props.ID, type: props.type});
         //alert("componentWillReceiveProps: " + this.state.showModal);
     }
    
    
      close() {
          
          //this.setState({showModal: false});
          this.props.cancelAction();
          
      }
    
    
      confirm() {
          this.setState({showModal: false});
          switch(this.state.type) {
              case 'delete':
                  this.close();
                  this.props.removeItem(this.props.id);
                  break;
              case 'invite':
                  this.close();
                  this.props.acceptInvite(this.props.id, false);        // online used with reject invite
                  break;
              default:
                  this.close();
                  break;
          }
          
          
          
          
      }
    
      render() {
          
          return(
              
            <Modal className="modal2" show={this.state.showModal}  /*onHide={this.close}*/>
                 
                    
                  
                <Modal.Body className="col-xs-12" style= {{padding: "20", textAlign: "center"}}>
                    <h1 className="col-xs-12" style={{marginBottom: "40"}}>Are you sure?</h1>
                    <div className="col-xs-6">
                        <Button className="modalButton"  ref={input => this.inputElement = input} onClick={this.confirm}>Yes</Button>
                    </div>
                    <div className="col-xs-6">
                        <Button className="modalButton" onClick={this.close} >No</Button>
                    </div>
                </Modal.Body>
              
              
            </Modal>
          );
          
      }
    
    
    
}