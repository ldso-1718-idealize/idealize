import React from 'react';
import {SimpleSlider} from './SimpleSlider'



function Title(props) {
  return (
    <div className="col-sm-12 panel-title">
      <h3>{props.title}</h3>
    </div>
  );
}



let styles = {

  panelBody: {"minHeight": 300},
  hr: {"margin-bottom": 80}

}

export class SearchWord extends React.Component {

  constructor(props) {
    super(props);
    this.state = {value: '', buttomDisabled: true};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
  
    if(event.target.value.length > 2) {
      this.setState({value: event.target.value, buttomDisabled: false});
    }else {
      this.setState({value: event.target.value, buttomDisabled: true});
    }

  }

  handleSubmit(event) {


    this.props.handleSubmitSearchWord(this.state.value);

  }


  render() {
    return (
      <div style={{paddingTop:window.innerHeight/10}}>
      <div className="container backgroundContainer" style={{paddingBottom:window.innerHeight/10}}>
        <div className="panel panel-default">
          <div className="panel-body" style={styles.panelBody}>
            <div>
              <div className="about-content">
                <SimpleSlider/>
              </div>
            </div>
            <div id="custom-search-inpu">
              <br></br>
              <Title title="Insert a Keyword"/>

              <div className="input-group col-sm-push-2 col-sm-8" >
                <input type="text" className="search-query form-control" value={this.state.value} placeholder="Search" onChange={this.handleChange}/>
                <span className="input-group-btn">
                  <button disabled={this.state.buttomDisabled} onClick={this.handleSubmit} className="btn stop-2" type="button">
                    <span className=" glyphicon glyphicon-search"></span>
                  </button>
                </span>
              </div>



            </div>
          </div>
          <div className="panel-footer">
            <button style={{background: '#42A5F5', color: '#003399'}} onClick={this.handleSubmit}  disabled={this.state.buttomDisabled} type="button" className="btn btn-success btn-lg title_font">Search</button>
          </div>
        </div>
      </div>
    </div>


    );
  }
}
export default SearchWord;