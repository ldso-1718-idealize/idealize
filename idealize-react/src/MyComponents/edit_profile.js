import React from 'react';
import { Button, Modal, FormGroup, ControlLabel, FormControl, Alert} from 'react-bootstrap';
import { api_url } from '../App';


function FieldGroup(props ) {
    return (
        <FormGroup controlId={props.id} validationState={props.error}>
            <ControlLabel>{props.label}</ControlLabel>
            <FormControl {...props} />
        </FormGroup>
    );
}


export class EditProfile extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {showModal: props.showModal, emailLabel: "Email address", alertVisible: false, emailError: false,  firstname: props.firstName, lastname: props.lastName, email: props.email, firstnameLabel: "First Name", lastnameLabel: "Last Name"};

       
        this.close = this.close.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.showEmailWrong = this.showEmailWrong.bind(this);
        this.showSomethingWentWrong = this.showSomethingWentWrong.bind(this);
        this.handleAlertDismiss = this.handleAlertDismiss.bind(this);
        this.handleAlertShow = this.handleAlertShow.bind(this);
      }
    
     close() {
        this.props.update(this.state.firstname, this.state.lastname, this.state.email); 
      }

    componentWillReceiveProps(nextProps) {
        this.setState({showModal: nextProps.showModal});
    }
    
    handleChange(event, type) {
        
        switch(type){
            case 'firstname':
                this.setState({firstname: event.target.value});
                break;
            case 'lastname':
                this.setState({lastname: event.target.value});
                break;
            case 'email':
                this.setState({email: event.target.value});
                break;
            default:
                break;
        }    
        

    }
    
    showEmailWrong() {
        
        this.setState({emailLabel: "Email address is incorrect", emailError: true});
    }
    
    showSomethingWentWrong() {
        this.handleAlertShow();
        return;
    }
    
    handleAlertDismiss() {
        this.setState({ alertVisible: false });
    }

    handleAlertShow() {
        this.setState({ alertVisible: true });
    }
    

    handleSubmit(event) {

          this.setState({emailLabel: "Email address", emailError: false, firstnameLabel: "First Name", lastnameLabel: "Last Name"  });
          this.handleAlertDismiss();
        
        
           var error = false;    
          if(!this.state.email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
              this.setState({email: ""});
              this.showEmailWrong();
              error = true;
          }
        
          if(this.state.firstname.length === 0){
              this.setState({firstnameLabel: "First name cannot be empty"});
              error = true;
          }
        
          if(this.state.lastname.length === 0) {
              this.setState({lastnameLabel: "Last name cannot be empty"});
                error = true;      
          }   
        
         if(error)
             return;

        
          var request = 'firstname=' + this.state.firstname + '&lastname=' + this.state.lastname + '&email=' + this.state.email;
         
          fetch(api_url + 'editProfile', {
            //fetch('http://www.mocky.io/v2/59f9e78c370000920d66b895', { //401
           // fetch('http://www.mocky.io/v2/59f9eae13700001d0e66b8a6', {   //200
				credentials: 'include',
                method: 'POST',
                headers: new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded',
                }),
                body: request
            }).then(function(response) {
                if (response.ok){
                    return response.text();
                }
                throw new Error('Network response was not ok.');
            }).then(() => {this.close()})
                .catch(() => this.showSomethingWentWrong(), // chamar msg de erro
                    
                    function(error) {
                        console.log('Problem with fetch: ' + error.message);
            });        

      }

    
    
    
    
    render() {
        return (
        <Modal className="modal2" show={this.state.showModal} /*onHide={this.close}*/>
                  <Modal.Header  closeButton onClick={this.props.closeModal}>
                    <Modal.Title>Edit Profile</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                     <form className={"login-form"}>
                         <FieldGroup
                            id="formControlsFirstName"
                            type="text"
                            name={this.state.firstnameLabel}
                            label={this.state.firstnameLabel}
                            error={this.state.firstnameLabel === "First Name" ? null : "error"}
                            placeholder="Enter your first name"
                            value={this.state.firstname}
                            onChange={(e) => this.handleChange(e, 'firstname')}
                        />
                        <FieldGroup
                            id="formControlsLastName"
                            type="text"
                            name={"lastname"}
                            label={this.state.lastnameLabel}
                            placeholder="Enter your last name"
                            error={this.state.lastnameLabel === "Last Name" ? null : "error"}   
                            value={this.state.lastname}
                            onChange={(e) => this.handleChange(e, 'lastname')}
                        />
                        <FieldGroup
                            id="formControlsEmail"
                            type="email"
                            name={"email"}
                            label={this.state.emailLabel}
                            error={this.state.emailError ? "error" : null}    
                            placeholder="Enter email"
                            value={this.state.email}
                            onChange={(e) => this.handleChange(e, 'email')}
                        />

                    </form>    
                  </Modal.Body>
                  <Modal.Footer>
                       {(() => {
                            if(this.state.alertVisible === true){
                                return (<Alert bsStyle="danger" onDismiss={this.handleAlertDismiss}>
                                            <h4>Oh snap! Something went wrong, try again later!</h4>        
                                        </Alert> );
                            }
                            return null;
                            
                        })()}
                     
                    
                    <div className="col-sm-6">
                        <Button className="modalButton" id="save" ref={input => this.inputElement = input} onClick={this.handleSubmit}>Save</Button>
                    </div>
                    <div className="col-sm-6">
                        <Button className="modalButton" onClick={this.props.closeModal} >Close</Button>
                    </div>
                  </Modal.Footer>
        </Modal>);

    }


     
     
}