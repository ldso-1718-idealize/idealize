import React from 'react';
import { Button} from 'react-bootstrap';
import ReactLoading from 'react-loading';
import {api_url} from "../App";


export class Reanalyze extends React.Component {
    
    constructor(props) {
        super(props);
        let country=props.country===undefined ? "":props.country;
        let translation=props.translation===undefined ? 0:props.translation;
        this.state = {
            showModal: props.showModal,
            alertVisible: false,
            synopsis: props.synopsis,
            country: country,
            translation: translation,
            title: props.title,
            listWords: props.listWords,
            keywordN: props.keywordN,
            isFetching: false,
            validCaptcha:props.validCaptcha,
            errorMessage: ""};
		

        this.close = this.close.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.showSomethingWentWrong = this.showSomethingWentWrong.bind(this);
        this.handleAlertDismiss = this.handleAlertDismiss.bind(this);
        this.handleAlertShow = this.handleAlertShow.bind(this);
        this.selectCountry=this.selectCountry.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
      }
    
    close() {
        this.setState({ showModal: false });
    }

    open() {
        this.setState({showModal: true});
    }

    componentWillReceiveProps(nextProps) {
        this.setState({showModal: nextProps.showModal});

        if(this.props.synopsis!==nextProps.synopsis)
            this.setState({
                synopsis:nextProps.synopsis
            });

        if(this.props.country!==nextProps.country)
            this.setState({
                country:nextProps.country
            });
        if(this.props.validCaptcha!==nextProps.validCaptcha)
            this.setState({
                validCaptcha:nextProps.validCaptcha
            });  
        if(this.props.translation!==nextProps.translation)
            this.setState({
                translation:nextProps.translation
            });
    }
    
    handleChange(event, type) {
        
        switch(type){
            case 'synopsis':
				var input = event.target.value;
				var myColor = 'black';
				if(input.length === 500){
					myColor = 'red';
				}
				this.setState({
					synopsis:input,
					chars_left: 500 - input.length,
					color: myColor
				});
                break;
            default:
                break;
        }    
        

    }
	
	selectCountry (val) {
        if(val==="")
            this.setState({ country: val, translation:0 });    
        else this.setState({ country: val });
       
    }
	
	handleInputChange(event){
        this.setState({ translation: +event.target.checked });
    }
    
    showSomethingWentWrong() {
        this.handleAlertShow();
        return;
    }
    
    handleAlertDismiss() {
        this.setState({ alertVisible: false });
    }

    handleAlertShow() {
        this.setState({ alertVisible: true });
    }
    

    handleSubmit(event) {

		let title = this.state.title;
		let synopsis = this.state.synopsis;
		let keywords = this.state.listWords;
        let keyword = this.state.keywordN;
        let country = this.state.country;
        let translation = this.state.translation;
        let self = this;
		let toSend = {
			"title": title,
			"synopsis": synopsis,
			"main_keyword": keyword,
			"other_keywords": keywords,
            "country": country,
            "translation":translation
		};
        this.setState({
            isFetching:true
        });
		fetch(api_url + 'reanalyze', {
					method: 'POST',
					headers: new Headers({
						'Accept': 'application/json',
						'Content-Type': 'application/json'
					}),
					body: JSON.stringify(toSend),
					credentials: 'include'
				})
					.then(function(response) {
                        console.log("here");
						if(response.status===400 || response.status === 404 ){
                            this.setState({
                                isFetching:false
                            });
						}
						else if(response.status===204){
                            this.setState({
                                isFetching:false
                            });
                            self.props.errorMessage("No Results. Please add more to your text, use concise and relevant language.");
							self.setState({ errorMessage: "No Results. Please add more to your text, use concise and relevant language."});
						}
						else
						{
							return response.json();
						}
                    })
                    .then((json) => window.location.replace('/idea?id=' + json['id']+'?reanalyzed'))
					.catch((err)=> {
                        this.setState({
                            isFetching:false
                        });
                        self.props.errorMessage("Connection Error - Make sure you have internet connection.");
					})
	}
    
    render() {
        return (
            <span>
                {!this.state.isFetching &&
                <Button
                    className="btn btn-lg title_font"
                    id="btn-analyze"
                    disabled={!this.state.validCaptcha}
                    onClick={this.handleSubmit}
                >
                    Reanalyze
                </Button>}
                {this.state.isFetching &&
                <div className="col-xs-6 col-xs-offset-2">
                <ReactLoading type="spin" color="#42A5F5"  className="center-block" />
                </div>
                }
            </span>
        );

    }


     
     
}