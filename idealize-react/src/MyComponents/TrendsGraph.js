import React, {Component} from 'react';
import './Results.css';
import {LineChart} from 'react-easy-chart';

export class TrendsGraph extends Component {

    constructor(props) {
        super(props);
        let initialWidth;
        if(window.innerWidth>790){
            initialWidth = window.innerWidth;
        }
        else initialWidth = 800;
        this.state =
            {
                windowWidth: initialWidth - 100
            };
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResize.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
    }

    handleResize() {
        if(window.innerWidth>790)
            this.setState({windowWidth: window.innerWidth - 100});
        else this.setState({windowWidth: 800});
    }

    render() {
        return (
            <div className="table-responsive">
                <h3 style={{textAlign:"center"}}>{this.props.name}</h3>
                <LineChart
                    xType={'time'}
                    axes
                    grid
                    verticalGrid
                    width={this.state.windowWidth/2/1.3}
                    height={this.state.windowWidth/3/1.5}
                    axisLabels={{x: 'Time', y: 'Interest'}}
                    mouseOverHandler={this.mouseOverHandler}
                    mouseOutHandler={this.mouseOutHandler}
                    mouseMoveHandler={this.mouseMoveHandler}
                    tickTimeDisplayFormat={'%b %y'}
                    lineColors={['blue']}
                    interpolate={'linear'}
                    data={this.props.data}
                />
            </div>
        );
    }
}

export default TrendsGraph;