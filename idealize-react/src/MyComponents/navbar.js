import  React, { Component } from 'react';
import { Navbar, Nav} from 'react-bootstrap';
import {LoginDropdown} from "./login_dropdown";
import 'font-awesome/css/font-awesome.min.css';

export class NavB extends Component {

  constructor(props) {
    super(props);

    this.updateLogin= props.updateLogin.bind(this);
  }

  render() {

    const navBrandStyle = {
      paddingLeft: "0.5em",
      paddingTop: "0",
      paddingBottom: "0",
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      left: "0%",
      position: "absolute"
    };

    var right = (
        <Nav id="nav-right">
            <div className="socialDiv">
                <div className="fb-share-button" data-href="https://projectidealize.me/" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a className="fa fa-3x fa-facebook-square fb-xfbml-parse-ignore" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fprojectidealize.me%2F&amp;src=sdkpreparse">{' '}</a></div>
                <a className="fa fa-3x fa-twitter-square" href="https://twitter.com/intent/tweet?url=https%3A%2F%2Fprojectidealize.me%2F&text=Are%20you%20an%20entrepeneur?%20Check%20Idealize%20Now!">{' '}</a>
            </div>
            <LoginDropdown  updateLogin={(state) => this.handleLoginUpdate(state)} loginUpdated={this.props.loginUpdated}/>
        </Nav>
    );
    
    return(
      <Navbar id="nav" {...this.props.sticky ? {fixedTop:true} : {}} inverse collapseOnSelect>
          <Navbar.Brand className="navbar-brand">
            <a href="/" style= {navBrandStyle} onClick={() =>
                {let tips=localStorage.getItem('tips')===null ? false : localStorage.getItem('tips')==="true";
                  localStorage.clear();
                  localStorage.setItem('tips',tips);}}>
                  <img src={require("../images/logowhiteletters.png")} alt="logo" style={{width: '28%', height: '28%', float: 'left', paddingRight: '0.5em'}} />
                  <span className="title_font" style={{color: 'white'}}>Idealize</span>
                </a>
              </Navbar.Brand>
          {!this.props.hideRight && right}
          </Navbar>
        );
      }

      handleLoginUpdate(state) {
        this.props.updateLogin(state);
      }
    }

export default NavB;