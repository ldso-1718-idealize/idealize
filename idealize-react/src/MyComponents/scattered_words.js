import React from 'react';
import 'whatwg-fetch';
import Ads from "./Ads";
import {wordnik_key} from "../App";


function getRandomColor(word) {
    var rand = require('random-seed').create({word});
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (var i = 0; i < 6; i++) {
        var randValue = rand.random();
        color += letters[Math.floor(randValue * 16)];
    }
    return color;
}

function getBrightness(color){
  let c = color.substring(1);      // strip #
  let rgb = parseInt(c, 16);   // convert rrggbb to decimal
  let r = (rgb >> 16) & 0xff;  // extract red
  let g = (rgb >>  8) & 0xff;  // extract green
  let b = (rgb >>  0) & 0xff;  // extract blue

  let luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709

  return luma;
}

function ListItem(props) {
    let color = getRandomColor(props.value);
    let font;
    if(getBrightness(color)>200)
      font = "	#778899";
    else font = "#ffffff";

    let style = {
      background: color,
      color: font,
      borderRadius: 5,
      padding:10
    };

    return (
      <label style={style}  className={"normal_text"}>{props.value}{'  '}<span className="glyphicon glyphicon-arrow-right"></span>{'  '} {props.center}</label>
    );
}
function   remove(array, element) {
  return array.filter(e => e !== element);
}

export class Words extends React.Component{
  constructor(props) {
    super(props);
    this.state = {words:[],checkedCount:0,checkedWords:[]};
    this.handleChange= this.handleChange.bind(this);
    // This binding is necessary to make `this` work in the callback
    this.fetchWords= this.fetchWords.bind(this);
  }
  

  componentDidMount(){
    this.fetchWords(10);
    
  }

  fetchWords(nrWords){
    let list = [];
   fetch('https://api.wordnik.com/v4/words.json/randomWords?hasDictionaryDef=true&minCorpusCount=10000&includePartOfSpeech=noun,adjective&minLength=4&maxLength=20&limit='+nrWords+'&api_key='+wordnik_key)
    .then(function(response) {
       return response.json();
    })
    .then(function(json) {
      for(let i=0;i<json.length;i++){
        list.push(json[i].word);
      }

    })
    .catch(function(error) {
      console.log("Unexpected response: " + error.message);
    })
    .then(() => this.refreshWords(list))

  }
  
  refreshWords(list){
    this.setState({
      words:list
    })
  }

  handleChange(e) {
    if(e.target.checked){
      if (this.state.checkedCount===2){
        let newWords=this.state.checkedWords;
        newWords.splice(-1,1);
        newWords.push(e.target.name);
        this.setState({
          checkedWords: newWords
        });
      }
      else{
        let newWords=this.state.checkedWords;
        newWords.push(e.target.name);
        this.setState({
          checkedCount: newWords.length,
          checkedWords: newWords
        });
      }  
    }
    else {
      let newWords= remove(this.state.checkedWords,e.target.name)
      this.setState({
        checkedCount: newWords.length,
        checkedWords: newWords
      });
    }     
}
 checked(word){
   return this.state.checkedWords.indexOf(word) > -1
 }

  render() {
    let listItems = this.state.words.map((word) =>
    <tr  key={word}>
   
    <td> <ListItem value={word} center={this.props.centerWord} /> </td>
    <td> <input onChange={this.handleChange} checked={this.checked(word)} type='checkbox' name={word} key={word}/> </td>
   
    </tr>
    );

    return (
      <div className="wordsContainer" style={{paddingBottom:window.innerHeight/10,paddingTop:window.innerHeight/20}} >
      <Ads />
      <div className="words-title"><h2>Choose Up to 2 Associations for Brainstorming</h2></div>
      <table className="table  table-bordered table-responsive" >
      <thead>
      <tr>
        <th>Associations</th>
        <th>Checks</th>
      </tr>
      </thead>
      <tbody> 
     
        {listItems}
      
       
      </tbody>
      </table>
      <div style={{margin:'0 auto',paddingTop:window.innerHeight/25,overflow:'hidden'}}>
          <div className="col-xs-6">
            <button className="btn btn-lg stop-4 title_font" onClick={this.fetchWords.bind(this,10)} >
              Regenerate
            </button>
          </div>
          <div className="col-xs-6" >
            <button className="btn btn-lg stop-5 title_font" disabled={this.state.checkedCount===0} onClick={this.props.brainstormHandler.bind(this,"synopsis",this.state.checkedWords)} >
              Brainstorm
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Words;