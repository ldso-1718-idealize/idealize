import  React, { Component } from 'react';
import './footer.css';


export class Footer extends Component {

  render() {   
    return (
        <div>
            <div id="footer">
            <div id="partners_div">
            <span className="title_font">Partners:</span>
            <img id="feup_logo" src={require("../images/logofeup.png")} alt="logofeup" />
            </div> 
            <div id="footer_contacts_div">
            <a href="contacts" id="contacts" className="navlinkto">Contacts</a>
            |
            <a href="about" id="about" className="navlinkto">About</a>
            <span id="copyright">Copyright &#169;  2017/2018 </span>
            </div>
            </div>
        </div>
    );
}
}

export default Footer;