import React, {Component} from 'react';
import { Modal, FormGroup, FormControl, ControlLabel, Button, InputGroup } from 'react-bootstrap';
import SwitchButton from 'react-switch-button/lib/react-switch-button.js';
import './Form.css';


//eslint-disable-next-line
import {api_url} from "../App";



export class ShareIdea extends Component {
    constructor(props) {
        super(props);

        this.state = {
            ideaid: this.props.ideaid,
            hasTeam: false,
            teams: [],
            selectedTeam: '',
            emailValid: false,
            shareToEmail: [
                {
                    index: 0,
                    value: '',
                    valid: false,
                }
            ],
            menuOpen: false,
            failedAttempt: false,
            successfulAttempt: false,
        };
    }

    render() {

        var shareByEmail = (
            <form className="login-form">
                {this.state.shareToEmail.map(item =>
                    <FormGroup key={item.index}>
                        {item.index === 0 && <p>Email</p>}
                        <InputGroup>
                            <FormControl
                                type="email"
                                className="formControlsUsername"
                                value={item.value}
                                onChange={(e) => this.handleEmailInput(e, item.index)}
                                onKeyPress={(event) => this.handleKeyPress(event)}
                            />
                            <InputGroup.Button>
                                <Button onClick={() => this.onRemoveEmail(item.index)}>
                                    Remove
                                </Button>
                            </InputGroup.Button>
                        </InputGroup>
                    </FormGroup>
                )}
                <Button id="login-btn"
                        onClick={() => this.onSubmitEmail()}
                        disabled={!this.state.emailValid}
                >
                    Share
                </Button>
                <Button id="login-btn"
                        onClick={() => this.onNewEmail()}
                >
                    New Email
                </Button>
                {this.state.failedAttempt ?
                    <p id="reset-password">
                        An error has occurred. Please try again
                    </p> :
                    null
                }
                {this.state.successfulAttempt ?
                    <p id="reset-password">
                        Idea successfully shared!
                    </p> :
                    null
                }
            </form>
        );

        var shareToTeam = (
            <form className="login-form">
                <FormGroup controlId="formControlsSelect">
                    <ControlLabel>Choose Team</ControlLabel>
                    <FormControl
                        componentClass="select"
                        onChange={(e) => this.handleSelectChange(e)}
                    >
                        {this.state.teams.length ?
                            this.state.teams.map(team =>
                                <option value={team.id}>{team.name}</option>
                            ) :
                            "Loading..."
                        }
                    </FormControl>
                    <Button id="login-btn"
                            onClick={() => this.onSubmitTeam()}
                    >
                        Share
                    </Button>
                    {this.state.failedAttempt ?
                        <p id="reset-password">
                            An error has occurred. Please try again
                        </p> :
                        null
                    }
                    {this.state.successfulAttempt ?
                        <p id="reset-password">
                            Idea successfully shared!
                        </p> :
                        null
                    }
                </FormGroup>

            </form>
        );

        return (
            <div className="edit-button">
                <button className="edit-button" onClick={(e) => this.open(e)}>
                    <img
                        src={require("../images/share-button.png")}
                        style={{width: '30px', height: '30px'}}
                        alt=""/>
                </button>
                <Modal
                    show={this.state.menuOpen}
                    onHide={() => this.close()}
                    id="mymodal"
                >
                    <SwitchButton
                        className={"login-switch"}
                        name="switch-4"
                        labelRight="Share with my team"
                        defaultChecked={false}
                        onChange={() => this.changeForms()}
                    />
                    {!this.state.hasTeam ?
                        shareByEmail
                        :
                        shareToTeam
                    }
                </Modal>
            </div>
        );
    }

    componentWillMount() {
        //fetch("http://www.mocky.io/v2/5a31b0cd2e00006e35e3b591", {
        fetch(api_url + 'teams', {  //words + .... + graphs
            method: 'GET',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
            }),
            credentials: 'include'
        })
            .then(function (response) {

                return response.json();
            })
            .catch(function (err) {
                console.log(err);
            })
            .then((json) => this.getTeams(json));
    }

    getTeams(teams) {
        if(teams===undefined)
            return;
        let list = [];
        for (let i = 0; i < teams.length; i++) {
            list.push({
                name: teams[i].name,
                id: teams[i].id,
            });
        }
        this.setState({
            teams:list
        })
    }

    close() {
        this.setState({
            menuOpen: false,
            failedAttempt: false,
            successfulAttempt: false,
            shareToEmail: [
                {
                    index: 0,
                    value: '',
                }
            ],
        });
    }

    open(e) {
        e.preventDefault();
        this.setState({menuOpen: true,
            hasTeam: false,
            shareToEmail: [
                {
                    index: 0,
                    value: '',
                }
            ],
            selectedTeam: '',
        });
    }

    changeForms(){
        this.setState({hasTeam: !this.state.hasTeam})
    }

    onRemoveEmail(index) {
        if (this.state.shareToEmail.length > 1){
            let list = [];

            for (let i = 0; i < this.state.shareToEmail.length; i++) {
                if (i > index) {
                    list.push({
                        index: i-1,
                        value: this.state.shareToEmail[i].value,
                        valid: this.state.shareToEmail[i].valid,
                    })
                } else if (i !== index){
                    list.push(this.state.shareToEmail[i]);
                }
            }

            this.setState({shareToEmail: list});
        }
        else {
            console.log("No sir");
        }

    }

    handleEmailInput(e, index) {
        let value = e.target.value;
        let match = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        let valid = false;
        if (match !== null) {
            valid = true;
        }

        let list = this.state.shareToEmail;
        list[index] = {
            index: list[index].index,
            value: value,
            valid: valid,
        };

        let emailValid = true;
        for (let i = 0; i < list.length; i++) {
            if (list[i].valid === false) {
                emailValid = false;
                break;
            }
        }

        this.setState({emailValid: emailValid,
            shareToEmail: list});
    }

    handleSelectChange(e) {
        let value = e.target.value;
        this.setState({selectedTeam: value});
    }

    handleKeyPress(event) {
        if (event.key === 'Enter'){
            event.preventDefault();
            this.onNewEmail();
        }
    }

    onNewEmail() {
        let emails = this.state.shareToEmail;
        emails.push({index: emails.length, value: '', valid: false});
        this.setState({shareToEmail: emails, emailValid: false});
    }

    onSubmitEmail(){
        let list = [];
        for (var i = 0; i < this.state.shareToEmail.length; i++) {
            list.push(this.state.shareToEmail[i].value);
        }

        let toSend = {
            "id": this.state.ideaid,
            "emails": list,
        };
        let self = this;

        fetch(api_url + 'share', {
        //fetch("http://www.mocky.io/v2/5a330536310000da1338bbd2", {
            method: 'POST',
            headers: new Headers({
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify(toSend),
            credentials: 'include'
        }).then(function(response) {
            if (response.ok){
                return response.text();
            }
            throw new Error('Network response was not ok.');
        }).then(function() {
            console.log("Success");
            self.setState({
                failedAttempt: false,
                successfulAttempt: true,
            });
        }).catch(function(error) {
            self.setState({
                failedAttempt: true,
                successfulAttempt: false,
            });
            console.log('Problem with fetch: ' + error.message);
        });

    }

    onSubmitTeam(){
        if (this.state.teams.length > 0){
            let teamid;
            let self = this;

            if (this.state.selectedTeam === '') {
                console.log(this.state.teams[0]);
                teamid = this.state.teams[0].id;
            }
            else {
                console.log(this.state.selectedTeam);
                teamid = this.state.selectedTeam;
            }

            var formData = "teamid=" + teamid + "&ideaid=" + this.state.ideaid;

            fetch(api_url + 'team/addIdea', {
            //fetch('http://www.mocky.io/v2/59f9eae13700001d0e66b8a6', {   //200
                method: 'POST',
                credentials: 'include',
                headers: new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded',
                }),
                body: formData
            }).then(function(response) {
                if (response.ok){
                    return response.text();
                }
                throw new Error('Network response was not ok.');
            }).then(function() {
                self.setState({
                    failedAttempt: false,
                    successfulAttempt: true,
                });
            }).catch(function(error) {
                self.setState({
                    failedAttempt: true,
                    successfulAttempt: false,
                });
                console.log('Problem with fetch: ' + error.message);
            });
        }
    }
}
export default ShareIdea;

/*
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


function Email1(){
    var element = document.getElementById("email1");
    element.style.background = "#FFFFFF"
}

function sendEmail(id){
    var shareidea = document.getElementById("shareidea");
    var emailTxt = [];
    for(var i = 0; i<shareidea.childNodes.length;i++){
        var input = shareidea.childNodes[i];
        emailTxt.push(input.value);
    }
	var count = 0;
    for(var j=0; j<emailTxt.length;j++){
        if(!validateEmail(emailTxt[j])){
            var index = j + 1;
            var element = document.getElementById("email"+index);
            element.style.background = "#F8FF49";
            count++;
        }
    }
	if(count === 0){
	   sendEmailList(emailTxt, id);
	}
}

function addEmail() {
    var shareidea = document.getElementById("shareidea");
    var listEmails  = shareidea.childNodes;
    var input = document.createElement("INPUT");
    input.type = "text";
    input.name = "email"+(listEmails.length+1);
    input.id = "email"+(listEmails.length+1);
    input.addEventListener("click", function(){
        input.style.background = "#FFFFFF";
    });

    shareidea.appendChild(input);

    if(listEmails.length === 2) {
        var btnEmail = document.getElementById("btnEmail");
        var removeTxt = document.createTextNode("Remove Email");
        var btn = document.createElement("BUTTON");
        btn.appendChild(removeTxt);
        btn.type = "button";
        btn.id = "removeEmail";
        btn.addEventListener("click", function(){
            if(listEmails.length>1){
                var index = (listEmails.length)-1;
                shareidea.removeChild(shareidea.childNodes[index]);
            }
            if(listEmails.length === 1){
                var element = document.getElementById("removeEmail");
                element.parentNode.removeChild(element);
            }
        });
        btnEmail.appendChild(btn);
    }


}

function sendEmailList(emailList, id){

    console.log(emailList);
    console.log(id);

    let toSend = {
        "id": id,
        "emails": emailList
    };

    fetch(api_url + 'share', {
        method: 'POST',
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }),
        body: JSON.stringify(toSend),
        credentials: 'include'
    })
        .then(function(response) {
            if(response.status===400 || response.status === 404 ){
                //Resend???
            }
            else
            {
                console.log("Email sent successfully");
            }
        })
        .then((json) => window.location.replace('/idea?id=' + json['id']))
        .catch(function(err){
            console.log(err);
        })
}
*/