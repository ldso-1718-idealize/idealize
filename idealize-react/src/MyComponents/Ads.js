import React, { Component } from 'react';

export class Ads extends Component {
    constructor(props) {
        super(props);
        this.state = {
            windowHeight: window.innerHeight,
            windowWidth:window.innerWidth
        };
    }
    
    
    componentDidMount() {
        window.addEventListener('resize', this.handleResize.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
    }

    handleResize() {
        this.setState({
            windowWidth: window.innerWidth,
            windowHeight: window.innerHeight
        });
    }

    render() {
        let render=
        (
           <div>
           { this.state.windowWidth>=1200 &&
                <div>
                <img style={{position:"fixed",right:"3%",paddingTop:this.state.windowHeight/5}} className="center-block" src={require("../images/ad.png")} alt="advertisement" />
                <img style={{position:"fixed",left:"3%",paddingTop:this.state.windowHeight/5}} className="center-block" src={require("../images/ad.png")} alt="advertisement" />
                </div>
            }
            </div>
            
        );
        return render;
    }
}
export default Ads;