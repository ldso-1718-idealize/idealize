import React, { Component } from 'react';
import 'whatwg-fetch';
import { FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';
import {api_url} from "../App";
import {captcha_site} from "../App";
import ReCAPTCHA from "react-google-recaptcha";
var captcha;
function FieldGroup(props) {
    return (
        <FormGroup controlId={props.id}>
            <ControlLabel>{props.label}</ControlLabel>
            <FormControl {...props} />
        </FormGroup>
    );
}

export class RegisterForm extends Component {
    constructor () {
        super();
        this.state = {
            firstname: '',
            lastname: '',
            username: '',
            email: '',
            password: '',
            verifyPassword: '',
            emailValid: false,
            goodPassword: false,
            passwordValid: false,
            formValid: false,
            validCaptcha:false,
            registerUnsuccessful: false
        };
        
        this.captchaChange = this.captchaChange.bind(this);
    }
   
    captchaChange (value){
        if(value!==null){
            this.setState({
                validCaptcha:true
            },() => {this.validateForm()});
        }
        else captcha.reset();
    };
  

    render() {
        return (
            <form className={"login-form"}>
                <FieldGroup
                    id="formControlsFirstName"
                    type="text"
                    name={"firstname"}
                    label="First Name"
                    placeholder="Enter your first name"
                    value={this.state.firstname}
                    onChange={(e) => this.handleInput(e, 'firstname')}
                />
                <FieldGroup
                    id="formControlsLastName"
                    type="text"
                    name={"lastname"}
                    label="Last Name"
                    placeholder="Enter your last name"
                    value={this.state.lastname}
                    onChange={(e) => this.handleInput(e, 'lastname')}
                />
                <FieldGroup
                    id="formControlsUsername"
                    type="text"
                    name={"username"}
                    label="Username"
                    placeholder="Enter your username"
                    value={this.state.username}
                    onChange={(e) => this.handleInput(e, 'username')}
                />
                <FieldGroup
                    id="formControlsEmail"
                    type="email"
                    name={"email"}
                    label="Email address"
                    placeholder="Enter email"
                    value={this.state.email}
                    onChange={(e) => this.handleInput(e, 'email')}
                />
                <FieldGroup
                    id="formControlsPassword"
                    label="Password"
                    type="password"
                    name={"password"}
                    placeholder="Minimum characters: 5"
                    value={this.state.password}
                    onChange={(e) => this.handleInput(e, 'password')}
                />
                <FieldGroup
                    id="formControlsPassword2"
                    label="Confirm your password"
                    type="password"
                    value={this.state.verifyPassword}
                    onChange={(e) => this.handleInput(e, 'verifyPassword')}
                    onKeyPress={(event) => this.handleKeyPress(event)}
                />
                <ReCAPTCHA
                sitekey={captcha_site}  
                ref={(el) => { captcha = el; }}
                onChange={this.captchaChange}
                />
                <Button id="register-btn"
                        disabled={!this.state.formValid}
                        onClick={() => this.onSubmit()}
                >
                    Register
                </Button>
                {this.state.registerUnsuccessful ?
                    <p id={"login-submit"}>Error! Try again</p> :
                    null
                }
            </form>
        );
    }

    handleInput(e, field) {
        let value = e.target.value;

        this.setState({[field]: value},
            () => {this.validateField(field, value)});
    }

    validateField(field, value) {
        let emailValid = this.state.emailValid;
        let goodPassword = this.state.goodPassword;
        let passwordValid = this.state.passwordValid;

        if (field === 'email') {
            emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        } else if (field === 'password'){
            goodPassword = value.length >= 5;
        } else if (field === 'verifyPassword'){
            passwordValid = (this.state.password === value);
        }

        this.setState({emailValid: emailValid,
                        goodPassword: goodPassword,
                        passwordValid: passwordValid},
            () => {this.validateForm()});
    }

    validateForm() {
        this.setState({formValid: this.state.emailValid && this.state.goodPassword && this.state.passwordValid && this.state.validCaptcha});
    }

    handleKeyPress(event) {
        if (event.key === 'Enter'){
            if (this.state.formValid){
                this.onSubmit();
            }
        }
    }

    unsuccessful(){
        captcha.reset();
        this.setState({registerUnsuccessful: true,
                        firstname: '',
                        lastname: '',
                        username: '',
                        email: '',
                        password: '',
                        verifyPassword: '',
                        emailValid: false,
                        goodPassword: false,
                        passwordValid: false,
                        formValid: false,
                        validCaptcha:false
        });

    }

    onSubmit() {
        var formData = "username=" + this.state.username +
            "&password=" + this.state.password +
            "&firstname=" + this.state.firstname +
            "&lastname=" + this.state.lastname +
            "&email=" + this.state.email;

        // eslint-disable-next-line
        let res;

        fetch(api_url + 'register', {
        //fetch('http://www.mocky.io/v2/59f9e78c370000920d66b895', { //401
        //fetch('http://www.mocky.io/v2/59f9eae13700001d0e66b8a6', {   //200
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
            }),
            body: formData
        }).then(function(response) {
            if (response.ok){
                return response.text();
            }
            throw new Error('Network response was not ok.');
		}).then(() => this.props.updateUser(this.state.username))
            .catch(() => this.unsuccessful(),
                function(error) {
                    console.log('Problem with fetch: ' + error.message);
        });
    }
}

export default RegisterForm;