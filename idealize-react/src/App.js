import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'react-switch-button/dist/react-switch-button.css';
import './index.css';
import './App.css';
import { Route } from 'react-router-dom';
import Home from './pages/Home/Home';
import Idea from './pages/Idea/Idea';
import Activate from './pages/Activate/activate';
import Reset from './pages/Reset/Reset'
import Teams from './pages/Teams/Teams'
import Share from './pages/Share/Share'
import About from './pages/About/About'
import Profile from './pages/Profile/Profile';
import Contacts from './pages/Contacts/Contacts'
import Background from './images/cubes.png';


var backStyle = {
  backgroundColor: "#4a7ce0",
  backgroundImage: "url(" + Background + ")",
};

export var api_url;
export var wordnik_key;
export var captcha_site;
export var captcha_secret;
if (process.env.NODE_ENV !== 'production') {
    api_url = process.env.REACT_APP_API_URL_DEV;
}
else api_url = process.env.REACT_APP_API_URL_PROD;
wordnik_key=process.env.REACT_APP_WORDNIK;
captcha_site=process.env.REACT_APP_RECAPTCHA_SITE_KEY;
captcha_secret=process.env.REACT_APP_RECAPTCHA_SECRET_KEY;

class App extends Component {


    render() {

        return (
            <div style={backStyle}>
                <Route exact={true} path="/" component={Home} />
                <Route exact={true} path="/idea" component={Idea} />
                <Route exact={true} path="/profile" component={Profile} />
                <Route exact={true} path="/activate" component={Activate}/>
                <Route exact={true} path="/reset" component={Reset}/>
                <Route exact={true} path="/share" component={Share}/>
                <Route exact={true} path="/teams" component={Teams}/>
                <Route exact={true} path="/contacts" component={Contacts}/>
                <Route exact={true} path="/about" component={About}/>
            </div>
        );
    }


}

export default App;
