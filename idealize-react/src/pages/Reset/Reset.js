import React, {Component} from 'react';
import 'whatwg-fetch';
import { FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';
import {NavB} from "../../MyComponents/navbar";
import 'bootstrap/dist/css/bootstrap.css';

// eslint-disable-next-line
import {api_url} from "../../App";

function FieldGroup(props) {
    return (
        <FormGroup controlId={props.id}>
            <ControlLabel>{props.label}</ControlLabel>
            <FormControl {...props} />
        </FormGroup>
    );
}

class Reset extends Component {
    constructor(props) {
        super(props);

        this.state = {
            hash: "",
            password: "",
            url: "api.projectidealize.me/reset",
            validHash: false,
            passwordChanged: false,
            goodPassword: false,
            passwordValid: false,
            verifyPassword: "",
            formValid: false,
            updateUnsuccessful: false,
            message: "Couldn't update your password. Click here to go back to the homepage",

        }
    }

    handleLoginUpdate(state) {
        this.setState({isLogged: state});
    }

    render() {

        var form = (
            <form className={"login-form"} id="update-password">
                <FieldGroup
                    id="formControlsPassword"
                    label="Password"
                    type="password"
                    name={"password"}
                    placeholder="Minimum characters: 5"
                    value={this.state.password}
                    onChange={(e) => this.handleInput(e, 'password')}
                />
                <FieldGroup
                    id="formControlsPassword2"
                    label="Confirm your password"
                    type="password"
                    value={this.state.verifyPassword}
                    onChange={(e) => this.handleInput(e, 'verifyPassword')}
                    onKeyPress={(event) => this.handleKeyPress(event)}
                />
                <Button id="register-btn"
                        disabled={!this.state.formValid}
                        onClick={() => this.resetPassword()}
                >
                    Update Password
                </Button>
                {this.state.updateUnsuccessful ?
                    <p id={"login-submit"}>Error! Try again</p> :
                    null
                }
            </form>
        );

        var link = (
            <a
                href="/"
                onClick={() => window.location.replace("/")}
                className="activate-link"
            >
                {this.state.message}
            </a>
        );

        var page = (
            <div id="page">
                <NavB updateLogin={(state, username) => this.handleLoginUpdate(state, username)} hideRight={true}/>
                {!this.state.passwordChanged ?
                    form :
                    link
                }
            </div>

        );

        if (this.state.validHash){
            return page;
        } else {
            return null;
        }
    }

    componentWillMount() {
        var res = this.props.history.location.search.split("?");

        var params = []; //[i][0]-> var_name; [i][1]-> var_value
        if (res.length !== 2) {
            window.location.replace("/");
        } else {
            for (var i = 1; i < res.length; i++) {
                params[i - 1] = res[i].split("=");
            }

            this.checkReset(params[0][1]);
        }

    }

    resetPassword() {
        var res = this.props.history.location.search.split("?");

        var params = []; //[i][0]-> var_name; [i][1]-> var_value

        for (var i = 1; i < res.length; i++) {
            params[i - 1] = res[i].split("=");
        }

        var formData = "hash=" + params[0][1] + "&password=" + this.state.password;

        fetch(api_url + 'updatePassword', {
        //fetch('http://www.mocky.io/v2/59f9e78c370000920d66b895', { //401
        //fetch('http://www.mocky.io/v2/5a1e9f982f00001727ee2eb4', {   //200
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
            }),
            body: formData
        }).then(function(response) {
            if (response.ok){
                return response.text();
            }
            throw new Error('Network response was not ok.');
        }).then(() => this.setState({passwordChanged: true,
                                    message: "Password successfully updated! Click here to go back to the homepage"}))
            .catch(() => this.unsuccessful(),
                function(error) {
                    console.log('Problem with fetch: ' + error.message);
                });
    }

    checkReset(hash) {
        var formData = "hash=" + hash;

        fetch(api_url + 'checkReset', {
        //fetch('http://www.mocky.io/v2/59f9e78c370000920d66b895', { //401
        //fetch('http://www.mocky.io/v2/5a1e9f982f00001727ee2eb4', {   //200
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
            }),
            body: formData
        }).then(function(response) {
            if (response.ok){
                return response.text();
            }
            throw new Error('Network response was not ok.');
        }).then(() => this.setState({validHash: true}))
            .catch(() => this.invalidHash(),
                function(error) {
                    console.log('Problem with fetch: ' + error.message);
                });
    }

    handleInput(e, field) {
        let value = e.target.value;

        this.setState({[field]: value},
            () => {this.validateField(field, value)});
    }

    handleKeyPress(event) {
        if (event.key === 'Enter'){
            if (this.state.formValid){
                this.resetPassword();
            }
        }
    }

    validateField(field, value) {
        let goodPassword = this.state.goodPassword;
        let passwordValid = this.state.passwordValid;

        if (field === 'password'){
            goodPassword = value.length >= 5;
        } else if (field === 'verifyPassword'){
            passwordValid = (this.state.password === value);
        }

        this.setState({goodPassword: goodPassword,
                passwordValid: passwordValid},
            () => {this.validateForm()});
    }

    validateForm() {
        this.setState({formValid: this.state.goodPassword && this.state.passwordValid});
    }

    unsuccessful(){
        this.setState({updateUnsuccessful: true,
            password: '',
            verifyPassword: '',
            goodPassword: false,
            passwordValid: false,
            formValid: false
        });

    }

    invalidHash(){
        this.setState({passwordChanged: true,
                        validHash: true,
                        message: "The link you clicked is not valid. Click here to go back to the homepage"})
    }

} export default Reset;