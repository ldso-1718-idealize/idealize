import React, { Component } from 'react';
import {NavB} from "../../MyComponents/navbar";
import './Teams.css';
import {Footer} from '../../MyComponents/Footer';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { slide as Menu } from 'react-burger-menu'
import { api_url } from '../../App';



export class Teams extends Component {
  constructor() {
    super();
    this.state = {
      teams: [],
      curTeam:null,
      curTeamIdeas:[],
      curTeamMembers:[],
      addmember:"",
      menuOpen:true,
      isAdmin: false,
      validFetch: false,
      errorMessage:"",
      errorMessageTeam:"",
      createTeam:"",
      errorColor:'green'

    };
    this.removeItem = this.removeItem.bind(this);
    this.changeTeam = this.changeTeam.bind(this);
  }

    handleLoginUpdate(state) {
        this.setState({isLogged: state});
    }

  componentDidMount() {
    this.fetchTeams();
  }
  fetchTeams(){
    fetch(api_url + 'teams', {
    //fetch('http://www.mocky.io/v2/5a2dcf6932000096386fa8cf', {
		credentials: 'include'
	})
    .then(function(response) {
      if(response.status===404){
        window.location.replace('/');
      }
      else if(response.status===400){
        window.location.replace('/');
      }
        return response.json();
    })
    .catch(function(err){
      console.log(err);
      //window.location.replace('/');
    })
    .then((json) => this.refreshData(json))
  }


  
  refreshData(data) {
    this.setState({
      teams: data,
      validFetch: true
    });
  }
    

  
removeItem(i,type) {

    let confirm= window.confirm("Are you sure you want to delete?");
    if(!confirm)
        return;
    let data;
    let fetch_url=api_url+"removeSharedIdea";
    
    if(type==="idea")
    {
		data = "teamid=" + this.state.curTeam['id'] + "&ideaid=" + i;
		fetch_url = api_url+"removeSharedIdea";
	}
    else
	{
		data = "teamid=" + this.state.curTeam['id'] + "&member=" + i;
		fetch_url = api_url+"removeMember";
	}
    //fetch('http://www.mocky.io/v2/59f9e78c370000920d66b895', { //401
       fetch(fetch_url, {  //real requests
    //fetch('http://www.mocky.io/v2/5a1e9f982f00001727ee2eb4', {   //200
        method: 'POST',
        headers: new Headers({
            'Content-Type': 'application/x-www-form-urlencoded',
        }),
        body: data,
		credentials: 'include'
    }).then(function(response) {
        if (response.ok){
            return response.text();
        }
        throw new Error('Network response was not ok.');
    }).then(() => {
        if(type==="idea"){
            let ideas=this.state.curTeam['ideas'].slice();
            for (let j = 0; j < ideas.length; j++) {
                
                if (ideas[j].id === i) {
                    
                    ideas.splice(j, 1);
                }
            }
            let aux = this.state.curTeam;
                aux['ideas'] = ideas;
            this.ListTeamIdeas(aux);
        }
        else{
            let members=this.state.curTeam['members'].slice();
            for (let j = 0; j < members.length; j++) {
                
                if (members[j].name === i) {
                    
                    members.splice(j, 1);
                }
            }
            let aux = this.state.curTeam;
                aux['members'] = members;
            this.ListTeamMembers(aux);
        }
        
    })
}

addMember(id, name) {
    let data ="id=" + id + "&username=" + name;
    let fetch_url=api_url+"teamInvite";
    
    //fetch('http://www.mocky.io/v2/59f9e78c370000920d66b895', { //401
       fetch(fetch_url, {  //real requests
    //fetch('http://www.mocky.io/v2/5a1e9f982f00001727ee2eb4', {   //200
        method: 'POST',
        headers: new Headers({
            'Content-Type': 'application/x-www-form-urlencoded',
        }),
        body: data,
		credentials: 'include'
    }).then((response) => {
        if (response.ok){
            this.setState({
                errorMessage:"Invite sent successfully!",
                errorColor:'green'
            });
        }
        else{
            this.setState({
                errorMessage:"Invitation Failed",
                errorColor:'red'
            });
            return;
        }
    })
}

createTeam(name) {
    let data ="name="+name;
    let fetch_url=api_url+"team";
    //fetch('http://www.mocky.io/v2/59f9e78c370000920d66b895', { //401
      fetch(fetch_url, {  //real requests
    //fetch('http://www.mocky.io/v2/5a36c1342f0000ef16127afa', {   //200
        method: 'POST',
        headers: new Headers({
            'Content-Type': 'application/x-www-form-urlencoded',
        }),
        body: data,
		credentials: 'include'
    }).then((response) => {
        if (response.ok){
            return response.json();
        }
        else{
            this.setState({
                errorMessageTeam:"Team Creation failed"
            });
            return;
        }
    }).then((json) => {
        let teams=this.state.teams;
        teams.push(json);
        this.setState({
            errorMessageTeam:"",
            teams:teams
        });
    })
}
changeTeam(i){
    this.setState({
        curTeam: this.state.teams[i],
        menuOpen:false
    });
    this.ListTeamIdeas(this.state.teams[i]);
    this.ListTeamMembers(this.state.teams[i]);
 }
    

  ListTeams(teams){
    if(teams===undefined)
        window.location.replace('/');
    let teams_jsx=[];
    for(let i=0;i<teams.length;i++){
      teams_jsx.push(
        <a key={teams[i]['name']} className="normal_text menu-item" onClick={()=>this.changeTeam(i)}>{teams[i]['name']}</a>    
      );
    }
 
    return(teams_jsx);
  }
    
  ListTeamIdeas(team){
    let ideas=team['ideas'];
    let ideas_jsx=[];
    if(ideas===undefined || ideas.length===0){
        ideas_jsx.push(
            <div>
            <p className="normal_text">No Ideas</p>
            </div>
        );
        this.setState({
            curTeamIdeas: ideas_jsx
         });
       return;
    }
    for(let i=0;i<ideas.length;i++){
    
      let idea_url = '/idea?id=' + ideas[i]['id'];
      ideas_jsx.push(
          
        <div key={ideas[i]['id']} className="idea_portfolio title_font panel panel-default text-justify row">
          <a className="btn btn-lg table-responsive inline-block " href={idea_url}> <img className="inline-block myimg"  src={require("../../images/light-bulb.png")} alt="Icons made by Freepik from www.flaticon.com is licensed by CC 3.0 BY" /> {ideas[i]['title']}  <span className="collapse-non-md normal_text">{ideas[i]['creationdate']}</span></a>
          <div className=" pull-right inline-block action-buttons ">
          {team['admin'] &&
            <button className="deleteIdea" onClick={()=> this.removeItem(ideas[i]['id'],"idea")}>
            <span className="glyphicon glyphicon-trash" />
            </button>
          }   
          </div>
         
        </div>
       
      );
    }

    this.setState({
       curTeamIdeas: ideas_jsx
    });

  }


  updateInputValue (evt,type) {
    if(type==="member")
        this.setState({
            addmember: evt.target.value
        });
    else
        this.setState({
            createTeam: evt.target.value
        });
  } 

 ListTeamMembers(team){
    let members=team['members'];
    let members_jsx=[];
    if(members===undefined || members.length===0){
        members_jsx.push(
            <div>
            <p className="normal_text">No Members</p>
            </div>
        );
        this.setState({
            curTeamMembers: members_jsx
         });
       return;
    }
    for(let i=0;i<members.length;i++){
      members_jsx.push(
        <div key={members[i]['name']}>
        <h4 className="normal_text">{members[i]['name']} 
        {(team['admin'] && team['idfounder']!==members[i]['id']) && 
            <button type="button" onClick={()=>this.removeItem(members[i]['name'],"member")} className="delete" aria-label="Delete Member">
            <span aria-hidden="true">&times;</span>
            </button>
        }
        </h4>
        </div>
      );
    }

    this.setState({
       curTeamMembers: members_jsx
    });

  }
  
  render() {

    if(this.state.validFetch)
    return (
       
        <div id="outer-container">
          <Menu noOverlay id="slide" isOpen={this.state.menuOpen} pageWrapId={ "page-wrap" } outerContainerId={ "outer-container" }>
          <div className="menu-title">  <h3>My Teams</h3></div>
            {this.ListTeams(this.state.teams)}
            <div style={{paddingTop:25}}>
                <input  type="text" value={this.state.createTeam}  onChange={evt => this.updateInputValue(evt,"team")} className="add_team normal_text" placeholder="New Team" />
                <button type="button" disabled={this.state.createTeam.length===0} className="create_team_button" onClick={()=> this.createTeam(this.state.createTeam)}><span className="glyphicon glyphicon-plus"></span></button>
                <div className="errormessage"><p className="normal_text" style={{color:'red'}}>{this.state.errorMessageTeam}</p></div>
            </div>
          </Menu>
          <NavB sticky={false} updateLogin={(state, username) => this.handleLoginUpdate(state, username)} />

          <main id="page-wrap" className="teamsContainer" >
              <div className="teams-div">
                <div className="profile-title">  <h3>{this.state.curTeam!==null ? this.state.curTeam['name'] : "Select a Team"}</h3></div>
                <div className="panel-body teams-body">
                
                    <div className="box-body col-xs-5">
                    <div className="panel panel-default">
                    <div className="panel-body portfolio">

                    {this.state.curTeam!==null &&
                      <div className="portfolio-heading">
                          <h3>Members</h3>
                          {this.state.curTeam['admin'] &&
                          <div>
                            <input  type="text" value={this.state.addmember}  onChange={evt => this.updateInputValue(evt,"member")} className="add_member normal_text" placeholder="Add Member" />
                            <button type="button" disabled={this.state.addmember.length===0} className="add_member_button" onClick={()=> this.addMember(this.state.curTeam['id'], this.state.addmember)}><span className="glyphicon glyphicon-plus"></span></button>
                            <div className="errormessage"><p className="normal_text" style={{color:this.state.errorColor}}>{this.state.errorMessage}</p></div>
                          </div>
                          }
                      </div>
                    }
                        <ul className="list-group-item teamlist" >
                        <ReactCSSTransitionGroup transitionName="example"
                              transitionEnterTimeout={500}
                              transitionLeaveTimeout={300}>
                        {this.state.curTeamMembers}
                        </ ReactCSSTransitionGroup >
                        </ul>
                   </div>
                    </div>     
                    </div>       
                  <div className="col-xs-7 portfolio">
                    <div className="panel panel-default">
                      <div className="panel-body portfolio">
                      {this.state.curTeam!==null &&
                        <div className="portfolio-heading">
                          <h3>Portfolio</h3>
                        </div>
                      }
                        
                        <ul className="list-group-item teamlist">
                        <ReactCSSTransitionGroup transitionName="example"
                              transitionEnterTimeout={500}
                              transitionLeaveTimeout={300}>
                            {this.state.curTeamIdeas}
                             </ ReactCSSTransitionGroup >
                        </ul>
                       
                      </div>
                    </div>
                  </div>
                  </div>

                  </div> 
            </main>
    
            <Footer />
        </div>
       
        );
      else return null;
      }
    }
export default Teams;
