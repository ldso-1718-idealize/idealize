import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'react-switch-button/dist/react-switch-button.css';
import {Words} from '../../MyComponents/scattered_words';
import {LightBulb} from '../../MyComponents/lightbulb';
import {Form} from '../../MyComponents/Form';
import {Results} from '../../MyComponents/Results';
import {SearchWord} from '../../MyComponents/home_page';
import {NavB} from '../../MyComponents/navbar';
import {Footer} from '../../MyComponents/Footer';

class Home extends Component {
    constructor(props) {
        super(props);
        let listwords=localStorage.getItem('listwords')===null ? [] : JSON.parse(localStorage.getItem('listwords'));
        let centerword=localStorage.getItem('centerword')===null ? "replaceToUserInput" : localStorage.getItem('centerword');
        let currentpage=localStorage.getItem('currentpage')===null ? "initial" : localStorage.getItem('currentpage');
        let synopsis=localStorage.getItem('synopsis')===null ? "" : localStorage.getItem('synopsis');
        let errormessage=localStorage.getItem('errormessage')===null ? "" : localStorage.getItem('errormessage');
        let translation=localStorage.getItem('translation')===null ? 0 : localStorage.getItem('translation');
        let country=localStorage.getItem('country')===null ? "" : localStorage.getItem('country');
      
        this.state = {
            listWords: listwords,
            centerWord: centerword,
            currentPage: currentpage,
            synopsis: synopsis,
            errorMessage: errormessage,
            isLogged: false,
            translation:translation,
            country:country
        }
      
    }

    componentWillMount(){
        if(window.performance.navigation.type !==1){
            let tips=localStorage.getItem('tips')===null ? false : localStorage.getItem('tips')==="true";
            localStorage.clear();
            localStorage.setItem('tips',tips);
        }
        let listwords=localStorage.getItem('listwords')===null ? [] : JSON.parse(localStorage.getItem('listwords'));
        let centerword=localStorage.getItem('centerword')===null ? "replaceToUserInput" : localStorage.getItem('centerword');
        let currentpage=localStorage.getItem('currentpage')===null ? "initial" : localStorage.getItem('currentpage');
        let synopsis=localStorage.getItem('synopsis')===null ? "" : localStorage.getItem('synopsis');
        let errormessage=localStorage.getItem('errormessage')===null ? "" : localStorage.getItem('errormessage');
        let translation=localStorage.getItem('translation')===null ? 0 : localStorage.getItem('translation');
        let country=localStorage.getItem('country')===null ? "" : localStorage.getItem('country');

        this.setState({
            listWords: listwords,
            centerWord: centerword,
            currentPage: currentpage,
            synopsis: synopsis,
            errorMessage: errormessage,
            translation:translation,
            country:country
        });
    }
    
    handleWordsTransition(newPage, newlist) {
        this.setState({
            currentPage: newPage,
            listWords: newlist
        });
        localStorage.setItem('currentpage',newPage);
        localStorage.setItem('listwords',JSON.stringify(newlist));
        
    }

    handleSynopsisTransition(page,synopsis,country,translation) {
        let errorM = this.state.errorMessage;
        if(page==="results")
            errorM="";
        if(page==="words")
            errorM="";
        this.setState({
            currentPage: page,
            synopsis: synopsis,
            errorMessage: errorM,
            country:country,
            translation:translation
        });
        localStorage.setItem('currentpage',page);
        localStorage.setItem('synopsis',synopsis);
        localStorage.setItem('errormessage',errorM);
        localStorage.setItem('country',country);
        localStorage.setItem('translation',translation)
    }

    handleSubmitSearchWord(centerWord) {
        this.setState({
            currentPage: "words",
            centerWord: centerWord
        });
        localStorage.setItem('currentpage',"words");
        localStorage.setItem('centerword',centerWord);
    }

    handleResultsTransition(synopsis,errorM){
        this.setState({
            currentPage: "synopsis",
            synopsis: synopsis,
            errorMessage: errorM
        });
        localStorage.setItem('currentpage',"synopsis");
        localStorage.setItem('synopsis',synopsis);
        localStorage.setItem('errormessage',errorM);
    }

    handleLoginUpdate(state) {
        this.setState({isLogged: state});
    }

    render() {
        const currentPage = this.state.currentPage;
        let nextPage;
        switch (currentPage) {
            case "initial":
                nextPage = <SearchWord handleSubmitSearchWord={this.handleSubmitSearchWord.bind(this)}/>;
                break;
            case "words":
                nextPage = <Words centerWord={this.state.centerWord}
                                  brainstormHandler={this.handleWordsTransition.bind(this)}/>;
                break;
            case "synopsis":
                nextPage = <Form errorMessage={this.state.errorMessage} synopsis={this.state.synopsis} listWords={this.state.listWords} synopsisHandler={this.handleSynopsisTransition.bind(this)} keywordN={this.state.centerWord} fromPage="Home" />;
                break;
            case "results":
                nextPage = <Results isLogged={this.state.isLogged} translation={this.state.translation} country={this.state.country} synopsis={this.state.synopsis} listWords={this.state.listWords} keywordN={this.state.centerWord} resultsHandler={this.handleResultsTransition.bind(this)} />;
                break;
            case "analysis":
                break;
            default:
                break;
        }
        return (
            <div className="App">
                <NavB updateLogin={(state) => this.handleLoginUpdate(state)} />
                {nextPage}
                <LightBulb page={this.state.currentPage}/>
                <Footer />
                </div>
        );
    }
}

export default Home;