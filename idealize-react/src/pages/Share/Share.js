import React, {Component} from 'react';
import 'whatwg-fetch';
import {NavB} from "../../MyComponents/navbar";
import { LoginForm } from "../../MyComponents/login_form";
import { RegisterForm } from "../../MyComponents/register_form";
import 'bootstrap/dist/css/bootstrap.css';

// eslint-disable-next-line
import {api_url} from "../../App";

class Share extends Component {
    constructor(props) {
        super(props);

        this.state = {
            hash: "",
            username: '',
            validHash: false,
            userValidated: false,
            message: 'The link you clicked is not valid. Click here to go back to the homepage',
            badFetch: false,
        }
    }

    handleLoginUpdate(state) {
        this.setState({isLogged: state});
    }

    handleUserUpdate(userValue) {
        if (userValue !== '') {
            this.setState({username: userValue});
        }

        this.userHasAccount(userValue);
    }

    render() {

        var register = <RegisterForm updateUser={(userValue, closeModal) => this.handleUserUpdate(userValue, closeModal)} /> ;
        var login = <LoginForm updateUser={(userValue, closeModal) => this.handleUserUpdate(userValue, closeModal)} /> ;

        var nav = <NavB updateLogin={(state, username) => this.handleLoginUpdate(state, username)} hideRight={true}/> ;

        var forms = (
            <div>
                {nav}
                <table className="content">
                    <tr>
                        <td id="page-left">
                            {register}
                        </td>
                        <td id="page-right">
                            {login}
                            <div id="share-message">
                                {this.state.badFetch ?
                                    <p>Something went wrong. Please try again</p> :
                                    <br/>
                                }
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        );

        var link = (
            <div id="page">
                {nav}
                <a
                    href="/"
                    onClick={() => window.location.replace("/")}
                    className="activate-link"
                >
                    {this.state.message}
                </a>
            </div>
        );

        if (this.state.validHash){
            if (!this.state.userValidated) {
                return forms;
            } else {
                return link;
            }
        } else {
            return link;
        }
    }

    componentWillMount() {
        var res = this.props.history.location.search.split("?");

        var params = []; //[i][0]-> var_name; [i][1]-> var_value
        if (res.length !== 2) {
            window.location.replace("/");
        } else {
            for (var i = 1; i < res.length; i++) {
                params[i - 1] = res[i].split("=");
            }

            this.checkShareLink(params[0][1]);
        }

    }

    userHasAccount(userValue) {
        let self = this;
        let res = this.props.history.location.search.split("?");

        let params = []; //[i][0]-> var_name; [i][1]-> var_value

        for (let i = 1; i < res.length; i++) {
            params[i - 1] = res[i].split("=");
        }

        var formData = "hash=" + params[0][1] + "&username=" + userValue;

        fetch(api_url + 'addSharedIdea', {
        //fetch('http://www.mocky.io/v2/59f9e78c370000920d66b895', { //401
        //fetch('http://www.mocky.io/v2/5a1e9f982f00001727ee2eb4', {   //200
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
            }),
            body: formData
        }).then(function(response) {
            if (response.ok){
                return response.text();
            }
            throw new Error('Network response was not ok.');
        }).then(() => self.setState({
            userValidated: true,
            message: "The idea has been added to your portfolio!",
        }))
            .catch(() => self.unsuccessful(),
                function(error) {
                    console.log('Problem with fetch: ' + error.message);
                });

    }

    checkShareLink(hash) {
        var formData = "hash=" + hash;
        let self = this;

        fetch(api_url + 'checkShare', {
        //fetch('http://www.mocky.io/v2/59f9e78c370000920d66b895', {   //401
        //fetch('http://www.mocky.io/v2/5a394f1c370000791cfd2fb1', {   //200
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
            }),
            body: formData,
			credentials: 'include'
        }).then(function(response) {
            if (response.status === 200){
                return response.text();
            }
			if(response.status === 202){
				self.setState({
					userValidated: true,
					message: "The idea has been added to your portfolio!",
				})
				return response.text();
			}
            throw new Error('Network response was not ok.');
        }).then(() => self.setState({validHash: true}))
            .catch(() => self.invalidHash(),
                function(error) {
                    console.log('Problem with fetch: ' + error.message);
                });
    }

    unsuccessful(){
        this.setState({username: '',
                        userValidated: false,
                        badFetch: true,
        });

    }

    invalidHash(){
        this.setState({validHash: false,
            message: "The link you clicked is not valid. Click here to go back to the homepage"})
    }

} export default Share;