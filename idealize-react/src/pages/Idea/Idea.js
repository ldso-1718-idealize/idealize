import React, {Component} from 'react';
import {Nav, NavItem, Tab} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.css';
import './Idea.css';
import '../../MyComponents/Results.css';
import {ListGraphs} from '../../MyComponents/Results';
import {NavB} from "../../MyComponents/navbar";
import {Form} from "../../MyComponents/Form";
import {api_url} from "../../App";
import Footer from "../../MyComponents/Footer";
import Ads from "../../MyComponents/Ads";

class Idea extends Component {
    constructor(props) {
        super(props);

        let errormessage=localStorage.getItem('errormessage')===null ? "" : localStorage.getItem('errormessage');

        this.state = {
            keyword: 'MyKeyWord',
            randomWords: [
                'word1',
                'word2',
                'word3',
                'word4',
                'word5',
                'word6',
                'word7',
                'word8',
                'word9',
                'word10'
            ],
            synopsis: "",
            title: "",
            creationdate: "",
			country: "",
            activePage: 1,
            graphdata: [],
            ideaid: "",
            urlArgs: [],
            validID: false,
			showModal : false,
			validFetch: false,
            errorMessage: errormessage,
            reanalyzeMessage: ""

        };
		
		this.openModal = this.openModal.bind(this);
		this.closeModal = this.closeModal.bind(this);    
    }

    handleLoginUpdate(state) {
        this.setState({isLogged: state});
    }
	
	openModal() {
	   this.setState({ showModal: true });
    }

    closeModal() {
	   this.setState({showModal: false})
    }    

    render() {
		if(this.state.validFetch)
		{
            var tab1 = [
                <div key="tab1" id="tab1">
                     {this.state.reanalyzeMessage!=="" &&
                        <span className="font_title panel reanalyzeMessage">{this.state.reanalyzeMessage}</span>
                     }
                    <Form
                        errorMessage={this.state.errorMessage}
                        synopsis={this.state.synopsis}
                        title={this.state.title}
                        listWords={this.state.randomWords}
                        keywordN={this.state.keyword}
                        country={this.state.country}
                        fromPage="Idea"
                        ideaId={this.state.ideaid}
                    />
                </div>
                
            ];

			var tab2 = [
               
				<div key="tab2" id="tab2">
                    <div className="results_idea">
                        <div className="form-title">
                            <h3>Results</h3>
                        </div>
                        <ListGraphs graphs={this.state.graphdata}/>
                    </div>
				</div>
                
			];

			var page = (
				<div className="App">
					<NavB updateLogin={(state, username) => this.handleLoginUpdate(state, username)}/>
					<Tab.Container id="tabs" defaultActiveKey="first">
                        <div className ="ideaContainer" style={{paddingBottom:window.innerHeight/10}}>
                            <Ads />
                           
                            <Nav bsStyle="pills" id="idea-tabs">
                                <NavItem className="idea-pills" eventKey="first">
                                    Idea Info
                                </NavItem>
                                <NavItem className="idea-pills" eventKey="second" onClick={() => this.closeModal()}>
                                    Idea Graphs
                                </NavItem>
                            </Nav>
                            <Tab.Content animation>
                                <Tab.Pane eventKey="first">
                                    {tab1}
                                </Tab.Pane>
                                <Tab.Pane eventKey="second">
                                    {tab2}
                                </Tab.Pane>
                            </Tab.Content>
                        </div>
					</Tab.Container>
                    <Footer/>
				</div>
			);

			if (this.state.validID) {
				return page;
			} else {
				return null;
			}
		}
		else
		{
			return null;
		}
    }

    componentWillMount() {
        let res = this.props.history.location.search.split("?");
        if (res.length > 3) {
            window.location.replace("/");
        } else {
            let params= res[1].split("&");

            let args = [];
            for (let i = 0; i < params.length; i++) {
                let tmp = params[i].split('=');
                args.push({
                    name: tmp[0],
                    value: tmp[1],
                })
            }


            if (args[0].name === 'id') {
                this.setState({
                    validID: true,
                    ideaid: args[0].value,
                    urlArgs: args,
                });

                if (res[2]!==undefined && res[2]==="reanalyzed"){
                    this.setState({
                        reanalyzeMessage:"Reanalyze was successful!"
                    })
                }
                setTimeout(()=>this.setState({
                    reanalyzeMessage:""
                }), 1700);

                fetch(api_url + 'idea?ideaid=' + args[0].value, {
                //fetch("http://www.mocky.io/v2/5a330202310000292738bbc5", {  //words + .... + graphs
                    method: 'GET',
                    headers: new Headers({
                        'Content-Type': 'application/x-www-form-urlencoded',
                    }),
                    credentials: 'include'
                })
                    .then(function (response) {

                        return response.json();
                    })
                    .catch(function (err) {
                        console.log(err);
                    })
                    .then((json) => this.refreshData(json));
            }
        }

    }

    refreshData(data) {
        if(data===undefined)
            window.location.replace('/');
        this.setState({
            keyword: data.keyword,
            randomWords: data.keywords,
            graphdata: data.trends,
            synopsis: data.synopsis,
            title: data.title,
            creationdate: data.creationdate,
			country: data.country,
			validFetch: true
        });

    }
}

export default Idea;
