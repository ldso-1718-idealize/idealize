import React, { Component } from 'react';
import {NavB} from "../../MyComponents/navbar";
import {EditProfile} from '../../MyComponents/edit_profile';
import {Confirmation} from '../../MyComponents/Confirmation'
import {api_url} from "../../App";
import Footer from "../../MyComponents/Footer";
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import 'whatwg-fetch';
import "isomorphic-fetch";

export class Profile extends Component {
  constructor() {
    super();
    this.state = {
      userdata: [],
      userideas: [],
      userinvitesdata: [],    
      userinvites: [],    
      validFetch: false,
      showModal : false,
      showConfirmationModal: false,    
      confirmationID : "",
      typeConfirmation: "",    
          
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.removeItem = this.removeItem.bind(this);  
    this.favoriteItem = this.favoriteItem.bind(this);
    this.update =this.update.bind(this);
    this.acceptInvite = this.acceptInvite.bind(this); 
    this.cancelAction = this.cancelAction.bind(this);
    this.confirmation = this.confirmation.bind(this);  
  }
    
   cancelAction() {
       this.setState({showConfirmationModal: false});
   }    

   confirmation(id, type) {
       
       this.setState({typeConfirmation: type, confirmationID: id, showConfirmationModal: true});
   }    
    
    handleLoginUpdate(state) {
        this.setState({isLogged: state});
    }

  componentDidMount() {
    this.fetchUserData();
    this.fetchUserInvites();  
  }
 
  openModal() {
       this.setState({ showModal: true });
  }

  closeModal() {
      this.setState({showModal: false})
  } 

  update(firstName, lastName, email) {
      
       
      
       
      var userdata = this.state.userdata;
     
      userdata['firstname'] = firstName;
      userdata['lastname'] = lastName;
      userdata['email'] = email;
        
      
      this.setState({userdata: userdata, showModal: false});
       
      
  }    

  fetchUserInvites() {
     
	  fetch(api_url + 'teamInvite', {
      //fetch('http://www.mocky.io/v2/5a32baf9310000df0c38bb2a', {
        credentials: 'include'
        })
        .then(function(response) {
          
            return response.json();
        })
        .catch(function(err){
          console.log(err);
          //window.location.replace('/');
        })
        .then((json) => {
          this.setState({userinvitesdata: json});
          this.listInvites();
      })             
  }  
    
    
  fetchUserData(){
	fetch(api_url + 'getProfileInfo', {
    //fetch('http://www.mocky.io/v2/5a326be5310000de0c38b9cc', {		
      credentials: 'include'
	})
    .then(function(response) {
      if(response.status===404){
        window.location.replace('/');
      }
      else if(response.status===400){
        window.location.replace('/');
      }
        return response.json();
    })
    .catch(function(err){
      console.log(err);
      window.location.replace('/');
    })
    .then((json) => this.refreshData(json))
  }


  refreshData(data) {           // TODO: erase userInvitesData e listInvites
    this.setState({
      userdata: data,
      isFetching: false,
      validFetch: true,
         
    });
      
    this.listIdeas();
    
  }
    
  starType(favorite) {
      if(favorite !== undefined && favorite === true )
          return (<span className="glyphicon glyphicon-star" />);
        return (<span className="glyphicon glyphicon-star-empty" />);
  }

  listIdeas(){
    let ideas = [];
    if(this.state.userdata['ideas']===undefined || this.state.userdata['ideas'].length===0){
        ideas.push(
            <div>
            <p className="normal_text">No Ideas</p>
            </div>
        );
        this.setState({
            userideas: ideas
         });
       return;
    }
    for(let i=0;i<this.state.userdata['ideas'].length;i++){
      let idea_url = '/idea?id=' + this.state.userdata['ideas'][i]['id'];
      ideas.push(
        
        <span>
       
    
        <div i key={this.state.userdata['ideas'][i]['id']} className="idea_portfolio title_font panel panel-default text-justify row ideas">
        {(this.state.userdata['ideas'][i]['author'] !== undefined && this.state.userdata['ideas'][i]['author'] !== "") &&
          <div  className="shared_portfolio App">
            <span className="title_font">Shared By: {this.state.userdata['ideas'][i]['author']}</span> 
          </div>
        }
          <a className="btn btn-lg table-responsive inline-block " href={idea_url}> <img className="inline-block myimg"  src={require("../../images/light-bulb.png")} alt="Icons made by Freepik from www.flaticon.com is licensed by CC 3.0 BY" /> {this.state.userdata['ideas'][i]['title']}  <span className="collapse-non-md normal_text">{this.state.userdata['ideas'][i]['creationdate']}</span></a>
          <div className=" pull-right inline-block action-buttons ">
            {(() => {
                if(this.state.userdata['ideas'][i]['author'] === undefined || this.state.userdata['ideas'][i]['author'] === "") {
                     return(<button   className="deleteIdea" onClick={()=> this.confirmation(this.state.userdata['ideas'][i]['id'], "delete")}>
                <span className="glyphicon glyphicon-trash" />
            </button> ) ; 
            }else {/*
                return(<button disabled="true" className="deleteIdea" onClick={()=> this.removeItem(this.state.userdata['ideas'][i]['id'])}>
                <span className="glyphicon glyphicon-trash" />
            </button>);
            
            */
            
            }
             
            })()}  
            
            <button className="favouriteIdea" onClick={()=> this.favoriteItem(this.state.userdata['ideas'][i]['id'])}>
                {(() => {
                    if(this.state.userdata['ideas'][i]['favorite'] !== undefined && this.state.userdata['ideas'][i]['favorite'] )
                        return (<span className="glyphicon glyphicon-star" />);
                    return (<span className="glyphicon glyphicon-star-empty" />);
                 })()}
            </button>
          </div>
         
        </div>
        </span>
       
      )
    }

    this.setState({
      userideas: ideas
    });

  }
  
  
  listInvites() {
      let invites = [];
	   
      if(this.state.userinvitesdata===undefined || this.state.userinvitesdata.length===0){
        invites.push(
            <div>
            <p className="normal_text">No invites</p>
            </div>
        );
        this.setState({
            userinvites: invites
         });
       return;
      }
      for(let i = 0;  i < this.state.userinvitesdata.length; i++) {
            if(this.state.userinvitesdata[i]['status'] !== "accepted") { 
              invites.push(

                <div key={this.state.userinvitesdata[i]['id']} className="idea_portfolio title_font panel panel-default text-justify row invites">
                  <a className="btn btn-lg table-responsive inline-block "  >  {this.state.userinvitesdata[i]['name']} </a>
                  <div className=" pull-right inline-block action-buttons ">
                    <button className="favouriteIdea deleteInvite" onClick={()=> this.acceptInvite(this.state.userinvitesdata[i]['id'], true)}>
                        <span className="glyphicon glyphicon-ok" />
                    </button>    
                    <button className="favouriteIdea" onClick={()=> this.confirmation(this.state.userinvitesdata[i]['id'], "invite")}>
                        <span className="glyphicon glyphicon-remove" />
                    </button>
                  </div>

                </div>

              )
        }
      }
      this.setState({
         userinvites: invites
      });
  }
  
  
  
  
  
  
 favoriteItem(i) {
		let data ="ideaid=" + i;
		fetch(api_url + 'favoriteIdea', { //401
        //fetch('http://www.mocky.io/v2/59f9e78c370000920d66b895', { //401
        //fetch('http://www.mocky.io/v2/5a1e9f982f00001727ee2eb4', {   //200
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
            }),
            body: data,
			credentials: 'include'
        }).then(function(response) {
            if (response.ok){
                return response.text();
            }
            throw new Error('Network response was not ok.');
        }).then(() => {
            
            let ideas = this.state.userdata['ideas'].slice();
            
            for (var j = 0; j < ideas.length; j++) {
                
                if (ideas[j].id === i) {
                     
                    if(ideas[j].favorite === true)
                        ideas[j].favorite = false;
                    else
                        ideas[j].favorite = true;
                                        
                }
            }
                       
            let aux = this.state.userdata;
            aux['ideas'] = ideas;
            
            
            this.setState({
                userdata: aux
            });
            
            this.listIdeas();
            
        })
            .catch(() => { /* Error message */}
                );
  }
  
  
  
  
  removeItem(i) {
        
		let data ="ideaid=" + i;
		fetch(api_url + 'deleteIdea', { //401
        //fetch('http://www.mocky.io/v2/59f9e78c370000920d66b895', { //401
        //fetch('http://www.mocky.io/v2/5a1e9f982f00001727ee2eb4', {   //200
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
            }),
            body: data,
			credentials: 'include'
        }).then(function(response) {
            if (response.ok){
                return response.text();
            }
            throw new Error('Network response was not ok.');
        }).then(() => {
            
            let ideas = this.state.userdata['ideas'].slice();
            for (var j = 0; j < ideas.length; j++) {
                
                if (ideas[j].id === i) {
                    
                    ideas.splice(j, 1);
                }
            }
           let aux = this.state.userdata;
            aux['ideas'] = ideas;
            
            
            this.setState({
                userdata: aux
            });
            
            this.listIdeas();
            
            
             
        })
            .catch(() => { /* Error message */}
                );
      
		
	}



acceptInvite(i, accept) {
         
		 let urlPath = 'joinTeam';
		 
		 if(!accept)
		 {
			 urlPath = 'refuseTeamInvite';
		 }
		 
		let data ="id=" + i;
		fetch(api_url + urlPath, {   //200
        //fetch('http://www.mocky.io/v2/5a1e9f982f00001727ee2eb4', {   //200
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
            }),
            body: data,
			credentials: 'include'
        }).then(function(response) {
            if (response.ok){
                return response.text();
            }
            throw new Error('Network response was not ok.');
        }).then(() => {
            let invites = this.state.userinvitesdata;
            
            for (var j = 0; j < invites.length; j++) {

                    if (invites[j].id === i) {

                        invites.splice(j, 1);
                    }
                }

           


            this.setState({
                    userinvitesdata: invites
            });
            
            this.listInvites();
         }).catch(() => {});
            
        
       
        
      
        
    }


    

  
    


  render() {
    if(this.state.validFetch)
    return (
        <div className="">
          <NavB updateLogin={(state, username) => this.handleLoginUpdate(state, username)} />
          <div className=" container profileContainer">
              <div className="profile_div">
                <div className="profile-title">  <h3>Profile</h3></div>
                <div className="panel-body">
                    <div className="box-body col-xs-5">
                      <div className="col-sm-12">
                        <h2 className="profile_name" style={{textAlign: "center"}}>{this.state.userdata['username']}</h2>
                      </div>
                      <div className="clearfix" />
                      <hr />
                      <div className="col-sm-5 col-xs-6 tital normal_text">First Name:</div><div className="col-sm-7 col-xs-6 normal_text">{this.state.userdata['firstname']}</div>
                      <div className="clearfix" />
                      <div className="bot-border" />
                      <div className="col-sm-5 col-xs-6 tital normal_text">Last Name:</div><div className="col-sm-7 normal_text">{this.state.userdata['lastname']}</div>
                      <div className="clearfix" />
                      <div className="bot-border" />
                      <div className="col-sm-5 col-xs-6 tital normal_text">Email:</div><div className="col-sm-7 normal_text">{this.state.userdata['email']}</div>
                      <div className="clearfix" />
                      <div style={{textAlign: "center"}}>           
                          <button style={{marginTop: 40, marginBottom: 10, width: 200, backgroundColor: '#42a5f4;'}} onClick={this.openModal} type="button" className="btn btn-lg">Edit Profile</button>
                      </div>          
                      <div className="bot-border" />
                     <Confirmation showModal={this.state.showConfirmationModal} id = {this.state.confirmationID}  type={this.state.typeConfirmation}  cancelAction={this.cancelAction} removeItem = {this.removeItem} acceptInvite = {this.acceptInvite} />    
                    
                     < EditProfile showModal={this.state.showModal} firstName={this.state.userdata['firstname']} lastName={this.state.userdata['lastname']} email={this.state.userdata['email']} closeModal={this.closeModal} update ={this.update} /> 
                    
                     <div className="portfolio">
                         <div className="panel panel-default">
                             <div className="panel-body portfolio">
                                 <div className="portfolio-heading">
                                    <h3>Invites</h3>
                                 </div>
                                 <ul  className="list-group-item" style={{ height: "200", overflowY: "auto"}}> 
                                     <ReactCSSTransitionGroup transitionName="example"
                                     transitionEnterTimeout={500}
                                     transitionLeaveTimeout={300}>
                                    {this.state.userinvites}
                                     </ ReactCSSTransitionGroup >
                                 </ul>
                             </div>        
                         </div>    
                        </div>     
                    </div>  
                     
                  <div className="col-xs-7 portfolio">
                    <div className="panel panel-default">
                      <div className="panel-body portfolio">
                        <div className="portfolio-heading">
                          <h3>Portfolio</h3>
                        </div>
                        
                        <ul  className="list-group-item" style={{ height: "470", overflowY: "auto"}}>
                            <ReactCSSTransitionGroup transitionName="example"
                             transitionEnterTimeout={500}
                             transitionLeaveTimeout={300}>
                            {this.state.userideas}
                             </ ReactCSSTransitionGroup >
                        </ul>
                       
                      </div>
                    </div>
                  </div>
                  </div>

                  </div>
                 
              
            
             
            </div>
            <Footer />
        </div>
        );
      else return null;
      }
    }
export default Profile;
