import React, {Component} from 'react';
import {NavB} from "../../MyComponents/navbar";
import 'bootstrap/dist/css/bootstrap.css';
import {api_url} from "../../App";

class Activate extends Component {
    constructor(props) {
        super(props);

        this.state = {
            hash: "",
            url: "api.projectidealize.me/activate",
            validID: false,
            accountActivated: false
        }
    }

    handleLoginUpdate(state) {
        this.setState({isLogged: state});
    }

    render() {
        var message = "Couldn't activate your account. Click here to go to the Homepage";
        if (this.state.accountActivated){
            message = "Account successfully activated! Click here to go to the Homepage";
        }

        var page = (
            <div id="page">
                <NavB updateLogin={(state, username) => this.handleLoginUpdate(state, username)} hideRight={true}/>
                <a
                    href="/"
                    onClick={() => window.location.replace("/")}
                    className="activate-link"
                >
                    {message}
                </a>
            </div>

        );

        if (this.state.validID){
            return page;
        } else {
            return null;
        }
    }

    componentWillMount() {
        var res = this.props.history.location.search.split("?");

        var params = []; //[i][0]-> var_name; [i][1]-> var_value
        if (res.length !== 2) {
            window.location.replace("/");
        } else {
            this.setState({validID: true});
            for (var i = 1; i < res.length; i++) {
                params[i - 1] = res[i].split("=");
            }
            //console.log(params[0][1]);
            this.setState({hash: params[0][1]});
        }
		
		//console.log(params[0][1]);

        this.activateAccount(params[0][1]);
    }

    activateAccount(hash) {
        var formData = "hash=" + hash;
        let self = this;

        fetch(api_url + 'activate', {
        //fetch('http://www.mocky.io/v2/59f9e78c370000920d66b895', { //401
        //fetch('http://www.mocky.io/v2/59f9eae13700001d0e66b8a6', {   //200
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/x-www-form-urlencoded',
            }),
            body: formData
        }).then(function(response) {
            if (response.ok){
                return response.text();
            }
            throw new Error('Network response was not ok.');
        }).then(() => self.setState({accountActivated: true}))
            .catch(function(error) {
                    console.log('Problem with fetch: ' + error.message);
                });
    }

} export default Activate;