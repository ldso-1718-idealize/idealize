import React, {Component} from 'react';
import 'whatwg-fetch';
import {NavB} from "../../MyComponents/navbar";
import {Footer} from '../../MyComponents/Footer';
import './Contacts.css';
import Ads from '../../MyComponents/Ads';
import {captcha_site} from "../../App";
import ReCAPTCHA from "react-google-recaptcha";
var captcha;

let limChar = 500;

class Contacts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            email: '',
            subject: '',
            errorMessage: "",
            chars_left: limChar,
            color: 'black',
            validCaptcha:false,
            formValid:false,
            emailValid:false,
            contentValid:false
        };
        this.emailHandler=this.emailHandler.bind(this);
        this.contactsHandler=this.contactsHandler.bind(this);
        this.subjectHandler=this.subjectHandler.bind(this);
        this.descriptionHandler=this.descriptionHandler.bind(this);
        this.captchaChange = this.captchaChange.bind(this);
    }

    descriptionHandler(event) {
        let input = event.target.value;
        let myColor = 'black';
        if(input.length === limChar){
            myColor = 'red';
        }
        this.setState({
            value:input,
            chars_left: limChar - input.length,
            color: myColor
        });
        this.validateField('content',input.length);

    }
    subjectHandler(event) {
        let input = event.target.value;
        this.setState({
            subject:input
        });
    }
    
    captchaChange(value){
        if(value!==null){
            this.setState({
                validCaptcha:true
            });
        }
        else captcha.reset();
    };

    validateField(field, value) {
        let emailValid = this.state.emailValid;
        let contentValid = this.state.contentValid;
        if (field === 'email') {
            emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        } 
        if(field === 'content'){
            contentValid = value!==0;
        }

        this.setState({emailValid: emailValid,
                       contentValid: contentValid},
            () => {this.validateForm()});
    }

    validateForm() {
        this.setState({formValid: this.state.emailValid && this.state.contentValid && this.state.validCaptcha});
    }

    contactsHandler() {
        let txtEmail = this.state.email;
        let txtSubject = this.state.subject;
        let txtDescription = this.state.value;

        // send txtEmail txtSubject txtDescription to some one here
        alert(txtEmail+txtSubject+txtDescription);

    }
    emailHandler(event) {
        var input = event.target.value;
        this.setState({
            email:input
        });
        this.validateField('email',input);
    }

    handleLoginUpdate(state) {
        this.setState({isLogged: state});
    }

    render() {
        let myColor = this.state.color;
        let myChar = this.state.chars_left;
        let myValue = this.state.value;
        return (
            <div className="App">
                <div>
                    <NavB updateLogin={(state) => this.handleLoginUpdate(state)} />
                </div>

                <div className="formContainer">
                    <div className="form">
                    <Ads />
                        <div className="form-title"><h3>Contact Us</h3></div>
                        <div className="form-group">
                            <form className="goAnalise">
                                <div className="form-group">
                                    <label className="form-label normal_text" htmlFor="email">E-Mail:</label>
                                    <input type="text" className="form-text" name="email" placeholder="Input your e-mail" onChange={this.emailHandler.bind(this)}/>
                                    <label className="form-label normal_text" htmlFor="subject">Subject:</label>
                                    <input type="text" className="form-text" name="subject" placeholder="Input your subject" onChange={this.subjectHandler.bind(this)}/>
                                    <label className="form-label normal_text" htmlFor="description">Text to send:</label>
                                    <textarea name="description" className="form-description"  rows="5" value={myValue} onChange={this.descriptionHandler.bind(this)} maxLength="limChar" placeholder="Describe your subject here..." ></textarea>
                                    <div className="charcount" style={{color:myColor}}>Characters Left: {myChar}</div>
                                    <div className="recaptcha_div">
                                    <ReCAPTCHA
                                    sitekey={captcha_site}  
                                    ref={(el) => { captcha = el; }}
                                    onChange={this.captchaChange}
                                    />
                                    </div>
                                    <button type="submit" disabled={!this.state.formValid} className="btn btn-lg title_font form-button" id="btn-contacts" onClick={this.contactsHandler.bind(this)}>Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

export default Contacts;