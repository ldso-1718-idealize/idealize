For Docker Build

Have docker and docker-compose installed in your OS  
Hit "docker-compose up" in **the docker folder**  
The product is available at localhost:3000  

If using CentOS:

install all the .rpm files inside the Vagrant folder
install mysql and create  a database named idealize and initialize it using the script db.sql in the folder Database
run each component by using "service {nginx|idealize-api|idealizetrends} start" depending on what component you wish to run