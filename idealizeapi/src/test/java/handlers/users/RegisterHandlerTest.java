package handlers.users;

import org.junit.Test;
import org.mockito.Mockito;
import services.MySqlBasedUserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static utils.Utils.*;

public class RegisterHandlerTest extends Mockito {

    @Test
    public void testRegister() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        MySqlBasedUserService service = mock(MySqlBasedUserService.class);
        Map parameterMap = mock(Map.class);

        Mockito.doNothing().when(service).setup();
        when(request.getParameterMap()).thenReturn(parameterMap);
        when(request.getParameterMap().containsKey(USERNAME)).thenReturn(true);
        when(request.getParameterMap().containsKey(PASSWORD)).thenReturn(true);
        when(request.getParameterMap().containsKey(EMAIL)).thenReturn(true);
        when(request.getParameterMap().containsKey(FIRSTNAME)).thenReturn(true);
        when(request.getParameterMap().containsKey(LASTNAME)).thenReturn(true);
        when(request.getParameterMap().containsKey(ACTIVATE)).thenReturn(true);
        when(request.getParameter(USERNAME)).thenReturn(USERNAME);
        when(request.getParameter(PASSWORD)).thenReturn(PASSWORD);
        when(request.getParameter(EMAIL)).thenReturn(EMAIL);
        when(request.getParameter(FIRSTNAME)).thenReturn(FIRSTNAME);
        when(request.getParameter(LASTNAME)).thenReturn(LASTNAME);
        when(request.getParameter(ACTIVATE)).thenReturn("false");
        when(service.register(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(true);
        //when(service.register(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), true)).thenReturn(true);
        when(service.getUserID(Mockito.anyString())).thenReturn(1);
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);
        when(response.getStatus()).thenReturn(HttpServletResponse.SC_OK);

        new RegisterHandler(service).doPost(request, response);

        verify(request, atLeast(1)).getParameter(USERNAME); // only if you want to verify username was called...
        verify(request, atLeast(1)).getParameter(PASSWORD); // only if you want to verify username was called...
        verify(request, atLeast(1)).getParameter(EMAIL); // only if you want to verify username was called...
        verify(request, atLeast(1)).getParameter(FIRSTNAME); // only if you want to verify username was called...
        verify(request, atLeast(1)).getParameter(LASTNAME); // only if you want to verify username was called...
        verify(response, atLeast(1)).setStatus(HttpServletResponse.SC_OK); // only if you want to verify username was called...

        writer.flush(); // it may not have been flushed yet...
    }
}