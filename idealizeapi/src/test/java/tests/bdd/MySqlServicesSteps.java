package tests.bdd;

import org.json.JSONObject;
import org.junit.Assert;
import services.MySqlBasedUserService;
import static utils.Configs.readConfig;
import static utils.Utils.*;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;


public class MySqlServicesSteps {

    private MySqlBasedUserService service;

    private static final String CONFIG_PROPERTIES_PATH = "src/main/resources/config.properties";

    private boolean registered = false;
    private boolean deleted = false;
    private String loginToken;
    private String profileInfo;
    private int userID;
    private int savedIdea;

    private static final String DUMMY_PASSWORD = "Dummy_Password",
                                DUMMY_FIRSTNAME = "Dummy_Firstname",
                                DUMMY_LASTNAME = "Dummy_Lastname",
                                DUMMY_EMAIL = "Dummy_Email";

    @Before
    public void dbSetup() {

        readConfig(CONFIG_PROPERTIES_PATH);
        service = new MySqlBasedUserService();
        service.setup();

    }


    //REGISTER
    @Given("^the username (.*), the password (.*), the firstname (.*), the lastname (.*) and the email (.*)$")
    public void register(String username, String password, String firstname, String lastname, String email) throws Throwable {

        registered = service.register(username, password, firstname, lastname, email);

    }

    @Then("^(.*) is now a new user$")
    public void checkRegister(String username) throws Throwable {

        assertThat(true, is(registered));

        //clean database
        service.deleteAccount(username);

    }


    //LOGIN/LOGOUT
    @Given("^the username (.*) and the password (.*)$")
    public void login(String username, String password) throws Throwable {

        service.register(username, password, DUMMY_FIRSTNAME, DUMMY_LASTNAME, DUMMY_EMAIL);

        loginToken = service.login(username, password);

    }

    @Then("^(.*) is now logged in$")
    public void checkLogin(String username) throws Throwable {

        assertThat(loginToken, is(not((ACCOUNT_NOT_ACTIVATED))));
        assertThat(loginToken, is(not((AUTHENTICATION_FAILED))));

        //clean database
        service.deleteAccount(username);
        service.logout(loginToken);

    }


    //DELETE ACCOUNT
    @Given("^that the registered user (.*) wants to delete his account$")
    public void prepareUser(String username) throws Throwable {

        service.register(username, DUMMY_PASSWORD, DUMMY_FIRSTNAME, DUMMY_LASTNAME, DUMMY_EMAIL);

    }

    @When("^(.*) deletes his account$")
    public void deleteAccount(String username) throws Throwable {

        deleted = service.deleteAccount(username);

    }

    @Then("^(.*) is no longer registered in the system$")
    public void checkDeleted(String username) throws Throwable {

        assertThat(true, is(deleted));

    }


    //GET PROFILE
    @Given("^that the user (.*) is registered$")
    public void prepareGetProfile(String username) throws Throwable {

        service.register(username, DUMMY_PASSWORD, DUMMY_FIRSTNAME, DUMMY_LASTNAME, DUMMY_EMAIL);

    }

    @When("^(.*) wants to see his profile$")
    public void getProfile(String username) throws Throwable {

        profileInfo = service.getProfileInfo(username);

    }

    @Then("^the (.*) profile information should match the registered information$")
    public void checkProfileInfo(String username) throws Throwable {

        JSONObject profile = new JSONObject(profileInfo);

        assertThat(username, is(profile.get(USERNAME)));
        assertThat(DUMMY_FIRSTNAME, is(profile.get(FIRSTNAME)));
        assertThat(DUMMY_LASTNAME, is(profile.get(LASTNAME)));
        assertThat(DUMMY_EMAIL, is(profile.get(EMAIL)));

        //clean database
        service.deleteAccount(username);

    }


    //SAVE IDEA
    @Given("^the registered user (.*)$")
    public void prepareSaveIdea(String username) throws Throwable {

        service.register(username, DUMMY_PASSWORD, DUMMY_FIRSTNAME, DUMMY_LASTNAME, DUMMY_EMAIL);
        userID = service.getUserID(username);

    }

    @When("^he saves his idea, with title (.*), synopsis (.*), keyword (.*) and trends (.*)$")
    public void saveIdea(String title, String synopsis, String keyword, String trends) throws Throwable {

        savedIdea = service.saveIdea(title, userID, synopsis, keyword, trends);

    }

    @Then("^the idea must be stored in the system$")
    public void checkSavedIdea() throws Throwable {

        Assert.assertTrue(savedIdea > -1);

        //clean database
        service.deleteAccount(userID);
        service.deleteIdea(savedIdea);

    }

}
