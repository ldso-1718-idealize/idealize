package tests.bdd;

import static utils.Configs.CUCUMBER_FEATURES_PATH;

import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;

@CucumberOptions(features = {CUCUMBER_FEATURES_PATH})

@RunWith(Cucumber.class)
public class CucumberMySqlServices {
}
