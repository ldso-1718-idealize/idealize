package tests;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import services.MySqlBasedUserService;

import static org.junit.Assert.*;
import static utils.Configs.readConfig;
import static utils.Utils.AUTHENTICATION_FAILED;
import static utils.Utils.KEYWORD;

public class MySqlBasedUserServiceTest {

    private static String PROPERTIES_FILE_NAME = "propertiesFileName";

    //alternativa PROPERTIES_FILE_NAME
    private static String CONFIG_PATH = "src/main/resources/config.properties";

    private static String DUMMY_USERNAME = "Test";
    private static String DUMMY_PASSWORD = "Test";
    private static String DUMMY_FIRSTNAME = "Test";
    private static String DUMMY_LASTNAME = "Test";
    private static String DUMMY_EMAIL = "Test@test.com";
    private static String DUMMY_USERNAME2 = "Test2";
    private static String DUMMY_PASSWORD2 = "Test2";
    private static String DUMMY_TITLE = "Title";
    private static int DUMMY_ID = 0;
    private static String DUMMY_SYNOPSIS = "Synopsis";
    private static String DUMMY_KEYWORD = "Keyword";
    private static String DUMMY_TRENDS = "{\"username\":\"xyz\",\"password\":\"xyz\"}";

    private MySqlBasedUserService service;

    @Before
    public void setup()
    {
        readConfig(System.getProperty(PROPERTIES_FILE_NAME));
        service = new MySqlBasedUserService();
        service.setup();
    }

    @Test
    public void register() {

        boolean res = service.register(DUMMY_USERNAME, DUMMY_PASSWORD, DUMMY_FIRSTNAME, DUMMY_LASTNAME, DUMMY_EMAIL);

        boolean res2 = service.register(DUMMY_USERNAME, DUMMY_PASSWORD, DUMMY_FIRSTNAME, DUMMY_LASTNAME, DUMMY_EMAIL);

        assertEquals(true, res);
        assertEquals(false, res2);

        service.deleteAccount(DUMMY_USERNAME);
    }

    @Test
    public void login() {

        boolean res = service.register(DUMMY_USERNAME, DUMMY_PASSWORD, DUMMY_FIRSTNAME, DUMMY_LASTNAME, DUMMY_EMAIL);

        assertEquals(true, res);

        String token = service.login(DUMMY_USERNAME, DUMMY_PASSWORD);
        String token2 = service.login(DUMMY_USERNAME2, DUMMY_PASSWORD2);

        assertNotEquals(AUTHENTICATION_FAILED, token);
        assertEquals(AUTHENTICATION_FAILED, token2);

        boolean res2 = service.logout(token);

        assertEquals(true, res2);

        service.deleteAccount(DUMMY_USERNAME);
    }

    @Test
    public void deleteAccount() {

        boolean res = service.register(DUMMY_USERNAME, DUMMY_PASSWORD, DUMMY_FIRSTNAME, DUMMY_LASTNAME, DUMMY_EMAIL);

        assertEquals(true, res);

        boolean res2 = service.deleteAccount(DUMMY_USERNAME);

        assertEquals(true, res2);
    }

    @Test
    public void saveAndDeleteIdea() {

        int res = service.saveIdea(DUMMY_TITLE, DUMMY_ID, DUMMY_SYNOPSIS, DUMMY_KEYWORD, DUMMY_TRENDS);

        JSONObject obj = new JSONObject();
        obj.put(KEYWORD, DUMMY_KEYWORD);

        service.createIdeaKeyword(DUMMY_ID, obj.toString());

        boolean resBol = false;

        if(res > -1)
            resBol = true;

        //System.out.println(res);

        assertEquals(true, resBol);

        boolean res2 = service.deleteIdea(DUMMY_TITLE);

        assertEquals(true, res2);
    }

    @After
    public void finish()
    {
        service.close();
    }

}