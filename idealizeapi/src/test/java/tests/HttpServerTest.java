package tests;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.DataOutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;

import handlers.mails.*;
import handlers.stats.GetStats;
import handlers.stats.IdeaHandler;
import handlers.stats.Reanalyze;
import handlers.users.*;

import org.eclipse.jetty.server.*;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import services.MySqlBasedUserService;

import static utils.Configs.readConfig;
import static utils.Utils.*;

import javax.servlet.http.HttpServletResponse;

public class HttpServerTest
{
    private Server server;
    private MySqlBasedUserService service;

    private static final int PORT = 8443;
    private static final String LOCAL_URL = "http://localhost:" + PORT;

    private static final String GET_METHOD = "GET";
    private static final String POST_METHOD = "POST";
    private static final String DELETE_METHOD = "DELETE";

    private static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
    private static final String UTF_8 = "utf-8";

    private static final String CONFIG_PROPERTIES_PATH = "src/main/resources/config.properties";

    private static final String TEXTRANK_ONERESULT = "Who is the best team in europe? They asked this and I answered portugal, of course, not only in europe but also in the universe, lol, I think that this is not gonna work. Why don't you get me more results?";
    private static final String TEXTRANK_NORESULT = "Hello.";

    private int currentUserID;

    @Before
    public void startJetty() throws Exception
    {

        readConfig(CONFIG_PROPERTIES_PATH);

        service = new MySqlBasedUserService();
        service.setup();

        server = new Server(PORT);
        ServletContextHandler context = new ServletContextHandler();

        context.addServlet(new ServletHolder(new RegisterHandler(service)),REGISTER_PATH);
        context.addServlet(new ServletHolder(new LoginHandler(service)),LOGIN_PATH);
        context.addServlet(new ServletHolder(new CheckSessionHandler(service)),CHECK_SESSION_PATH);
        context.addServlet(new ServletHolder(new LogoutHandler(service)),LOGOUT_PATH);
        context.addServlet(new ServletHolder(new DeleteAccountHandler(service)),DELETE_ACCOUNT_PATH);
        context.addServlet(new ServletHolder(new GetStats()),GET_STATS_PATH);
        context.addServlet(new ServletHolder(new GetProfileInfoHandler(service)),GET_PROFILE_INFO_PATH);
        context.addServlet(new ServletHolder(new IdeaHandler(service)),IDEA_PATH);
        context.addServlet(new ServletHolder(new EditProfileHandler(service)),EDIT_PROFILE_PATH);
        context.addServlet(new ServletHolder(new ActivateAccountHandler(service)),ACTIVATION_PATH);
        context.addServlet(new ServletHolder(new PasswordResetCheckHandler(service)),RESET_PASSWORD_CHECK_PATH);
        context.addServlet(new ServletHolder(new PasswordResetHandler(service)),RESET_PASSWORD_PATH);
        context.addServlet(new ServletHolder(new PasswordUpdateHandler(service)),UPDATE_PASSWORD_PATH);
        context.addServlet(new ServletHolder(new TeamHandler(service)),TEAM_PATH);
        context.addServlet(new ServletHolder(new TeamInviteHandler(service)),TEAM_INVITE_PATH);
        context.addServlet(new ServletHolder(new JoinTeamHandler(service)),JOIN_TEAM_PATH);
        context.addServlet(new ServletHolder(new RefuseTeamInviteHandler(service)),REFUSE_INVITE_TEAM_PATH);
        context.addServlet(new ServletHolder(new AddTeamIdeaHandler(service)),ADD_TEAM_IDEA_PATH);
        context.addServlet(new ServletHolder(new GetTeamsHandler(service)),GET_TEAMS_PATH);
        context.addServlet(new ServletHolder(new Reanalyze(service)),REANALYZE_PATH);
        context.addServlet(new ServletHolder(new ShareIdeaHandler(service)),SHARE_IDEA_PATH);
        context.addServlet(new ServletHolder(new CheckShareHandler(service)),CHECK_SHARE_PATH);
        context.addServlet(new ServletHolder(new AddSharedIdeaHandler(service)),ADD_SHARED_IDEA_PATH);
        context.addServlet(new ServletHolder(new RemoveTeamIdeaHandler(service)),REMOVED_SHARED_IDEA_PATH);
        context.addServlet(new ServletHolder(new RemoveTeamMemberHandler(service)),REMOVED_TEAM_MEMBER_PATH);
        context.addServlet(new ServletHolder(new FavoriteIdeaHandler(service)),FAVORITE_IDEA_PATH);
        context.addServlet(new ServletHolder(new DeleteIdeaHandler(service)),DELETE_IDEA_PATH);

        server.setHandler(context);

        server.start();
    }

    @After
    public void stopJetty()
    {
        try
        {
            server.stop();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetRegister() throws Exception
    {
        URL url = new URL(LOCAL_URL + REGISTER_PATH);
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();

        conn.connect();

        assertEquals(GET_METHOD, conn.getRequestMethod());
        assertEquals(HttpServletResponse.SC_OK, conn.getResponseCode());
    }

    @Test
    public void testPostRegister() throws Exception
    {
        URL url = new URL(LOCAL_URL + REGISTER_PATH);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        String urlParameters = USERNAME + "=nome&" +
                                PASSWORD + "=pwd&" +
                                FIRSTNAME + "=fn&" +
                                LASTNAME + "=ln&" +
                                EMAIL + "=em&" +
                                ACTIVATE + "=false";

        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
        int postDataLength = postData.length;

        conn.setRequestMethod(POST_METHOD);
        conn.setRequestProperty("Content-Type", CONTENT_TYPE);
        conn.setRequestProperty("charset", UTF_8);
        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        conn.setDoOutput(true);

        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.write(postData);
        }

        currentUserID = Integer.parseInt(conn.getHeaderField("id"));

        assertEquals(POST_METHOD, conn.getRequestMethod());
        assertEquals(HttpServletResponse.SC_OK, conn.getResponseCode());


        //delete test account in db
        service.deleteAccount(currentUserID);
    }

    @Test
    public void testPostLogin() throws Exception
    {
        //create test account in db
        service.register("nome","pwd", "fn", "ln", "em");
        currentUserID = service.getUserID("nome");


        URL url = new URL(LOCAL_URL + LOGIN_PATH);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        String urlParameters = USERNAME + "=nome&" +
                                PASSWORD + "=pwd";

        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
        int postDataLength = postData.length;

        conn.setRequestMethod(POST_METHOD);
        conn.setRequestProperty("Content-Type", CONTENT_TYPE);
        conn.setRequestProperty("charset", UTF_8);
        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        conn.setDoOutput(true);

        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.write(postData);
        }

        assertEquals(POST_METHOD, conn.getRequestMethod());
        assertEquals(HttpServletResponse.SC_OK, conn.getResponseCode());


        //delete test account and related sessions in db
        service.deleteUserSessions(currentUserID);
        service.deleteAccount(currentUserID);
    }

    @Test
    public void testGetLogout() throws Exception
    {
        //register test account in db
        service.register("nome","pwd", "fn", "ln", "em");
        currentUserID = service.getUserID("nome");


        //login
        URL url = new URL(LOCAL_URL + LOGIN_PATH);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        String urlParameters = USERNAME + "=nome&" +
                PASSWORD + "=pwd";

        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
        int postDataLength = postData.length;

        conn.setRequestMethod(POST_METHOD);
        conn.setRequestProperty("Content-Type", CONTENT_TYPE);
        conn.setRequestProperty("charset", UTF_8);
        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        conn.setDoOutput(true);

        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.write(postData);
        }

        List<String> cookies = conn.getHeaderFields().get("Set-Cookie");
        String cookie = cookies.get(0);


        //logout
        URL url2 = new URL(LOCAL_URL + LOGOUT_PATH);
        HttpURLConnection conn2 = (HttpURLConnection)url2.openConnection();

        //System.out.println(cookie);

        conn2.setRequestProperty("Cookie", cookie);

        conn2.connect();

        assertEquals(GET_METHOD, conn2.getRequestMethod());
        assertEquals(HttpServletResponse.SC_OK, conn2.getResponseCode());


        //delete test account in db
        service.deleteUserSessions(currentUserID);
        service.deleteAccount(currentUserID);
    }

    @Test
    public void testDeleteAccount() throws Exception
    {
        //register test account in db
        service.register("nome","pwd", "fn", "ln", "em");
        currentUserID = service.getUserID("nome");


        //delete
        URL url = new URL(LOCAL_URL + DELETE_ACCOUNT_PATH + "?" + USERID + "=" + currentUserID);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setRequestMethod(DELETE_METHOD);
        conn.setRequestProperty("Content-Type", CONTENT_TYPE);
        conn.setRequestProperty("charset", UTF_8);
        conn.setDoOutput(true);

        assertEquals(DELETE_METHOD, conn.getRequestMethod());
        assertEquals(HttpServletResponse.SC_OK, conn.getResponseCode());
    }

    /*
    @Test
    public void testTextRankOneResult() throws Exception
    {
        URL url = new URL(LOCAL_URL + GET_STATS_PATH);
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();

        String urlParameters = TEXTRANKQUERY + "=" + TEXTRANK_ONERESULT;

        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
        int postDataLength = postData.length;

        conn.setRequestMethod(POST_METHOD);
        conn.setRequestProperty("Content-Type", CONTENT_TYPE);
        conn.setRequestProperty("charset", UTF_8);
        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        conn.setDoOutput(true);

        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.write(postData);
        }

        assertEquals(POST_METHOD, conn.getRequestMethod());
        assertEquals(HttpServletResponse.SC_OK, conn.getResponseCode());
    }
    */

    @Test
    public void testTextRankNoResult() throws Exception
    {
        URL url = new URL(LOCAL_URL + GET_STATS_PATH);
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();

        String urlParameters = TEXTRANKQUERY + "=" + TEXTRANK_NORESULT;

        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
        int postDataLength = postData.length;

        conn.setRequestMethod(POST_METHOD);
        conn.setRequestProperty("Content-Type", CONTENT_TYPE);
        conn.setRequestProperty("charset", UTF_8);
        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        conn.setDoOutput(true);

        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.write(postData);
        }

        assertEquals(POST_METHOD, conn.getRequestMethod());
        assertEquals(HttpServletResponse.SC_NO_CONTENT, conn.getResponseCode());
    }

    @Test
    public void testGetProfile() throws Exception
    {
        //register test account in db
        service.register("nome","pwd", "fn", "ln", "em");
        currentUserID = service.getUserID("nome");

        //login
        URL url = new URL(LOCAL_URL + LOGIN_PATH);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        String urlParameters = USERNAME + "=nome&" +
                PASSWORD + "=pwd";

        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
        int postDataLength = postData.length;

        conn.setRequestMethod(POST_METHOD);
        conn.setRequestProperty("Content-Type", CONTENT_TYPE);
        conn.setRequestProperty("charset", UTF_8);
        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        conn.setDoOutput(true);

        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.write(postData);
        }

        List<String> cookies = conn.getHeaderFields().get("Set-Cookie");
        String cookie = cookies.get(0);


        //profile
        URL url2 = new URL(LOCAL_URL + GET_PROFILE_INFO_PATH);
        HttpURLConnection conn2 = (HttpURLConnection)url2.openConnection();

        conn2.setRequestProperty("Cookie", cookie);

        conn2.connect();

        BufferedReader br2 = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
        StringBuilder sb2 = new StringBuilder();
        String output2;
        while ((output2 = br2.readLine()) != null)
            sb2.append(output2);

        System.out.println(sb2.toString());

        assertEquals(GET_METHOD, conn2.getRequestMethod());
        assertEquals(HttpServletResponse.SC_OK, conn2.getResponseCode());

        //delete test entries in db
        service.deleteUserSessions(currentUserID);
        service.deleteAccount(currentUserID);
    }

    @Test
    public void testPostIdea() throws Exception
    {
        //register test account in db
        service.register("nome","pwd", "fn", "ln", "em");
        currentUserID = service.getUserID("nome");

        //login
        URL url = new URL(LOCAL_URL + LOGIN_PATH);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        String urlParameters = USERNAME + "=nome&" +
                PASSWORD + "=pwd";

        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
        int postDataLength = postData.length;

        conn.setRequestMethod(POST_METHOD);
        conn.setRequestProperty("Content-Type", CONTENT_TYPE);
        conn.setRequestProperty("charset", UTF_8);
        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        conn.setDoOutput(true);

        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.write(postData);
        }

        List<String> cookies = conn.getHeaderFields().get("Set-Cookie");
        String cookie = cookies.get(0);


        //save idea
        URL url2 = new URL(LOCAL_URL + IDEA_PATH);
        HttpURLConnection conn2 = (HttpURLConnection)url2.openConnection();

        JSONObject idea = new JSONObject();
        JSONArray keywords = new JSONArray();
        JSONArray trends = new JSONArray();

        idea.put("title", "tit");
        idea.put("synopsis", "sin");
        idea.put("main_keyword", "key");
        idea.put("country", "");

        keywords.put("key1");
        keywords.put("key2");

        idea.put("other_keywords", keywords);

        trends.put("trend1");
        trends.put("trend2");

        idea.put("trends", trends.toString());

        conn2.setRequestMethod(POST_METHOD);
        conn2.setRequestProperty("Cookie", cookie);
        conn2.setDoOutput(true);

        try (OutputStreamWriter wr = new OutputStreamWriter(conn2.getOutputStream())) {
            wr.write(idea.toString());
        }

        conn2.connect();

        //get last created idea id
        BufferedReader br = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String output;
        while ((output = br.readLine()) != null)
            sb.append(output);

        JSONObject obj = new JSONObject(sb.toString());

        int ideaID = obj.getInt(ID);

        assertEquals(POST_METHOD, conn2.getRequestMethod());
        assertEquals(HttpServletResponse.SC_OK, conn2.getResponseCode());

        //delete test entries in db

        service.deleteUserSessions(currentUserID);
        service.deleteAccount(currentUserID);
        service.deleteIdea(ideaID);
    }

    @Test
    public void testGetIdea() throws Exception
    {
        //register test account in db
        service.register("nome","pwd", "fn", "ln", "em");
        currentUserID = service.getUserID("nome");

        //login
        URL url = new URL(LOCAL_URL + LOGIN_PATH);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        String urlParameters = USERNAME + "=nome&" +
                PASSWORD + "=pwd";

        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
        int postDataLength = postData.length;

        conn.setRequestMethod(POST_METHOD);
        conn.setRequestProperty("Content-Type", CONTENT_TYPE);
        conn.setRequestProperty("charset", UTF_8);
        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        conn.setDoOutput(true);

        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.write(postData);
        }

        List<String> cookies = conn.getHeaderFields().get("Set-Cookie");
        String cookie = cookies.get(0);


        //save idea
        URL url2 = new URL(LOCAL_URL + IDEA_PATH);
        HttpURLConnection conn2 = (HttpURLConnection)url2.openConnection();

        JSONObject idea = new JSONObject();
        JSONArray keywords = new JSONArray();
        JSONArray trends = new JSONArray();

        idea.put("title", "tit");
        idea.put("synopsis", "sin");
        idea.put("main_keyword", "key");
        idea.put("country", "");

        keywords.put("key1");
        keywords.put("key2");

        idea.put("other_keywords", keywords);

        trends.put("trend1");
        trends.put("trend2");

        idea.put("trends", trends.toString());

        conn2.setRequestMethod(POST_METHOD);
        conn2.setRequestProperty("Cookie", cookie);
        conn2.setDoOutput(true);

        try (OutputStreamWriter wr = new OutputStreamWriter(conn2.getOutputStream())) {
            wr.write(idea.toString());
        }

        conn2.connect();

        //get last created idea id
        BufferedReader br = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String output;
        while ((output = br.readLine()) != null)
            sb.append(output);

        JSONObject obj = new JSONObject(sb.toString());

        int ideaID = obj.getInt(ID);


        //get idea
        String urlParameters3 = "?" + IDEAID + "=" + ideaID;
        URL url3 = new URL(LOCAL_URL + IDEA_PATH + urlParameters3);
        HttpURLConnection conn3 = (HttpURLConnection)url3.openConnection();

        conn3.setRequestProperty("Cookie", cookie);

        conn3.connect();

        BufferedReader br2 = new BufferedReader(new InputStreamReader(conn3.getInputStream()));
        StringBuilder sb2 = new StringBuilder();
        String output2;
        while ((output2 = br2.readLine()) != null)
            sb2.append(output2);

        assertEquals(GET_METHOD, conn3.getRequestMethod());
        assertEquals(HttpServletResponse.SC_OK, conn3.getResponseCode());


        //delete test entries in db
        service.deleteUserSessions(currentUserID);
        service.deleteAccount(currentUserID);
        service.deleteIdea(ideaID);
    }

    @Test
    public void testEditProfile() throws Exception
    {
        //register test account in db
        service.register("nome","pwd", "fn", "ln", "em");
        currentUserID = service.getUserID("nome");

        //login
        URL url = new URL(LOCAL_URL + LOGIN_PATH);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        String urlParameters = USERNAME + "=nome&" +
                PASSWORD + "=pwd";

        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
        int postDataLength = postData.length;

        conn.setRequestMethod(POST_METHOD);
        conn.setRequestProperty("Content-Type", CONTENT_TYPE);
        conn.setRequestProperty("charset", UTF_8);
        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        conn.setDoOutput(true);

        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.write(postData);
        }

        List<String> cookies = conn.getHeaderFields().get("Set-Cookie");
        String cookie = cookies.get(0);

        //get profile before edit
        URL url2 = new URL(LOCAL_URL + GET_PROFILE_INFO_PATH);
        HttpURLConnection conn2 = (HttpURLConnection)url2.openConnection();

        conn2.setRequestProperty("Cookie", cookie);

        conn2.connect();

        BufferedReader br2 = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
        StringBuilder sb2 = new StringBuilder();
        String output2;
        while ((output2 = br2.readLine()) != null)
            sb2.append(output2);

        System.out.println(sb2.toString());

        JSONObject profileBefore = new JSONObject(sb2.toString());

        assertEquals("fn", profileBefore.get(FIRSTNAME));
        assertEquals("ln", profileBefore.get(LASTNAME));
        assertEquals("em", profileBefore.get(EMAIL));


        //edit profile
        URL url3 = new URL(LOCAL_URL + EDIT_PROFILE_PATH);
        HttpURLConnection conn3 = (HttpURLConnection)url3.openConnection();

        String urlParameters2 = FIRSTNAME + "=FaculdadeEngenharia&" +
                LASTNAME + "=UniversidadePorto&" +
                EMAIL + "=feup@porto.pt";

        byte[] postData2 = urlParameters2.getBytes(StandardCharsets.UTF_8);
        int postDataLength2 = postData2.length;

        conn3.setRequestMethod(POST_METHOD);
        conn3.setRequestProperty("Cookie", cookie);
        conn3.setRequestProperty("Content-Type", CONTENT_TYPE);
        conn3.setRequestProperty("charset", UTF_8);
        conn3.setRequestProperty("Content-Length", Integer.toString(postDataLength2));
        conn3.setDoOutput(true);

        try (DataOutputStream wr = new DataOutputStream(conn3.getOutputStream())) {
            wr.write(postData2);
        }

        assertEquals(POST_METHOD, conn3.getRequestMethod());
        assertEquals(HttpServletResponse.SC_OK, conn3.getResponseCode());


        //get profile after edit
        URL url4 = new URL(LOCAL_URL + GET_PROFILE_INFO_PATH);
        HttpURLConnection conn4 = (HttpURLConnection)url4.openConnection();

        conn4.setRequestProperty("Cookie", cookie);

        conn4.connect();

        BufferedReader br3 = new BufferedReader(new InputStreamReader(conn4.getInputStream()));
        StringBuilder sb3 = new StringBuilder();
        String output3;
        while ((output3 = br3.readLine()) != null)
            sb3.append(output3);

        System.out.println(sb3.toString());

        JSONObject profileAfter = new JSONObject(sb3.toString());

        assertEquals("FaculdadeEngenharia", profileAfter.get(FIRSTNAME));
        assertEquals("UniversidadePorto", profileAfter.get(LASTNAME));
        assertEquals("feup@porto.pt", profileAfter.get(EMAIL));

        //delete test entries in db
        service.deleteUserSessions(currentUserID);
        service.deleteAccount(currentUserID);

    }

}