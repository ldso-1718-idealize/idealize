Feature: Save Idea
  Scenario: Username, Title, Synopsis, Keyword, Trends are Passed
    Given the registered user Dummy
    When he saves his idea, with title Dummy_Title, synopsis Dummy_Synopsis, keyword Dummy_Keyword and trends Dummy_Trends
    Then the idea must be stored in the system