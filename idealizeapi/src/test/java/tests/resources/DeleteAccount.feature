Feature: Delete Account
  Scenario: Username is Passed
    Given that the registered user Dummy wants to delete his account
    When Dummy deletes his account
    Then Dummy is no longer registered in the system