package main;

import handlers.mails.*;
import handlers.stats.GetStats;
import handlers.stats.IdeaHandler;
import handlers.stats.Reanalyze;
import handlers.users.*;
import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.util.thread.ScheduledExecutorScheduler;
import services.MySqlBasedUserService;

import javax.servlet.DispatcherType;
import java.io.*;
import java.util.EnumSet;

import static org.eclipse.jetty.util.URIUtil.HTTPS;
import static utils.Configs.*;
import static utils.Utils.*;

public class MainServer
{
    private static final int PORT = 8443;
    private static final int MAX_THREADS = 500;
    private static final int OUTPUT_BUFFER_SIZE = 32768;
    private static final int REQUEST_HEADER_SIZE = 8192;
    private static final int RESPONSE_HEADER_SIZE = 8192;
    private static final String ALLOWED_METHODS = "GET,POST,DELETE,HEAD,OPTIONS";
    private static final String PATH_SPEC = "/*";

    private static final String NO_CONFIG_PROVIDED = "Please provide a config file as argument";

    public static void main(String[] args) throws Exception
    {
        if(args.length != 1)
        {
            System.out.println(NO_CONFIG_PROVIDED);
            System.exit(0);
        }

        readConfig(args[0]);
        Server server = createServer();
        initializeSSL(server);
        startHandlers(server);

        server.start();
        server.join();
    }

    public static Server createServer()
    {
        QueuedThreadPool threadPool = new QueuedThreadPool();
        threadPool.setMaxThreads(MAX_THREADS);

        // Server
        Server server = new Server(threadPool);

        // Scheduler
        server.addBean(new ScheduledExecutorScheduler());

        return server;
    }

    public static void startHandlers(Server server)
    {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath(HOME_PATH);
        server.setHandler(context);

        MySqlBasedUserService service = new MySqlBasedUserService();
        service.setup();

        context.addServlet(new ServletHolder(new RegisterHandler(service)),REGISTER_PATH);
        context.addServlet(new ServletHolder(new LoginHandler(service)),LOGIN_PATH);
        context.addServlet(new ServletHolder(new CheckSessionHandler(service)),CHECK_SESSION_PATH);
        context.addServlet(new ServletHolder(new LogoutHandler(service)),LOGOUT_PATH);
        context.addServlet(new ServletHolder(new DeleteAccountHandler(service)),DELETE_ACCOUNT_PATH);
        context.addServlet(new ServletHolder(new GetStats()),GET_STATS_PATH);
        context.addServlet(new ServletHolder(new GetProfileInfoHandler(service)),GET_PROFILE_INFO_PATH);
        context.addServlet(new ServletHolder(new IdeaHandler(service)),IDEA_PATH);
        context.addServlet(new ServletHolder(new EditProfileHandler(service)),EDIT_PROFILE_PATH);
        context.addServlet(new ServletHolder(new ActivateAccountHandler(service)),ACTIVATION_PATH);
        context.addServlet(new ServletHolder(new PasswordResetCheckHandler(service)),RESET_PASSWORD_CHECK_PATH);
        context.addServlet(new ServletHolder(new PasswordResetHandler(service)),RESET_PASSWORD_PATH);
        context.addServlet(new ServletHolder(new PasswordUpdateHandler(service)),UPDATE_PASSWORD_PATH);
        context.addServlet(new ServletHolder(new TeamHandler(service)),TEAM_PATH);
        context.addServlet(new ServletHolder(new TeamInviteHandler(service)),TEAM_INVITE_PATH);
        context.addServlet(new ServletHolder(new JoinTeamHandler(service)),JOIN_TEAM_PATH);
        context.addServlet(new ServletHolder(new RefuseTeamInviteHandler(service)),REFUSE_INVITE_TEAM_PATH);
        context.addServlet(new ServletHolder(new AddTeamIdeaHandler(service)),ADD_TEAM_IDEA_PATH);
        context.addServlet(new ServletHolder(new GetTeamsHandler(service)),GET_TEAMS_PATH);
        context.addServlet(new ServletHolder(new Reanalyze(service)),REANALYZE_PATH);
        context.addServlet(new ServletHolder(new ShareIdeaHandler(service)),SHARE_IDEA_PATH);
        context.addServlet(new ServletHolder(new CheckShareHandler(service)),CHECK_SHARE_PATH);
        context.addServlet(new ServletHolder(new AddSharedIdeaHandler(service)),ADD_SHARED_IDEA_PATH);
        context.addServlet(new ServletHolder(new RemoveTeamIdeaHandler(service)),REMOVED_SHARED_IDEA_PATH);
        context.addServlet(new ServletHolder(new RemoveTeamMemberHandler(service)),REMOVED_TEAM_MEMBER_PATH);
        context.addServlet(new ServletHolder(new FavoriteIdeaHandler(service)),FAVORITE_IDEA_PATH);
        context.addServlet(new ServletHolder(new DeleteIdeaHandler(service)),DELETE_IDEA_PATH);

        FilterHolder holder = new FilterHolder(new CrossOriginFilter());
        holder.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, ALLOWED_ORIGINS);
        holder.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, ALLOWED_METHODS);
        context.addFilter(holder, PATH_SPEC, EnumSet.of(DispatcherType.REQUEST));
    }

    public static void initializeSSL(Server server) throws FileNotFoundException
    {
        File file = new File(KEYSTORE_PATH);
        if(!file.exists())
        {
            throw new FileNotFoundException(file.getAbsolutePath());
        }

        String path = file.getAbsolutePath();

        // HTTP Configuration
        HttpConfiguration http_config = new HttpConfiguration();
        http_config.setSecureScheme(HTTPS);
        http_config.setSecurePort(PORT);
        http_config.setOutputBufferSize(OUTPUT_BUFFER_SIZE);
        http_config.setRequestHeaderSize(REQUEST_HEADER_SIZE);
        http_config.setResponseHeaderSize(RESPONSE_HEADER_SIZE);
        http_config.setSendServerVersion(true);
        http_config.setSendDateHeader(false);

        HttpConfiguration ssl_config = new HttpConfiguration(http_config);
        ssl_config.addCustomizer(new SecureRequestCustomizer(true));

        // SSL Context Factory
        SslContextFactory sslContextFactory = new SslContextFactory();
        sslContextFactory.setKeyStorePath(path);
        sslContextFactory.setKeyStorePassword(KEYSTORE_PASSWORD);
        sslContextFactory.setKeyManagerPassword(KEY_MANAGER_PASSWORD);
        sslContextFactory.setTrustStorePath(path);
        sslContextFactory.setTrustStorePassword(TRUSTSTORE_PASSWORD);
        sslContextFactory.setIncludeCipherSuites(CYPHER_SUITS_1, CYPHER_SUITS_2, CYPHER_SUITS_3);

        // SSL HTTP Configuration
        HttpConfiguration https_config = new HttpConfiguration(http_config);
        https_config.addCustomizer(new SecureRequestCustomizer());

        // SSL Connector
        ServerConnector sslConnector = new ServerConnector(server,
                new SslConnectionFactory(sslContextFactory, HttpVersion.HTTP_1_1.asString()),
                new HttpConnectionFactory(ssl_config));
        sslConnector.setPort(PORT);
        server.addConnector(sslConnector);
    }
}