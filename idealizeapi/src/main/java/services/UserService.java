package services;

import org.json.JSONArray;

public interface UserService
{
    boolean register(String username, String password, String firstname, String lastname, String email);

    String login(String username, String password);

    String getUsernameFromSession(String token);

    boolean logout(String token);

    boolean deleteAccount(int id);

    String getProfileInfo(String username);

    int saveIdea(String title, int idAuthor, String synopsis, String keyword, String trends);

    String getIdea(int id);

    JSONArray getIdeaKeyword(int id);

    boolean editProfile(int userID, String firstname, String lastname, String email);

    int createTeam(int founderID, String name);

    int teamInvite(int teamID, int userID);

    String getUserInvites(int userID);

    boolean acceptTeamInvite(int teamID, int userID);

    String getUserTeams(int userID);

}
