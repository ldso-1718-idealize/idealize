package services;

/**
 * Created by furys on 23-11-2017.
 */
public interface MailService {
    boolean sendEmail(String address, String subj, String body);
}
