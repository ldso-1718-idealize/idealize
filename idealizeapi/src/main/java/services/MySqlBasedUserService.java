package services;

import org.json.JSONArray;
import org.json.JSONObject;
import utils.Utils;

import com.mchange.v2.c3p0.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.UUID;

import static utils.Configs.*;
import static utils.Utils.*;

public class MySqlBasedUserService implements UserService
{
    private static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://";
    private static final String USE_SSL = "?useSSL=true";
    private static final String SEPARATOR = "/";
    private static final String HOST_PORT_SEPARATOR = ":";
    private static final int MAX_STATEMENTS = 180;
    private static final int MAX_POOL_SIZE = 50;
    private static final int TEST_TIME = 4*60*60;
    private static final int CHECKOUT_TIMEOUT = 5000;

    //user
    private static final String CREATE_USER_QUERY = "insert into user(username, password, firstname, lastname, email) values(?,?,?,?,?);";
    private static final String INSERT_USER_ACTIVATION_QUERY = "insert into activation(id, hash) values(?,?);";
    private static final String ACCOUNT_ACTIVATED_QUERY = "select * from user where username = ?;";
    private static final String DELETE_ACTIVATION_QUERY = "delete from activation where id = ?;";
    private static final String GET_USER_FROM_ACTIVATION = "select * from activation where hash = ?;";
    private static final String ACTIVATE_ACCOUNT_QUERY = "update user set active = true where id = ?;";
    private static final String AUTHENTICATE_USER_QUERY = "select * from user where username = ? and password = ?;";
    private static final String CREATE_SESSION_QUERY = "insert into session(token, id) values(?,?);";
    private static final String GET_USER_FROM_SESSION_QUERY = "select * from session inner join user on user.id = session.id where token = ?;";
    private static final String DESTROY_SESSION_QUERY = "delete from session where token = ?;";
    private static final String DELETE_ACCOUNT_QUERY = "delete from user where id = ?;";
    private static final String DELETE_ACCOUNT_WITH_USERNAME_QUERY = "delete from user where username = ?;";
    private static final String GET_USERID_FROM_USERNAME_QUERY = "select id from user where username = ?;";
    private static final String GET_USERNAME_FROM_USERID = "select username from user where id = ?;";
    private static final String DELETE_USER_SESSIONS_QUERY = "delete from session where id = ?;";
    private static final String GET_PROFILE_QUERY = "select id, username, firstname, lastname, email from user where username = ?;";
    private static final String GET_USER_IDEAS = "select id, title, creationdate from idea where idauthor = ?;";
    private static final String GET_USER_SHARED_IDEAS = "select id, idauthor, title, creationdate from idea inner join idea_share on ididea = id where iduser = ?;";
    private static final String EDIT_PROFILE_QUERY = "update user set firstname = ?, lastname = ?, email = ? where id = ?;";
    private static final String GET_USER_FIRSTNAME_QUERY = "select firstname from user where id = ?;";
    private static final String GET_USER_LASTNAME_QUERY = "select lastname from user where id = ?;";
    private static final String GET_USER_EMAIL_QUERY = "select email from user where id = ?;";
    private static final String GET_USER_TEAMS = "select id, idfounder, name from team join team_member on team.id = team_member.idteam where idmember = ?;";
    private static final String CHANGE_PASSWORD_QUERY = "update user set password = ? where id = ?;";
    private static final String INSERT_PASSWORD_RESET_QUERY = "insert into password_reset(id, hash) values(?,?);";
    private static final String DELETE_PASSWORD_RESET_QUERY = "delete from password_reset where id = ?;";
    private static final String GET_USER_ID_FROM_PASSWORD_RESET_QUERY = "select id from password_reset where hash = ?";

    //idea
    private static final String SAVE_IDEA_QUERY = "insert into idea(title, idauthor, synopsis, keyword, trends) values(?,?,?,?,?);";
    private static final String SAVE_IDEA_QUERY2 = "insert into idea(title, idauthor, synopsis, keyword, trends, country) values(?,?,?,?,?,?);";
    private static final String GET_IDEA_QUERY = "select * from idea where id = ?;";
    private static final String DELETE_IDEA_QUERY = "delete from idea where title = ?;";
    private static final String DELETE_IDEA_BY_ID_QUERY = "delete from idea where id = ?;";
    private static final String DELETE_IDEA_KEYWORD_QUERY = "delete from idea_keyword where ididea = ?;";
    private static final String DELETE_IDEA_SHARE_QUERY = "delete from idea_share where ididea = ?;";
    private static final String DELETE_IDEA_SHARE_HASH_QUERY = "delete from share_hash where ididea = ?;";
    private static final String DELETE_FROM_FAVORITE_IDEA_QUERY = "delete from idea_favorite where ididea = ?;";
    private static final String CREATE_IDEA_KEYWORD_QUERY = "insert into idea_keyword(ididea, keyword) values(?,?);";
    private static final String GET_IDEA_KEYWORD_QUERY = "select keyword from idea_keyword where ididea = ?;";
    private static final String GET_IDEA_ID_FROM_SHARE_HASH_QUERY = "select ididea from share_hash where hash = ?";
    private static final String GET_IDEA_BASIC_INFO_BY_ID_QUERY = "select title, creationdate from idea where id = ?";
    private static final String FAVORITE_IDEA_QUERY = "insert into idea_favorite(ididea, iduser) values(?,?);";
    private static final String UNFAVORITE_IDEA = "delete from idea_favorite where ididea = ? and iduser = ?;";
    private static final String GET_FAVORITE_IDEA = "select * from idea_favorite where ididea = ? and iduser = ?;";
    private static final String CHECK_IDEA_FAVORITE_QUERY = "select * from idea_favorite where ididea = ? and iduser = ?;";

    //team
    private static final String CREATE_TEAM_QUERY = "insert into team(idfounder, name) values(?,?);";
    private static final String CREATE_TEAM_INVITE_QUERY = "insert into team_invite(idteam, iduser) values(?,?);";
    private static final String GET_USER_TEAM_INVITES_QUERY = "select idteam, accepted from team_invite where iduser = ?;";
    private static final String CREATE_TEAM_MEMBER_QUERY = "insert into team_member(idteam, idmember) values(?,?);";
    private static final String ACCEPT_TEAM_INVITE_QUERY = "update team_invite set accepted = ? where idteam = ? and iduser = ?;";
    private static final String DELETE_TEAM_INVITE_QUERY = "delete from team_invite where idteam = ? and iduser = ?;";
    private static final String CREATE_TEAM_IDEA_QUERY = "insert into team_idea(idteam, ididea) values(?,?);";
    private static final String REMOVE_TEAM_IDEA_QUERY = "delete from team_idea where idteam = ? and ididea = ?;";
    private static final String DELETE_TEAM_IDEA_QUERY = "delete from team_idea where ididea = ?;";
    private static final String REMOVE_TEAM_MEMBER_QUERY = "delete from team_member where idteam = ? and idmember = ?;";
    private static final String GET_TEAMID_FROM_NAME_QUERY = "select id from team where name = ?;";
    private static final String GET_TEAMNAME_FROM_ID_QUERY = "select name from team where id = ?;";
    private static final String JOIN_TEAM_TEAM_MEMBER = "select * from team_member where idteam = ?;";
    private static final String JOIN_TEAM_TEAM_IDEA = "select * from team join team_idea on team.id = team_idea.idteam where team.id = ?;";


    //other
    private static final String INSERT_SHARE_HASH_QUERY = "insert into share_hash(ididea, hash) values(?,?);";
    private static final String DELETE_SHARE_HASH_QUERY = "delete from share_hash where hash = ?;";
    private static final String INSERT_SHARE_IDEA_QUERY = "insert into idea_share(ididea, iduser) values(?,?);";

    private ComboPooledDataSource cpds;

    public void setup()
    {
        try
        {
            cpds = new ComboPooledDataSource();
            cpds.setDriverClass(DRIVER_NAME); //loads the jdbc driver
            cpds.setJdbcUrl(DB_URL + DB_HOST + HOST_PORT_SEPARATOR + DB_PORT + SEPARATOR + DB_NAME + USE_SSL);
            cpds.setUser(DB_USERNAME);
            cpds.setPassword(DB_PASSWORD);
            cpds.setMaxStatements(MAX_STATEMENTS);
            cpds.setMaxPoolSize(MAX_POOL_SIZE);
            cpds.setIdleConnectionTestPeriod(TEST_TIME);
            cpds.setCheckoutTimeout(CHECKOUT_TIMEOUT);
            Connection test = cpds.getConnection();
            test.close();
        }
        catch(Exception e)
        {
            System.out.println(CONNECTION_FAILED + e.getMessage());
            System.exit(1);
        }
    }

    public boolean register(String username, String password, String firstname, String lastname, String email)
    {
        return register(username, password, firstname, lastname, email, false);
    }

    public boolean register(String username, String password, String firstname, String lastname, String email, boolean activate)
    {
        Connection con = getConnection();

        boolean res = true;

        try{
            PreparedStatement stmt = con.prepareStatement(CREATE_USER_QUERY);
            stmt.setString(1, username);
            stmt.setString(2, Utils.applySHA(password));
            stmt.setString(3, firstname);
            stmt.setString(4, lastname);
            stmt.setString(5, email);
            stmt.execute();

            String token = UUID.randomUUID().toString();

            int id = checkLogin(username, password);

            if(id == ID_NOT_FOUND)
            {
                res = false;
            }
            else
            {
                if(activate)
                {
                    PreparedStatement stmt2 = con.prepareStatement(INSERT_USER_ACTIVATION_QUERY);
                    stmt2.setInt(1, id);
                    stmt2.setString(2, token);
                    stmt2.execute();

                    SendGridMailService mailService = new SendGridMailService();

                    mailService.sendEmail(email, ACTIVATION_SUBJECT, ACTIVATION_BODY + FRONT_END_DOMAIN + ACTIVATION_PATH + "?hash=" + token);
                }
                else
                {
                    PreparedStatement stmt2 = con.prepareStatement(ACTIVATE_ACCOUNT_QUERY);
                    stmt2.setInt(1, id);
                    stmt2.execute();
                }
            }
        }
        catch(Exception e){
            System.out.println(REGISTER_FAILED);
            e.printStackTrace();
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public boolean activateAccount(String token)
    {
        Connection con = getConnection();

        boolean res = true;

        try{
            PreparedStatement stmt = con.prepareStatement(GET_USER_FROM_ACTIVATION);
            stmt.setString(1, token);
            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                int id = rs.getInt(ID);

                PreparedStatement stmt2 = con.prepareStatement(ACTIVATE_ACCOUNT_QUERY);
                stmt2.setInt(1, id);
                stmt2.execute();

                PreparedStatement stmt3 = con.prepareStatement(DELETE_ACTIVATION_QUERY);
                stmt3.setInt(1, id);
                stmt3.execute();
            }
            else
            {
                System.out.println(ACTIVATION_FAILED);
                res = false;
            }
        }
        catch(Exception e){
            System.out.println(ACTIVATION_FAILED);
            e.printStackTrace();
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public String login(String username, String password)
    {
        Connection con = getConnection();

        int id = checkLogin(username, password);

        String res;

        if(id != ID_NOT_FOUND)
        {
            if(!accountActivated(username))
            {
                closeConnection(con);
                return ACCOUNT_NOT_ACTIVATED;
            }

            res = createSession(id);

            if(res == null)
            {
                closeConnection(con);
                res = AUTHENTICATION_FAILED;
            }
        }
        else
        {
            res = AUTHENTICATION_FAILED;
        }

        closeConnection(con);

        return res;
    }

    public boolean accountActivated(String username)
    {
        Connection con = getConnection();

        boolean res;

        try{
            PreparedStatement stmt = con.prepareStatement(ACCOUNT_ACTIVATED_QUERY);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                res = rs.getBoolean(ACTIVE);
            }
            else
            {
                res = false;
            }
        }
        catch(Exception e){
            System.out.println(ACCOUNT_NOT_ACTIVATED);
            e.printStackTrace();
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public String createSession(int id)
    {
        Connection con = getConnection();

        String token = UUID.randomUUID().toString();

        try{
            PreparedStatement stmt = con.prepareStatement(CREATE_SESSION_QUERY);
            stmt.setString(1, token);
            stmt.setInt(2, id);
            stmt.execute();
        }
        catch(Exception e){
            System.out.println(SESSION_CREATION_FAILED);
            token = null;
        }

        closeConnection(con);

        return token;
    }

    public int checkLogin(String username, String password)
    {
        Connection con = getConnection();

        int id = ID_NOT_FOUND;

        try{
            PreparedStatement stmt = con.prepareStatement(AUTHENTICATE_USER_QUERY);
            stmt.setString(1, username);
            stmt.setString(2, Utils.applySHA(password));
            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                id = rs.getInt(ID);
            }
        }
        catch(Exception e){
            System.out.println(AUTHENTICATION_FAILED);
        }

        closeConnection(con);

        return id;
    }

    public String getUsernameFromSession(String token)
    {
        Connection con = getConnection();

        String username = SESSION_NOT_FOUND;

        try{
            PreparedStatement stmt = con.prepareStatement(GET_USER_FROM_SESSION_QUERY);
            stmt.setString(1, token);
            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                username = rs.getString(USERNAME);
            }
        }
        catch(Exception e){
            System.out.println(SESSION_NOT_FOUND);
        }

        closeConnection(con);

        return username;
    }

    public boolean logout(String token)
    {
        Connection con = getConnection();

        boolean res = true;

        try{
            PreparedStatement stmt = con.prepareStatement(DESTROY_SESSION_QUERY);
            stmt.setString(1, token);
            stmt.execute();
        }
        catch(Exception e){
            System.out.println(SESSION_NOT_FOUND);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public boolean deleteAccount(int id)
    {
        Connection con = getConnection();

        boolean res = true;

        try{
            PreparedStatement stmt = con.prepareStatement(DELETE_ACCOUNT_QUERY);
            stmt.setInt(1, id);
            stmt.execute();
        }
        catch(Exception e){
            System.out.println(DELETE_ACCOUNT_ERROR);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public boolean deleteAccount(String username)
    {
        Connection con = getConnection();

        boolean res = true;

        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_ACCOUNT_WITH_USERNAME_QUERY);
            stmt.setString(1, username);
            stmt.execute();
        } catch (Exception e) {
            System.out.println(DELETE_ACCOUNT_ERROR);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public int getUserID(String username)
    {
        Connection con = getConnection();
        int userID = -1;

        try{
            PreparedStatement stmt = con.prepareStatement(GET_USERID_FROM_USERNAME_QUERY);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                userID = Integer.parseInt(rs.getString(ID));
            }
        }
        catch(Exception e){
            System.out.println(SESSION_NOT_FOUND);
        }

        closeConnection(con);

        return userID;
    }

    public boolean deleteUserSessions(int id)
    {
        Connection con = getConnection();

        boolean res = true;

        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_USER_SESSIONS_QUERY);
            stmt.setInt(1, id);
            stmt.execute();
        } catch (Exception e) {
            System.out.println(SESSION_NOT_FOUND);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public String getProfileInfo(String username)
    {
        Connection con = getConnection();

        JSONObject obj = new JSONObject();

        try {
            PreparedStatement stmt = con.prepareStatement(GET_PROFILE_QUERY);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                obj.put(USERNAME, rs.getString(USERNAME));
                obj.put(FIRSTNAME, rs.getString(FIRSTNAME));
                obj.put(LASTNAME, rs.getString(LASTNAME));
                obj.put(EMAIL, rs.getString(EMAIL));

                JSONArray arr = new JSONArray();

                PreparedStatement stmt2 = con.prepareStatement(GET_USER_IDEAS);
                stmt2.setString(1, rs.getString(ID));
                ResultSet rs2 = stmt2.executeQuery();

                while(rs2.next())
                {
                    JSONObject obj2 = new JSONObject();
                    obj2.put(ID, rs2.getString(ID));
                    obj2.put(TITLE, rs2.getString(TITLE));
                    String date = rs2.getString(CREATION_DATE);
                    obj2.put(CREATION_DATE, date.substring(0, date.indexOf('.')));

                    PreparedStatement stmt3 = con.prepareStatement(CHECK_IDEA_FAVORITE_QUERY);
                    stmt3.setString(1, rs2.getString(ID));
                    stmt3.setString(2, rs.getString(ID));
                    ResultSet rs3 = stmt3.executeQuery();

                    if(rs3.next())
                    {
                        obj2.put(FAVORITE, true);
                    }

                    arr.put(obj2);
                }

                PreparedStatement stmt4 = con.prepareStatement(GET_USER_SHARED_IDEAS);
                stmt4.setString(1, rs.getString(ID));
                ResultSet rs4 = stmt4.executeQuery();

                while(rs4.next())
                {
                    JSONObject obj2 = new JSONObject();
                    obj2.put(ID, rs4.getString(ID));
                    obj2.put(TITLE, rs4.getString(TITLE));
                    String date = rs4.getString(CREATION_DATE);
                    obj2.put(CREATION_DATE, date.substring(0, date.indexOf('.')));
                    obj2.put(AUTHOR, getUsernameFromID(rs4.getInt(ID_AUTHOR)));

                    PreparedStatement stmt5 = con.prepareStatement(CHECK_IDEA_FAVORITE_QUERY);
                    stmt5.setString(1, rs4.getString(ID));
                    stmt5.setString(2, rs.getString(ID));
                    ResultSet rs5 = stmt5.executeQuery();

                    if(rs5.next())
                    {
                        obj2.put(FAVORITE, true);
                    }

                    arr.put(obj2);
                }

                obj.put(IDEAS, arr);
            }
        } catch (Exception e) {
            System.out.println(USER_NOT_FOUND);
        }

        closeConnection(con);

        return obj.toString();
    }

    public int saveIdea(String title, int idAuthor, String synopsis, String keyword, String trends){
        Connection con = getConnection();

        int lastID = -1;

        try{
            PreparedStatement stmt = con.prepareStatement(SAVE_IDEA_QUERY, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, title);
            stmt.setInt(2, idAuthor);
            stmt.setString(3, synopsis);
            stmt.setString(4, keyword);
            stmt.setString(5, trends);
            stmt.execute();

            ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next())
            {
                lastID = rs.getInt(1);
            }

        }
        catch(Exception e){
            System.out.println(SAVE_IDEA_FAILED);
            e.printStackTrace();
        }

        closeConnection(con);

        return lastID;
    }

    public int saveIdea(String title, int idAuthor, String synopsis, String keyword, String trends, String country){
        Connection con = getConnection();

        int lastID = -1;

        try{
            PreparedStatement stmt = con.prepareStatement(SAVE_IDEA_QUERY2, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, title);
            stmt.setInt(2, idAuthor);
            stmt.setString(3, synopsis);
            stmt.setString(4, keyword);
            stmt.setString(5, trends);
            stmt.setString(6, country);
            stmt.execute();

            ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next())
            {
                lastID = rs.getInt(1);
            }

        }
        catch(Exception e){
            System.out.println(SAVE_IDEA_FAILED);
            e.printStackTrace();
        }

        closeConnection(con);

        return lastID;
    }

    public String getIdea(int id){

        Connection con = getConnection();

        JSONObject obj = new JSONObject();

        try {

            PreparedStatement stmt = con.prepareStatement(GET_IDEA_QUERY);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                obj.put(TITLE, rs.getString(TITLE));
                obj.put(IDAUTHOR, rs.getString(IDAUTHOR));
                obj.put(SYNOPSIS, rs.getString(SYNOPSIS));
                obj.put(KEYWORD, rs.getString(KEYWORD));
                obj.put(KEYWORDS, getIdeaKeyword(id));
                obj.put(TRENDS, new JSONObject(rs.getString(TRENDS)));
                String date = rs.getString(CREATION_DATE);
                obj.put(CREATION_DATE, date.substring(0, date.indexOf('.')));
                obj.put(COUNTRY, rs.getString(COUNTRY));
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(IDEA_NOT_FOUND);
        }

        closeConnection(con);

        return obj.toString();
    }

    public boolean createIdeaKeyword(int idIdea, String keyword){
        Connection con = getConnection();

        boolean res = true;

        try{
            PreparedStatement stmt = con.prepareStatement(CREATE_IDEA_KEYWORD_QUERY);
            stmt.setInt(1, idIdea);
            stmt.setString(2, keyword);
            stmt.execute();
        }
        catch(Exception e){
            System.out.println(IDEA_KEYWORD_ERROR);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public JSONArray getIdeaKeyword(int id){
        Connection con = getConnection();

        JSONArray keywords = new JSONArray();

        try {
            PreparedStatement stmt = con.prepareStatement(GET_IDEA_KEYWORD_QUERY);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while(rs.next())
            {
                keywords.put(rs.getString(KEYWORD));
            }

        } catch (Exception e) {
            System.out.println(USER_NOT_FOUND);
        }

        closeConnection(con);

        return keywords;
    }

    public boolean editProfile(int userID, String firstname, String lastname, String email){
        Connection con = getConnection();

        boolean res = true;

        try{
            PreparedStatement stmt = con.prepareStatement(EDIT_PROFILE_QUERY);
            stmt.setString(1, firstname);
            stmt.setString(2, lastname);
            stmt.setString(3, email);
            stmt.setInt(4, userID);

            stmt.execute();
        }
        catch(Exception e){
            System.out.println(UPDATE_PROFILE_ERROR);
            res = false;
        }

        closeConnection(con);

        return res;

    }

    public int createTeam(int founderID, String name) {

        Connection con = getConnection();

        int lastID = -1;

        try{
            PreparedStatement stmt = con.prepareStatement(CREATE_TEAM_QUERY, Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, founderID);
            stmt.setString(2, name);

            stmt.execute();

            ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next())
            {
                lastID = rs.getInt(1);
            }

        }
        catch(Exception e){
            System.out.println(CREATE_TEAM_FAILED);
            e.printStackTrace();
        }

        closeConnection(con);

        return lastID;

    }

    public int teamInvite(int teamID, int userID) {

        Connection con = getConnection();

        int lastID = -1;

        try{
            PreparedStatement stmt = con.prepareStatement(CREATE_TEAM_INVITE_QUERY, Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, teamID);
            stmt.setInt(2, userID);

            stmt.execute();

            ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next())
            {
                lastID = rs.getInt(1);
            }

        }
        catch(Exception e){
            System.out.println(CREATE_TEAM_INVITE_FAILED);
            e.printStackTrace();
        }

        closeConnection(con);

        return lastID;

    }

    public String getTeamNameFromID(int teamID) {
        Connection con = getConnection();

        String teamName = EMPTY_STRING;

        try{
            PreparedStatement stmt = con.prepareStatement(GET_TEAMNAME_FROM_ID_QUERY);
            stmt.setInt(1, teamID);
            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                teamName = rs.getString(NAME);
            }
        }
        catch(Exception e){
            System.out.println(TEAM_NOT_FOUND);
        }

        closeConnection(con);

        return teamName;
    }

    public String getUserInvites(int userID) {

        Connection con = getConnection();

        JSONArray arr = new JSONArray();

        try {

            PreparedStatement stmt = con.prepareStatement(GET_USER_TEAM_INVITES_QUERY);
            stmt.setInt(1, userID);
            ResultSet rs = stmt.executeQuery();

            while(rs.next())
            {
                JSONObject obj = new JSONObject();

                String teamName = getTeamNameFromID(rs.getInt(IDTEAM));

                obj.put(NAME, teamName);

                String status;

                if(rs.getBoolean(ACCEPTED))
                    status = ACCEPTED;
                else status = PENDING;

                obj.put(STATUS, status);
                obj.put(ID, rs.getInt(IDTEAM));

                arr.put(obj);

            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(INVITE_NOT_FOUND);
        }

        closeConnection(con);

        return arr.toString();
    }

    public boolean createTeamMember(int teamID, int memberID) {
        Connection con = getConnection();

        boolean res = true;

        try{
            PreparedStatement stmt = con.prepareStatement(CREATE_TEAM_MEMBER_QUERY);
            stmt.setInt(1, teamID);
            stmt.setInt(2, memberID);
            stmt.execute();
        }
        catch(Exception e){
            System.out.println(CREATE_TEAM_MEMBER_FAILED);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public boolean acceptTeamInvite(int teamID, int userID){
        Connection con = getConnection();

        boolean accept = true;

        try{
            PreparedStatement stmt = con.prepareStatement(ACCEPT_TEAM_INVITE_QUERY);
            stmt.setBoolean(1, accept);
            stmt.setInt(2, teamID);
            stmt.setInt(3, userID);
            stmt.execute();
        }
        catch(Exception e){
            System.out.println(ACCEPT_TEAM_INVITE_FAILED);
            accept = false;
        }

        closeConnection(con);

        return accept;
    }

    public boolean declineTeamInvite(int teamID, int userID){
        Connection con = getConnection();

        boolean accept = true;

        try{
            PreparedStatement stmt = con.prepareStatement(DELETE_TEAM_INVITE_QUERY);
            stmt.setInt(1, teamID);
            stmt.setInt(2, userID);
            stmt.execute();
        }
        catch(Exception e){
            System.out.println(DECLINE_TEAM_INVITE_FAILED);
            accept = false;
        }

        closeConnection(con);

        return accept;
    }

    public boolean deleteTeamInvite(int teamID, int userID){
        Connection con = getConnection();

        boolean accept = true;

        try{
            PreparedStatement stmt = con.prepareStatement(DELETE_TEAM_INVITE_QUERY);
            stmt.setInt(1, teamID);
            stmt.setInt(2, userID);
            stmt.execute();
        }
        catch(Exception e){
            System.out.println(DELETE_TEAM_INVITE_FAILED);
            accept = false;
        }

        closeConnection(con);

        return accept;
    }

    public boolean addTeamIdea(int teamID, int ideaID) {
        Connection con = getConnection();

        boolean res = true;

        try{
            PreparedStatement stmt = con.prepareStatement(CREATE_TEAM_IDEA_QUERY);
            stmt.setInt(1, teamID);
            stmt.setInt(2, ideaID);
            stmt.execute();
        }
        catch(Exception e){
            System.out.println(CREATE_TEAM_IDEA_FAILED);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public boolean removeTeamIdea(int teamID, int ideaID) {
        Connection con = getConnection();

        boolean res = true;

        try{
            PreparedStatement stmt = con.prepareStatement(REMOVE_TEAM_IDEA_QUERY);
            stmt.setInt(1, teamID);
            stmt.setInt(2, ideaID);
            stmt.execute();
        }
        catch(Exception e){
            System.out.println(REMOVE_TEAM_IDEA_FAILED);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public boolean removeTeamMember(int teamID, int userID) {
        Connection con = getConnection();

        boolean res = true;

        try{
            PreparedStatement stmt = con.prepareStatement(REMOVE_TEAM_MEMBER_QUERY);
            stmt.setInt(1, teamID);
            stmt.setInt(2, userID);
            stmt.execute();
        }
        catch(Exception e){
            System.out.println(REMOVE_TEAM_MEMBER_FAILED);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public String getUsernameFromID(int userID)
    {
        Connection con = getConnection();

        String username = "";

        try{
            PreparedStatement stmt = con.prepareStatement(GET_USERNAME_FROM_USERID);
            stmt.setInt(1, userID);
            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                username = rs.getString(USERNAME);
            }
        }
        catch(Exception e){
            System.out.println(USER_NOT_FOUND);
        }

        closeConnection(con);

        return username;
    }

    public String getUserTeams(int userID) {

        Connection con = getConnection();

        JSONArray arr = new JSONArray();

        try {

            //get teams
            PreparedStatement stmt = con.prepareStatement(GET_USER_TEAMS);
            stmt.setInt(1, userID);
            ResultSet rs = stmt.executeQuery();

            //for each team
            while(rs.next())
            {
                //team basic info
                JSONObject obj = new JSONObject();

                int teamID = rs.getInt(ID);
                int idFounder = rs.getInt(FOUNDERID);
                String teamName = rs.getString(NAME);

                boolean admin = false;
                if(userID == idFounder)
                    admin = true;

                obj.put(ID, teamID);
                obj.put(FOUNDERID, idFounder);
                obj.put(NAME, teamName);
                obj.put(ADMIN, admin);


                //get team members
                JSONArray arr2 = new JSONArray();

                PreparedStatement stmt2 = con.prepareStatement(JOIN_TEAM_TEAM_MEMBER);
                stmt2.setInt(1, teamID);
                ResultSet rs2 = stmt2.executeQuery();

                while(rs2.next()) {

                    JSONObject obj2 = new JSONObject();

                    int memberID = rs2.getInt(MEMBERID);

                    String memberName = getUsernameFromID(memberID);

                    obj2.put(NAME, memberName);
                    obj2.put(ID, memberID);

                    arr2.put(obj2);

                }

                obj.put(MEMBERS, arr2);


                //get team ideas
                JSONArray arr3 = new JSONArray();

                PreparedStatement stmt3 = con.prepareStatement(JOIN_TEAM_TEAM_IDEA);
                stmt3.setInt(1, teamID);
                ResultSet rs3 = stmt3.executeQuery();

                while(rs3.next()) {

                    JSONObject obj3 = new JSONObject();

                    int ideaID = rs3.getInt(IDIDEA);

                    PreparedStatement stmt4 = con.prepareStatement(GET_IDEA_BASIC_INFO_BY_ID_QUERY);
                    stmt4.setInt(1, ideaID);
                    ResultSet rs4 = stmt4.executeQuery();

                    if(rs4.next()) {
                        String title = rs4.getString(TITLE);
                        String creationDate = rs4.getString(CREATION_DATE);

                        obj3.put(ID, ideaID);
                        obj3.put(TITLE, title);
                        obj3.put(CREATION_DATE, creationDate.substring(0, creationDate.indexOf('.')));

                    }

                    arr3.put(obj3);

                }

                obj.put(IDEAS, arr3);


                //wrap all
                arr.put(obj);

            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(TEAM_NOT_FOUND);
        }

        closeConnection(con);

        return arr.toString();
    }

    public int getTeamID(String teamName)
    {
        Connection con = getConnection();
        int teamID = -1;

        try{
            PreparedStatement stmt = con.prepareStatement(GET_TEAMID_FROM_NAME_QUERY);
            stmt.setString(1, teamName);
            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                teamID = Integer.parseInt(rs.getString(ID));
            }
        }
        catch(Exception e){
            System.out.println(SESSION_NOT_FOUND);
        }

        closeConnection(con);

        return teamID;
    }

    public String getUserFirstname(int userID){
        Connection con = getConnection();

        String fn = null;

        try {
            PreparedStatement stmt = con.prepareStatement(GET_USER_FIRSTNAME_QUERY);
            stmt.setInt(1, userID);
            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                fn = rs.getString(FIRSTNAME);
            }
        } catch (Exception e) {
            System.out.println(USER_NOT_FOUND);
        }

        closeConnection(con);

        return fn;
    }

    public String getUserLastname(int userID){
        Connection con = getConnection();

        String ln = null;

        try {
            PreparedStatement stmt = con.prepareStatement(GET_USER_LASTNAME_QUERY);
            stmt.setInt(1, userID);
            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                ln = rs.getString(LASTNAME);
            }
        } catch (Exception e) {
            System.out.println(USER_NOT_FOUND);
        }

        closeConnection(con);

        return ln;
    }

    public String getUserEmail(int userID){
        Connection con = getConnection();

        String em = null;

        try {
            PreparedStatement stmt = con.prepareStatement(GET_USER_EMAIL_QUERY);
            stmt.setInt(1, userID);
            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                em = rs.getString(EMAIL);
            }
        } catch (Exception e) {
            System.out.println(USER_NOT_FOUND);
        }

        closeConnection(con);

        return em;
    }

    public boolean deleteIdea(String title){
        Connection con = getConnection();

        boolean res = true;

        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_IDEA_QUERY);
            stmt.setString(1, title);
            stmt.execute();
        } catch (Exception e) {
            System.out.println(IDEA_NOT_FOUND);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public boolean deleteIdea(int id){
        Connection con = getConnection();

        boolean res = false;

        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_IDEA_BY_ID_QUERY);
            stmt.setInt(1, id);
            stmt.execute();

            res = true;
        } catch (Exception e) {
            System.out.println(IDEA_NOT_FOUND);
        }

        try {
            PreparedStatement stmt2 = con.prepareStatement(DELETE_IDEA_KEYWORD_QUERY);
            stmt2.setInt(1, id);
            stmt2.execute();

            res = true;
        } catch (Exception e) {
            System.out.println(IDEA_NOT_FOUND);
        }

        try {
            PreparedStatement stmt3 = con.prepareStatement(DELETE_IDEA_SHARE_QUERY);
            stmt3.setInt(1, id);
            stmt3.execute();

            res = true;
        } catch (Exception e) {
            System.out.println(IDEA_NOT_FOUND);
        }

        try {
            PreparedStatement stmt4 = con.prepareStatement(DELETE_IDEA_SHARE_HASH_QUERY);
            stmt4.setInt(1, id);
            stmt4.execute();

            res = true;
        } catch (Exception e) {
            System.out.println(IDEA_NOT_FOUND);
        }

        try {
            PreparedStatement stmt5 = con.prepareStatement(DELETE_TEAM_IDEA_QUERY);
            stmt5.setInt(1, id);
            stmt5.execute();

            res = true;
        } catch (Exception e) {
            System.out.println(IDEA_NOT_FOUND);
        }

        try {
            PreparedStatement stmt6 = con.prepareStatement(DELETE_FROM_FAVORITE_IDEA_QUERY);
            stmt6.setInt(1, id);
            stmt6.execute();

            res = true;
        } catch (Exception e) {
            System.out.println(IDEA_NOT_FOUND);
        }

        closeConnection(con);

        return res;
    }

    public boolean requestPasswordReset(String username){
        Connection con = getConnection();

        boolean res = true;

        int id = getUserID(username);

        if(id == ID_NOT_FOUND)
        {
            closeConnection(con);
            return false;
        }

        String email = getUserEmail(id);

        if(email == null)
        {
            closeConnection(con);
            return false;
        }

        String token = UUID.randomUUID().toString();

        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_PASSWORD_RESET_QUERY);
            stmt.setInt(1, id);
            stmt.setString(2, token);
            stmt.execute();

            SendGridMailService mailService = new SendGridMailService();

            mailService.sendEmail(email, RESET_PASSWORD_SUBJECT, RESET_PASSWORD_BODY + FRONT_END_DOMAIN + RESET_PASSWORD_PATH + "?hash=" + token);
        } catch (Exception e) {
            System.out.println(PASSWORD_RESET_ERROR);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public boolean resetPassword(String token, String newPassword)
    {
        Connection con = getConnection();

        boolean res = true;

        int id = getUserIDFromPasswordReset(token);

        if(id == ID_NOT_FOUND)
        {
            closeConnection(con);
            return false;
        }

        try{
            if(!updatePassword(id, newPassword))
            {
                res = false;
            }
            else
            {
                PreparedStatement stmt = con.prepareStatement(DELETE_PASSWORD_RESET_QUERY);
                stmt.setInt(1, id);

                stmt.execute();
            }
        }
        catch(Exception e){
            System.out.println(CHANGE_PASSWORD_ERROR);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public int getUserIDFromPasswordReset(String token)
    {
        Connection con = getConnection();

        int res = ID_NOT_FOUND;

        try{
            PreparedStatement stmt = con.prepareStatement(GET_USER_ID_FROM_PASSWORD_RESET_QUERY);
            stmt.setString(1, token);

            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                res = rs.getInt(ID);
            }
            else
            {
                res = ID_NOT_FOUND;
            }
        }
        catch(Exception e){
            System.out.println(USER_NOT_FOUND);
        }

        closeConnection(con);

        return res;
    }

    public boolean updatePassword(int id, String newPassword){
        Connection con = getConnection();

        boolean res = true;

        try{
            PreparedStatement stmt = con.prepareStatement(CHANGE_PASSWORD_QUERY);
            stmt.setString(1, Utils.applySHA(newPassword));
            stmt.setInt(2, id);

            stmt.execute();
        }
        catch(Exception e){
            System.out.println(CHANGE_PASSWORD_ERROR);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public boolean shareIdea(int id, String email){
        Connection con = getConnection();

        boolean res = true;

        if(email == null)
        {
            closeConnection(con);
            return false;
        }

        String token = UUID.randomUUID().toString();

        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_SHARE_HASH_QUERY);
            stmt.setInt(1, id);
            stmt.setString(2, token);
            stmt.execute();

            SendGridMailService mailService = new SendGridMailService();

            mailService.sendEmail(email, SHARE_IDEA_SUBJECT, SHARE_IDEA_BODY + FRONT_END_DOMAIN + SHARE_IDEA_PATH + "?hash=" + token);
        } catch (Exception e) {
            System.out.println(IDEA_NOT_FOUND);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public int getIdFromShareHash(String hash)
    {
        Connection con = getConnection();

        int res = ID_NOT_FOUND;

        try{
            PreparedStatement stmt = con.prepareStatement(GET_IDEA_ID_FROM_SHARE_HASH_QUERY);
            stmt.setString(1, hash);

            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                res = rs.getInt(IDIDEA);
            }
            else
            {
                res = ID_NOT_FOUND;
            }
        }
        catch(Exception e){
            System.out.println(IDEA_NOT_FOUND);
            e.printStackTrace();
        }

        closeConnection(con);

        return res;
    }

    public boolean insertShareIdea(int id, String hash){
        Connection con = getConnection();

        boolean res = true;

        int idea = getIdFromShareHash(hash);

        try{
            PreparedStatement stmt = con.prepareStatement(INSERT_SHARE_IDEA_QUERY);
            stmt.setInt(1, idea);
            stmt.setInt(2, id);

            stmt.execute();

            PreparedStatement stmt2 = con.prepareStatement(DELETE_SHARE_HASH_QUERY);
            stmt2.setString(1, hash);

            stmt2.execute();
        }
        catch(Exception e){
            System.out.println(SHARE_IDEA_ERROR);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public boolean favoriteIdea(int userID, int ideaID) {
        Connection con = getConnection();

        boolean res = true;

        try{
            PreparedStatement stmt = con.prepareStatement(FAVORITE_IDEA_QUERY);
            stmt.setInt(1, ideaID);
            stmt.setInt(2, userID);
            stmt.execute();
        }
        catch(Exception e){
            System.out.println(FAVORITE_IDEA_ERROR);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public boolean unfavoriteIdea(int userID, int ideaID) {
        Connection con = getConnection();

        boolean res = true;

        try{
            PreparedStatement stmt = con.prepareStatement(UNFAVORITE_IDEA);
            stmt.setInt(1, ideaID);
            stmt.setInt(2, userID);
            stmt.execute();
        }
        catch(Exception e){
            System.out.println(UNFAVORITE_IDEA_ERROR);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public boolean isFavorited(int userID, int ideaID) {
        Connection con = getConnection();

        boolean res = false;

        try{
            PreparedStatement stmt = con.prepareStatement(GET_FAVORITE_IDEA);
            stmt.setInt(1, ideaID);
            stmt.setInt(2, userID);
            ResultSet rs = stmt.executeQuery();

            if(rs.next())
            {
                res = true;
            }
        }
        catch(Exception e){
            System.out.println(IDEA_NOT_FOUND);
            res = false;
        }

        closeConnection(con);

        return res;
    }

    public boolean userHasAccess(int userID, int ideaID)
    {
        Connection con = getConnection();

        try{
            PreparedStatement stmt = con.prepareStatement(GET_IDEA_QUERY);
            stmt.setInt(1, ideaID);
            ResultSet rs = stmt.executeQuery();

            if(rs.next() && rs.getInt(ID_AUTHOR) == userID)
            {
                return true;
            }

            PreparedStatement stmt2 = con.prepareStatement(GET_USER_SHARED_IDEAS);
            stmt2.setInt(1, userID);
            ResultSet rs2 = stmt2.executeQuery();

            while(rs2.next())
            {
                if(rs2.getInt(ID) == ideaID)
                {
                    return true;
                }
            }

            PreparedStatement stmt3 = con.prepareStatement(GET_USER_TEAMS);
            stmt3.setInt(1, userID);
            ResultSet rs3 = stmt3.executeQuery();

            while(rs3.next())
            {
                PreparedStatement stmt4 = con.prepareStatement(JOIN_TEAM_TEAM_IDEA);
                stmt4.setInt(1, rs3.getInt(ID));
                ResultSet rs4 = stmt4.executeQuery();

                while(rs4.next()) {
                    if(rs4.getInt(IDIDEA) == ideaID)
                    {
                        return true;
                    }
                }
            }
        }
        catch(Exception e){
            System.out.println(IDEA_NOT_FOUND);
        }

        closeConnection(con);

        return false;
    }

    public void close()
    {
        try
        {
            cpds.close();
        }
        catch(Exception e)
        {
            System.out.println(CONNECTION_CLOSURE_FAILED);
        }
    }

    public Connection getConnection()
    {
        try
        {
            return cpds.getConnection();
        }
        catch(Exception e)
        {
            System.out.print(e.getMessage());
        }

        return null;
    }

    public boolean closeConnection(Connection con)
    {
        if(con == null)
        {
            return false;
        }

        try
        {
            con.close();
            return true;
        }
        catch(Exception e)
        {
            System.out.print(e.getMessage());
        }

        return false;
    }
}
