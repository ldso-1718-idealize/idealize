package services;

import com.sharethis.textrank.Graph;
import com.sharethis.textrank.MetricVector;
import com.sharethis.textrank.Node;
import com.sharethis.textrank.TextRank;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static utils.Configs.LOG_PROPERTIES;
import static utils.Configs.RES_PATH;

public class TextRankService {

    private static String text;
    private static String result;

    private static final String LANG_CODE = "en";

    private static final String EXEC_ERROR = "exec exception";
    private static final String INTERRUPT_ERROR = "interrupt";
    private static final String TIMEOUT_ERROR = "timeout";

    private Map<String, Double> res = new HashMap<String, Double>();

    private final static Log LOG =
            LogFactory.getLog(TextRank.class.getName());

    public TextRankService(String input) throws Exception{

        PropertyConfigurator.configure(RES_PATH+LOG_PROPERTIES);

        text = input;

        // filter out overly large files

        boolean use_wordnet = true;

        // main entry point for the algorithm

        final TextRank tr = new TextRank(RES_PATH, LANG_CODE);
        tr.prepCall(text, use_wordnet);

        // wrap the call in a timed task

        final FutureTask<Collection<MetricVector>> task = new FutureTask<Collection<MetricVector>>(tr);
        Collection<MetricVector> answer = null;

        final Thread thread = new Thread(task);
        thread.run();

        try {
            answer = task.get(15000L, TimeUnit.MILLISECONDS); // timeout in N ms
            System.out.println(answer.size());
            Iterator<MetricVector> it = answer.iterator();

            while(it.hasNext())
            {
                MetricVector mt = it.next();
                res.put(mt.value.text, new Double(mt.count_rank + mt.link_rank + mt.metric));
                /*System.out.println(mt.value.text);
                System.out.println(mt.count_rank);
                System.out.println(mt.link_rank);
                System.out.println(mt.metric);
                System.out.println(mt.synset_rank);*/
            }
        }
        catch (ExecutionException e) {
            LOG.error(EXEC_ERROR, e);
        }
        catch (InterruptedException e) {
            LOG.error(INTERRUPT_ERROR, e);
        }
        catch (TimeoutException e) {
            LOG.error(TIMEOUT_ERROR, e);
            return;
        }

        /*Set<Map.Entry<String, Node>> nodes = tr.getGraph().entrySet();

        Iterator<Map.Entry<String, Node>> it2 = nodes.iterator();

        while(it2.hasNext())
        {
            Map.Entry<String, Node> entry = it2.next();
            System.out.println(entry.getValue().value.text);
            System.out.println(entry.getValue().rank);
        }*/

        result = tr.toString();
    }

    public String getResult(){
        return result;
    }

    public double getWeight(String key)
    {
        if(res.containsKey(key))
        {
            return res.get(key);
        }

        return -1;
    }

    public Map<String, Double> getMap()
    {
        return res;
    }

}
