package services;

import com.sendgrid.*;

import java.io.IOException;

import static utils.Configs.IDEALIZE_EMAIL;
import static utils.Configs.SEND_GRID_API_KEY;
import static utils.Utils.EMAIL_SEND;
import static utils.Utils.EMAIL_SEND_ERROR;
import static utils.Utils.TEXT_PLAIN;

public class SendGridMailService implements MailService {

    private SendGrid sendGrid;

    public SendGridMailService()
    {
        sendGrid = new SendGrid(SEND_GRID_API_KEY);
    }

    public boolean sendEmail(String address, String subj, String body)
    {
        boolean res = false;
        Email from = new Email(IDEALIZE_EMAIL);
        String subject = subj;
        Email to = new Email(address);
        Content content = new Content(TEXT_PLAIN, body);
        Mail mail = new Mail(from, subject, to, content);

        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint(EMAIL_SEND);
            request.setBody(mail.build());
            Response response = sendGrid.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
            res = true;
        } catch (IOException ex) {
            System.out.println(EMAIL_SEND_ERROR);
        }

        return res;
    }
}
