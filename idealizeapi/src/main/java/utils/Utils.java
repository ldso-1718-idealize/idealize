package utils;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.net.ssl.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.cert.X509Certificate;
import java.util.*;

import static utils.Configs.INDEXES_URL;
import static utils.Configs.TRENDS_URL;

public class Utils
{
    //Routes
    public static final String HOME_PATH = "/";
    public static final String REGISTER_PATH = "/register";
    public static final String LOGIN_PATH = "/login";
    public static final String CHECK_SESSION_PATH = "/getSession";
    public static final String LOGOUT_PATH = "/logout";
    public static final String DELETE_ACCOUNT_PATH = "/deleteAccount";
    public static final String GET_STATS_PATH = "/getStats";
    public static final String GET_PROFILE_INFO_PATH = "/getProfileInfo";
    public static final String IDEA_PATH = "/idea";
    public static final String EDIT_PROFILE_PATH = "/editProfile";
    public static final String TEAM_PATH = "/team";
    public static final String TEAM_INVITE_PATH = "/teamInvite";
    public static final String JOIN_TEAM_PATH = "/joinTeam";
    public static final String REFUSE_INVITE_TEAM_PATH = "/refuseTeamInvite";
    public static final String ADD_TEAM_IDEA_PATH = "/team/addIdea";
    public static final String GET_TEAMS_PATH = "/teams";
    public static final String ACTIVATION_PATH = "/activate";
    public static final String RESET_PASSWORD_CHECK_PATH = "/checkReset";
    public static final String RESET_PASSWORD_PATH = "/reset";
    public static final String UPDATE_PASSWORD_PATH = "/updatePassword";
    public static final String REANALYZE_PATH = "/reanalyze";
    public static final String SHARE_IDEA_PATH = "/share";
    public static final String CHECK_SHARE_PATH = "/checkShare";
    public static final String ADD_SHARED_IDEA_PATH = "/addSharedIdea";
    public static final String REMOVED_SHARED_IDEA_PATH = "/removeSharedIdea";
    public static final String REMOVED_TEAM_MEMBER_PATH = "/removeMember";
    public static final String FAVORITE_IDEA_PATH = "/favoriteIdea";
    public static final String DELETE_IDEA_PATH = "/deleteIdea";

    public static final String SHA256 = "SHA-256";
    public static final String FORMAT = "%040x";
    public static final String UTF8 = "UTF-8";

    public static final String POST = "POST";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String ACCEPT = "Accept";
    public static final String APPLICATION_JSON = "application/json";
    public static final int TIMEOUT = 30000;

    public static final String EMAIL_SEND = "mail/send";
    public static final String TEXT_PLAIN = "text/plain";

    //Words
    public static final String NAME = "name";
    public static final String USER = "user";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String EMAIL = "email";
    public static final String ACTIVE = "active";
    public static final String TEAMNAME = "teamname";
    public static final String IDTEAM = "idteam";
    public static final String TEAMID = "teamid";
    public static final String FOUNDERID = "idfounder";
    public static final String MEMBERID = "idmember";
    public static final String ADMIN = "admin";
    public static final String MEMBERS = "members";
    public static final String MEMBER = "member";
    public static final String STATUS = "status";
    public static final String ACCEPTED = "accepted";
    public static final String PENDING = "pending";
    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String IDAUTHOR = "idauthor";
    public static final String SYNOPSIS = "synopsis";
    public static final String KEYWORD = "keyword";
    public static final String ORIGINAL = "original";
    public static final String KEYWORDS = "keywords";
    public static final String TRENDS = "trends";
    public static final String FAVORITE = "favorite";
    public static final String CREATION_DATE = "creationdate";
    public static final String ID_AUTHOR = "idauthor";
    public static final String AUTHOR = "author";
    public static final String IDEAS = "ideas";
    public static final String IDEAID = "ideaid";
    public static final String USERID = "userid";
    public static final String TEXTRANKQUERY = "text_rank";
    public static final String DATA = "data";
    public static final String NKEYS = "nkeys";
    public static final String KEYS = "keys";
    public static final String KEYSWEIGHT = "keysweight";
    public static final String GOOGLE = "google";
    public static final String NTREND = "ntrend";
    public static final String Y = "y";
    public static final String TREND = "trend";
    public static final String AVGCOMP = "avgcomp";
    public static final String NA = "NA";
    public static final String INDEXES = "indexes";
    public static final String HASH = "hash";
    public static final String ACTIVATE = "activate";
    public static final String COUNTRY = "country";
    public static final String TRANSLATION = "translation";
    public static final String SHARE_HASH = "share_hash";
    public static final String IDIDEA = "ididea";
    public static final String EMPTY_STRING = "";

    public static final String IDEALIZE_SESSION = "IDEALIZE_SESSION";
    public static final int SESSION_EXPIRE = 18000;
    public static final int SESSION_DESTROY = 0;

    public static final String HOME_DIR_PATH = ".";
    public static final String CYPHER_SUITS_1 = "TLS_DHE_RSA.*";
    public static final String CYPHER_SUITS_2 = "TLS_ECDHE.*";
    public static final String CYPHER_SUITS_3 = "TLS_RSA_WITH_AES_256_CBC_SHA";

    //Error messages
    public static final String CONNECTION_FAILED = "Database connection failed! Please specify valid database connection configurations in the properties file";
    public static final String CONNECTION_CLOSURE_FAILED = "Database connection closure failed!";
    public static final String REGISTER_FAILED = "Failed to register new account!";
    public static final String AUTHENTICATION_FAILED = "Username/password is incorrect";
    public static final String SESSION_CREATION_FAILED = "Session could not be created";
    public static final String SESSION_NOT_FOUND = "Session not found";
    public static final String DELETE_ACCOUNT_ERROR = "Could not delete account!";
    public static final String USER_NOT_FOUND = "Could not get profile info!";
    public static final String SAVE_IDEA_FAILED = "Could not save the idea!";
    public static final String IDEA_NOT_FOUND = "The requested idea does not exists!";
    public static final String IDEA_KEYWORD_ERROR = "Could not create idea keyword!";
    public static final String UPDATE_PROFILE_ERROR = "Could not update profile!";
    public static final String CHANGE_PASSWORD_ERROR = "Could not update password!";
    public static final String CREATE_TEAM_FAILED = "Could not create the team!";
    public static final String TEAM_NOT_FOUND = "Team not found!";
    public static final String CREATE_TEAM_INVITE_FAILED = "Could not create the invite!";
    public static final String INVITE_NOT_FOUND = "Invite not found!";
    public static final String ACCEPT_TEAM_INVITE_FAILED = "Failed accepting invite!";
    public static final String DECLINE_TEAM_INVITE_FAILED = "Failed declining invite!";
    public static final String DELETE_TEAM_INVITE_FAILED = "Failed deleting invite!";
    public static final String CREATE_TEAM_MEMBER_FAILED = "Could not create the team member!";
    public static final String CREATE_TEAM_IDEA_FAILED = "Could not create the team idea!";
    public static final String REMOVE_TEAM_IDEA_FAILED = "Could not remove the team idea!";
    public static final String REMOVE_TEAM_MEMBER_FAILED = "Could not remove the team member!";

    public static final String ACCOUNT_NOT_ACTIVATED  = "Account is not activated!";
    public static final String ACTIVATION_FAILED = "Account activation failed!";
    public static final String PASSWORD_RESET_ERROR = "Could not reset password!";
    public static final String SHARE_IDEA_ERROR = "Could not share idea!";
    public static final String FAVORITE_IDEA_ERROR = "Could not favorite idea!";
    public static final String UNFAVORITE_IDEA_ERROR = "Could not unfavorite idea!";
    public static final int ID_NOT_FOUND = -1;

    public static final String EMAIL_SEND_ERROR = "Could not send email";
    public static final String ACTIVATION_SUBJECT = "Idealize Account Activation";
    public static final String ACTIVATION_BODY = "Your account for Idealize needs to be activated. To do so click the following link:\n\n";
    public static final String RESET_PASSWORD_SUBJECT = "Idealize Password Reset";
    public static final String RESET_PASSWORD_BODY = "You requested to reset your password. To do so click the following link:\n\n";
    public static final String SHARE_IDEA_SUBJECT = "Idealize Idea Share";
    public static final String SHARE_IDEA_BODY = "An idea was shared with you. To access it click the following link:\n\n";

    public static String applySHA(String string)
    {
        try
        {
            MessageDigest digest = MessageDigest.getInstance(SHA256);
            byte[] hash = digest.digest(string.getBytes(StandardCharsets.UTF_8));
            String fileId2 = new String(hash, StandardCharsets.UTF_8);
            return String.format(FORMAT, new BigInteger(1, fileId2.getBytes(/*YOUR_CHARSET?*/)));
        }
        catch(Exception e)
        {
            System.out.println("Failed to apply SHA256");
        }

        return null;
    }

    public static String getTrends(Set<String> query, String country, String translate)
    {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
        };

        // Install the all-trusting trust manager
        try
        {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        }
        catch(Exception e)
        {
            return null;
        }

        String result = null;

        String https_url = TRENDS_URL;
        String url_query = buildQuery(query);
        String parameters = COUNTRY + "=" + country + "&" + TRANSLATION + "=" + translate;

        System.out.println(https_url + url_query);
        URL url;
        try {

            url = new URL(https_url + url_query);
            HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
            con.setRequestMethod(POST);
            con.setDoOutput(true);
            con.setConnectTimeout(TIMEOUT);
            con.setReadTimeout(TIMEOUT);
            con.connect();

            System.out.println(parameters);

            OutputStream os = con.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(parameters);
            osw.flush();
            osw.close();

            result = get_content(con);

        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return result;
    }

    public static String getIndexes(String trends, Map<String, Double> map)
    {
        String result = null;

        String http_url = INDEXES_URL;

        String query;

        try
        {
            query = getIndexesQuery(trends, map);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }

        String id = new JSONArray(query).getJSONObject(0).getString(ID);

        URL url;

        try {

            url = new URL(http_url);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();

            con.setRequestMethod(POST);
            con.setDoOutput(true);
            con.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON);
            con.setRequestProperty(ACCEPT, APPLICATION_JSON);
            con.setConnectTimeout(TIMEOUT);
            con.setReadTimeout(TIMEOUT);
            con.connect();

            OutputStream os = con.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(query);
            osw.flush();
            osw.close();

            result = get_content(con);

        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return result;
    }

    private static String get_content(HttpsURLConnection con){

        String result = null;

        if(con!=null){

            try {

                InputStream inputStream = con.getInputStream();

                Scanner s = new Scanner(inputStream).useDelimiter("\\A");
                result = s.hasNext() ? s.next() : "";

            } catch (IOException e) {
                e.getMessage();
                return null;
            }
        }

        return result;
    }

    private static String get_content(HttpURLConnection con){

        String result = null;

        if(con!=null){

            try {

                InputStream inputStream = con.getInputStream();

                Scanner s = new Scanner(inputStream).useDelimiter("\\A");
                result = s.hasNext() ? s.next() : "";

            } catch (IOException e) {
                e.getMessage();
                return null;
            }
        }

        return result;
    }

    public static String buildQuery(Set<String> strings)
    {
        String res = "";

        Iterator<String> it = strings.iterator();

        while(it.hasNext())
        {
            String string = it.next().replaceAll("[ \\t\\\\x0B\\f\\r]+", "%20");

            if(it.hasNext())
            {
                res += string + ",";
            }
            else
            {
                res += string;
            }
        }

        return res;
    }

    public static String getIndexesQuery(String trends, Map<String, Double> map)
    {
        JSONArray arr = new JSONArray(trends);

        JSONArray res = new JSONArray();

        JSONObject obj = new JSONObject();
        obj.put(ID, UUID.randomUUID().toString());

        JSONArray data = new JSONArray();
        JSONObject data1 = new JSONObject();

        data1.put(NKEYS, arr.length());

        JSONArray keys = new JSONArray();
        JSONArray keysweight = new JSONArray();

        JSONArray google = new JSONArray();

        for(int i = 0; i < arr.length(); i++)
        {
            JSONObject cur = arr.getJSONObject(i);

            keys.put(cur.getString(KEYWORD));

            if(cur.has(ORIGINAL))
            {
                keysweight.put(map.get(cur.getString(ORIGINAL)).doubleValue());
            }
            else
            {
                keysweight.put(map.get(cur.getString(KEYWORD)).doubleValue());
            }

            JSONArray data2 = cur.getJSONArray(DATA).getJSONArray(0);

            JSONObject newObj = new JSONObject();
            newObj.put(NTREND, data2.length());

            JSONArray trend = new JSONArray();

            for(int j = 0; j < data2.length(); j++)
            {
                JSONObject cur2 = data2.getJSONObject(j);
                trend.put(cur2.get(Y));
            }

            newObj.put(TREND, trend);
            newObj.put(AVGCOMP, NA);

            google.put(newObj);
        }

        data1.put(KEYS, keys);
        data1.put(KEYSWEIGHT, keysweight);

        data.put(data1);

        obj.put(DATA, data);
        obj.put(GOOGLE, google);

        res.put(obj);

        return res.toString();
    }

    public static String getCompactTrends(String trends)
    {
        JSONArray arr = new JSONArray(trends);

        for(int i = 0; i < arr.length(); i++)
        {
            if(arr.getJSONObject(i).getJSONArray(DATA).getJSONArray(0).length() == 0)
            {
                arr.remove(i);
                i--;
            }
        }

        if(arr.length() == 0)
        {
            return null;
        }

        return arr.toString();
    }
}
