package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Furyst on 10/25/2017.
 */
public class Configs
{
    public static String CONFIG_PATH;

    public static String DB_HOST;
    public static String DB_PORT;
    public static String DB_NAME;
    public static String DB_USERNAME;
    public static String DB_PASSWORD;

    public static String KEYSTORE_PATH;
    public static String KEYSTORE_PASSWORD;
    public static String KEY_MANAGER_PASSWORD;
    public static String TRUSTSTORE_PASSWORD;
    public static String TRUSTSTORE_PATH;

    public static String RES_PATH;
    public static String LOG_PROPERTIES;
    public static final String CUCUMBER_FEATURES_PATH = "src/test/java/tests/resources/";

    public static String ALLOWED_ORIGINS = "http://projectidealize.me,https://projectidealize.me";
    public static String IDEALIZE_DOMAIN = "projectidealize.me";
    public static String TRENDS_URL = "https://projectidealize.me:3001/trendgraphs/";
    public static String INDEXES_URL = "http://idealize.dtdns.net:10101";
    public static String IDEALIZE_EMAIL = "idealize@projectidealize.me";
    public static String FRONT_END_DOMAIN = "https://projectidealize.me";

    public static String SEND_GRID_API_KEY = "";

    public static void readConfig(String path)
    {
        CONFIG_PATH = path;

        Properties prop = new Properties();
        InputStream input = null;

        try {

            input = new FileInputStream(CONFIG_PATH);

            prop.load(input);

            setConsts(prop);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void setConsts(Properties prop)
    {
        DB_HOST = prop.getProperty("dbaddr");
        DB_PORT = prop.getProperty("dbport");
        DB_NAME = prop.getProperty("dbname");
        DB_USERNAME = prop.getProperty("dbuser");
        DB_PASSWORD = prop.getProperty("dbpass");

        KEYSTORE_PATH = prop.getProperty("keystore_path");
        KEYSTORE_PASSWORD = prop.getProperty("keystore_password");
        KEY_MANAGER_PASSWORD = prop.getProperty("key_manager_password");
        TRUSTSTORE_PATH = prop.getProperty("truststore_path");
        TRUSTSTORE_PASSWORD = prop.getProperty("truststore_password");

        RES_PATH = prop.getProperty("res_path");
        LOG_PROPERTIES = prop.getProperty("log_properties");

        ALLOWED_ORIGINS = prop.getProperty("allowed_origins");
        IDEALIZE_DOMAIN = prop.getProperty("idealize_domain");
        TRENDS_URL = prop.getProperty("trends_url");
        INDEXES_URL = prop.getProperty("indexes_url");
        FRONT_END_DOMAIN = prop.getProperty("front_end_domain");

        IDEALIZE_EMAIL = prop.getProperty("idealize_email");

        SEND_GRID_API_KEY = prop.getProperty("send_grid_api_key");
    }
}
