package handlers.users;

import services.MySqlBasedUserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static utils.Configs.IDEALIZE_DOMAIN;
import static utils.Utils.*;

public class LoginHandler extends HttpServlet
{
    MySqlBasedUserService service = new MySqlBasedUserService();

    public LoginHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if(request.getParameterMap().containsKey(USERNAME) && request.getParameterMap().containsKey(PASSWORD))
        {
            String username = request.getParameter(USERNAME);
            String password = request.getParameter(PASSWORD);

            String token = service.login(username, password);

            if(token != AUTHENTICATION_FAILED && token != ACCOUNT_NOT_ACTIVATED)
            {
                Cookie cookie = new Cookie(IDEALIZE_SESSION, token);

                cookie.setSecure(true);
                cookie.setDomain(IDEALIZE_DOMAIN);
                cookie.setPath(HOME_PATH);
                cookie.setMaxAge(SESSION_EXPIRE);
                cookie.setHttpOnly(true);
                response.addCookie(cookie);

                PrintWriter out = response.getWriter();

                out.println(username);

                response.setStatus(HttpServletResponse.SC_OK);
            }
            else
            {
                PrintWriter out = response.getWriter();

                out.println(token);

                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
        else
        {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
