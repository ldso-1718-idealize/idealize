package handlers.users;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import services.MySqlBasedUserService;

import static utils.Utils.*;

public class DeleteAccountHandler extends HttpServlet
{
    MySqlBasedUserService service = new MySqlBasedUserService();

    public DeleteAccountHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if(request.getParameterMap().containsKey(USERID))
        {
            try{
                int userID = Integer.parseInt(request.getParameter(USERID));

                boolean res = service.deleteAccount(userID);

                if(!res){
                    System.out.println("Error deleting user in the database");
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    return;
                }

                response.setStatus(HttpServletResponse.SC_OK);

            }catch (NumberFormatException e){
                System.out.println("ID must be integer");
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
        }
        else
        {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }

    }

}