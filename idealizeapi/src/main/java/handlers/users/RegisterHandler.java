package handlers.users;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import services.MySqlBasedUserService;

import static utils.Utils.*;

public class RegisterHandler extends HttpServlet
{
    private MySqlBasedUserService service = new MySqlBasedUserService();

    public RegisterHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if(request.getParameterMap().containsKey(USERNAME) && request.getParameterMap().containsKey(PASSWORD) && request.getParameterMap().containsKey(FIRSTNAME)
        && request.getParameterMap().containsKey(LASTNAME) && request.getParameterMap().containsKey(EMAIL))
        {
            String username = request.getParameter(USERNAME);
            String password = request.getParameter(PASSWORD);
            String firstname = request.getParameter(FIRSTNAME);
            String lastname = request.getParameter(LASTNAME);
            String email = request.getParameter(EMAIL);

            boolean res;

            if(request.getParameterMap().containsKey(ACTIVATE) && !Boolean.parseBoolean(request.getParameter(ACTIVATE)))
            {
                res = service.register(username, password, firstname, lastname, email);
            }
            else
            {
                res = service.register(username, password, firstname, lastname, email, true);
            }

            int userID = service.getUserID(username);

            if(res)
            {
                response.setIntHeader("id", userID);
                response.setStatus(HttpServletResponse.SC_OK);
            }
            else
            {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
        else
        {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        PrintWriter out = response.getWriter();

        System.out.println("asd");

        out.println("{}");
    }
}
