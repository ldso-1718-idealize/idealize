package handlers.users;

import services.MySqlBasedUserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static utils.Utils.*;

public class RefuseTeamInviteHandler extends HttpServlet
{
    MySqlBasedUserService service = new MySqlBasedUserService();

    public RefuseTeamInviteHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cookie[] cookies = request.getCookies();

        if(cookies != null) {

            for (Cookie cookie : cookies) {

                if (cookie.getName().equals(IDEALIZE_SESSION)) {

                    String token = cookie.getValue();

                    String username = service.getUsernameFromSession(token);

                    if (!username.equals(SESSION_NOT_FOUND)) {

                        if(!request.getParameterMap().containsKey(ID)){
                            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                            return;
                        }

                        int teamID = Integer.parseInt(request.getParameter(ID));

                        if(teamID < 0){
                            System.out.println("Requested team not found");
                            return;
                        }

                        int userID = service.getUserID(username);

                        //Accept invite
                        boolean res = service.declineTeamInvite(teamID, userID);

                        if(!res){
                            System.out.println("Could not accept the invite");
                            return;
                        }

                        response.setStatus(HttpServletResponse.SC_OK);

                    }
                    else response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

                }
                else response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

            }

        }
        else{
            System.out.println("Invalid user");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

    }

}
