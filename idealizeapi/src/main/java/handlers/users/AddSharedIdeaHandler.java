package handlers.users;

import services.MySqlBasedUserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static utils.Utils.*;

public class AddSharedIdeaHandler extends HttpServlet
{
    MySqlBasedUserService service = new MySqlBasedUserService();

    public AddSharedIdeaHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if(request.getParameterMap().containsKey(USERNAME) && request.getParameterMap().containsKey(HASH))
        {
            String username = request.getParameter(USERNAME);
            String hash = request.getParameter(HASH);

            int userID = service.getUserID(username);

            boolean res = service.insertShareIdea(userID, hash);

            if(res)
            {
                response.setStatus(HttpServletResponse.SC_OK);
            }
            else
            {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
        else
        {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
