package handlers.users;

import services.MySqlBasedUserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static utils.Utils.*;

public class RemoveTeamIdeaHandler extends HttpServlet
{
    MySqlBasedUserService service = new MySqlBasedUserService();

    public RemoveTeamIdeaHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cookie[] cookies = request.getCookies();

        if(cookies != null) {

            for (Cookie cookie : cookies) {

                if (cookie.getName().equals(IDEALIZE_SESSION)) {

                    String token = cookie.getValue();

                    String username = service.getUsernameFromSession(token);

                    if (!username.equals(SESSION_NOT_FOUND)) {

                        if(request.getParameterMap().containsKey(TEAMID) && request.getParameterMap().containsKey(IDEAID))
                        {

                            try {

                                int teamID = Integer.parseInt(request.getParameter(TEAMID));
                                int ideaID = Integer.parseInt(request.getParameter(IDEAID));

                                boolean teamIdea = service.removeTeamIdea(teamID, ideaID);

                                if(!teamIdea){
                                    System.out.println("Failed removing idea from team");
                                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                    return;
                                }

                                response.setStatus(HttpServletResponse.SC_OK);

                            } catch(NumberFormatException e) {

                                System.out.println("Id must be integer");
                                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                return;

                            }

                        }

                        else response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

                    }
                    else response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

                }
                else response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

            }

        }
        else{
            System.out.println("Invalid user");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

    }

}

