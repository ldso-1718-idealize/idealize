package handlers.users;

import services.MySqlBasedUserService;
import static utils.Utils.*;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EditProfileHandler extends HttpServlet {

    MySqlBasedUserService service = new MySqlBasedUserService();

    public EditProfileHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        Cookie[] cookies = request.getCookies();

        if(cookies != null)
        {

            for(Cookie cookie : cookies)
            {
                if(cookie.getName().equals(IDEALIZE_SESSION))
                {
                    String token = cookie.getValue();

                    String username = service.getUsernameFromSession(token);

                    if(!username.equals(SESSION_NOT_FOUND))
                    {
                        if(request.getParameterMap().containsKey(FIRSTNAME) && request.getParameterMap().containsKey(LASTNAME) &&
                                request.getParameterMap().containsKey(EMAIL))
                        {

                            int userID = service.getUserID(username);

                            String firstname = request.getParameter(FIRSTNAME);
                            String lastname = request.getParameter(LASTNAME);
                            String email = request.getParameter(EMAIL);

                            if(firstname.isEmpty())
                                firstname = service.getUserFirstname(userID);

                            if(lastname.isEmpty())
                                lastname = service.getUserLastname(userID);

                            if(email.isEmpty())
                                email = service.getUserEmail(userID);

                            boolean updated = service.editProfile(userID, firstname, lastname, email);

                            if(updated)
                                response.setStatus(HttpServletResponse.SC_OK);
                            else response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

                        }
                        else
                        {
                            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        }
                    }
                    else
                    {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }

                    return;
                }
            }
        }

        System.out.println("Invalid user");

    }

}
