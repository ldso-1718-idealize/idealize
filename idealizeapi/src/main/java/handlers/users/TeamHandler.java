package handlers.users;

import org.json.JSONArray;
import org.json.JSONObject;
import services.MySqlBasedUserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static utils.Utils.*;

public class TeamHandler extends HttpServlet
{
    MySqlBasedUserService service = new MySqlBasedUserService();

    public TeamHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cookie[] cookies = request.getCookies();

        if(cookies != null) {

            for (Cookie cookie : cookies) {

                if (cookie.getName().equals(IDEALIZE_SESSION)) {

                    String token = cookie.getValue();

                    String username = service.getUsernameFromSession(token);

                    if (!username.equals(SESSION_NOT_FOUND)) {

                        if(!request.getParameterMap().containsKey(NAME)){
                            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                            return;
                        }

                        int userID = service.getUserID(username);
                        String teamName = request.getParameter(NAME);

                        int teamID = service.createTeam(userID, teamName);

                        service.createTeamMember(teamID, userID);

                        for (int i=1; i<request.getParameterMap().size(); i++) {
                            String userN = request.getParameter(USER + i);
                            int uID = service.getUserID(userN);

                            service.teamInvite(teamID, uID);
                        }


                        //collect information to retrieve
                        JSONObject obj = new JSONObject();

                        //basic info
                        obj.put(ID, teamID);
                        obj.put(NAME, teamName);
                        obj.put(FOUNDERID, userID);
                        obj.put(ADMIN, true);

                        //members
                        JSONArray arr = new JSONArray();

                        JSONObject obj2 = new JSONObject();
                        obj2.put(NAME, username);
                        obj2.put(ID, userID);

                        arr.put(obj2);

                        obj.put(MEMBERS, arr);

                        //ideas
                        JSONArray arr2 = new JSONArray();
                        obj.put(IDEAS, arr2);


                        PrintWriter out = response.getWriter();

                        out.println(obj.toString());

                        response.setStatus(HttpServletResponse.SC_OK);

                    }
                    else
                    {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }

                }
                else
                {
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                }

            }

        }
        else{
            System.out.println("Invalid user");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

    }

}
