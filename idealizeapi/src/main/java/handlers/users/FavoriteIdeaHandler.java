package handlers.users;

import services.MySqlBasedUserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static utils.Utils.*;

public class FavoriteIdeaHandler extends HttpServlet
{
    MySqlBasedUserService service = new MySqlBasedUserService();

    public FavoriteIdeaHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cookie[] cookies = request.getCookies();

        if(cookies != null) {

            for (Cookie cookie : cookies) {

                if (cookie.getName().equals(IDEALIZE_SESSION)) {

                    String token = cookie.getValue();

                    String username = service.getUsernameFromSession(token);

                    if (!username.equals(SESSION_NOT_FOUND)) {

                        if(request.getParameterMap().containsKey(IDEAID))
                        {

                            try {

                                int userID = service.getUserID(username);
                                int ideaID = Integer.parseInt(request.getParameter(IDEAID));

                                boolean isFavorited = service.isFavorited(userID, ideaID);

                                boolean res;

                                if(isFavorited)
                                {
                                    res = service.unfavoriteIdea(userID, ideaID);
                                }
                                else
                                {
                                    res = service.favoriteIdea(userID, ideaID);
                                }

                                if(!res){
                                    System.out.println("Could not favorite/unfavorite idea");
                                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                    return;
                                }

                                response.setStatus(HttpServletResponse.SC_OK);

                            } catch(NumberFormatException e) {

                                System.out.println("Id must be integer");
                                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                return;

                            }

                        }

                        else response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

                    }
                    else response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

                }
                else response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

            }

        }
        else{
            System.out.println("Invalid user");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

    }

}

