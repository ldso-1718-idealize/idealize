package handlers.users;

import services.MySqlBasedUserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static utils.Configs.IDEALIZE_DOMAIN;
import static utils.Utils.*;

public class LogoutHandler extends HttpServlet
{
    MySqlBasedUserService service = new MySqlBasedUserService();

    public LogoutHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(IDEALIZE_SESSION)) {
                    String token = cookie.getValue();

                    boolean res = service.logout(token);

                    System.out.println(token);

                    if (res) {
                        Cookie newCookie = new Cookie(IDEALIZE_SESSION, token);

                        newCookie.setSecure(true);
                        newCookie.setDomain(IDEALIZE_DOMAIN);
                        newCookie.setPath(HOME_PATH);
                        newCookie.setMaxAge(SESSION_DESTROY);
                        newCookie.setHttpOnly(true);
                        response.addCookie(newCookie);

                        response.setStatus(HttpServletResponse.SC_OK);
                    } else {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }

                    return;
                }
            }
        }
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }
}
