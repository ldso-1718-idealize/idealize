package handlers.users;

import services.MySqlBasedUserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static utils.Utils.*;

public class GetTeamsHandler extends HttpServlet
{
    MySqlBasedUserService service = new MySqlBasedUserService();

    public GetTeamsHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Cookie[] cookies = request.getCookies();

        if(cookies != null) {

            for (Cookie cookie : cookies) {

                if (cookie.getName().equals(IDEALIZE_SESSION)) {

                    String token = cookie.getValue();

                    String username = service.getUsernameFromSession(token);

                    if (!username.equals(SESSION_NOT_FOUND)) {

                        int userID = service.getUserID(username);

                        String userTeams = service.getUserTeams(userID);

                        PrintWriter out = response.getWriter();

                        out.println(userTeams);

                        response.setStatus(HttpServletResponse.SC_OK);

                    }
                    else response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

                }
                else response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

            }

        }
        else{
            System.out.println("Invalid user");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
