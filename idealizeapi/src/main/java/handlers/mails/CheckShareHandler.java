package handlers.mails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import services.MySqlBasedUserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import static utils.Utils.*;

public class CheckShareHandler extends HttpServlet
{
    MySqlBasedUserService service = new MySqlBasedUserService();

    public CheckShareHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(IDEALIZE_SESSION)) {
                    String token = cookie.getValue();

                    String username = service.getUsernameFromSession(token);

                    if (!username.equals(SESSION_NOT_FOUND)) {
                        if (request.getParameterMap().containsKey(HASH)) {
                            try {
                                String hash = request.getParameter(HASH);

                                int id = service.getUserID(username);

                                boolean res = service.insertShareIdea(id, hash);

                                if (res) {

                                    response.setStatus(HttpServletResponse.SC_ACCEPTED);
                                } else response.setStatus(HttpServletResponse.SC_NOT_FOUND);

                            } catch (NumberFormatException e) {
                                System.out.println("Idea id must be an integer");
                                e.printStackTrace();
                                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                            }
                        } else {
                            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        }

                        return;
                    }
                }
            }

            if (request.getParameterMap().containsKey(HASH)) {
                try {
                    String hash = request.getParameter(HASH);

                    int res = service.getIdFromShareHash(hash);

                    if (res != ID_NOT_FOUND) {

                        response.setStatus(HttpServletResponse.SC_OK);
                    } else response.setStatus(HttpServletResponse.SC_NOT_FOUND);

                } catch (NumberFormatException e) {
                    System.out.println("Idea id must be an integer");
                    e.printStackTrace();
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                }
            } else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        } else if (request.getParameterMap().containsKey(HASH)) {
            try {
                String hash = request.getParameter(HASH);

                int res = service.getIdFromShareHash(hash);

                if (res != ID_NOT_FOUND) {

                    response.setStatus(HttpServletResponse.SC_OK);
                } else response.setStatus(HttpServletResponse.SC_NOT_FOUND);

            } catch (NumberFormatException e) {
                System.out.println("Idea id must be an integer");
                e.printStackTrace();
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

        System.out.println("Invalid user");
    }
}
