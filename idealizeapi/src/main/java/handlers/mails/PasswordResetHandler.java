package handlers.mails;

import services.MySqlBasedUserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static utils.Utils.EMAIL;
import static utils.Utils.HASH;
import static utils.Utils.USERNAME;

public class PasswordResetHandler extends HttpServlet
{
    MySqlBasedUserService service = new MySqlBasedUserService();

    public PasswordResetHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameterMap().containsKey(USERNAME)) {
            String username = request.getParameter(USERNAME);

            boolean res = service.requestPasswordReset(username);

            if (res) {
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
