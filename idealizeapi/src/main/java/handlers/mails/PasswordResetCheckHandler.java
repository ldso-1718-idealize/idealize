package handlers.mails;

import services.MySqlBasedUserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static utils.Utils.HASH;
import static utils.Utils.ID_NOT_FOUND;
import static utils.Utils.USERNAME;

public class PasswordResetCheckHandler extends HttpServlet
{
    MySqlBasedUserService service = new MySqlBasedUserService();

    public PasswordResetCheckHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameterMap().containsKey(HASH)) {
            String token = request.getParameter(HASH);

            int id = service.getUserIDFromPasswordReset(token);

            if (id != ID_NOT_FOUND) {
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
