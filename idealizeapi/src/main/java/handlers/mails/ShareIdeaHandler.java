package handlers.mails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import services.MySqlBasedUserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import static utils.Utils.HASH;
import static utils.Utils.IDEALIZE_SESSION;
import static utils.Utils.SESSION_NOT_FOUND;

public class ShareIdeaHandler extends HttpServlet
{
    MySqlBasedUserService service = new MySqlBasedUserService();

    public ShareIdeaHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();

        if(cookies != null)
        {

            for(Cookie cookie : cookies)
            {
                if(cookie.getName().equals(IDEALIZE_SESSION))
                {
                    String token = cookie.getValue();

                    String username = service.getUsernameFromSession(token);

                    if(!username.equals(SESSION_NOT_FOUND))
                    {

                        StringBuffer sb = new StringBuffer();
                        String line = null;
                        try {
                            BufferedReader reader = request.getReader();
                            while ((line = reader.readLine()) != null)
                                sb.append(line);
                        } catch (Exception e) {
                            System.out.println("Error reading JSON request");
                            e.printStackTrace();
                        }

                        System.out.println(sb.toString());

                        try {
                            JSONObject jsonObject =  new JSONObject(sb.toString());

                            int id = Integer.parseInt(jsonObject.getString("id"));
                            JSONArray emails = jsonObject.getJSONArray("emails");

                            boolean res = true;

                            for(int i = 0; i < emails.length(); i++)
                            {
                                res = res && service.shareIdea(id, emails.getString(i));
                            }

                            if(res)
                            {
                                response.setStatus(HttpServletResponse.SC_OK);
                            }
                            else response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

                        } catch (JSONException e) {

                            e.printStackTrace();
                            throw new IOException("Error parsing JSON request string");
                        }

                    }
                    else
                    {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }

                    return;
                }
            }
        }
        else
        {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

        System.out.println("Invalid user");
    }
}
