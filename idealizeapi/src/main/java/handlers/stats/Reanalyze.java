package handlers.stats;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import services.MySqlBasedUserService;
import services.TextRankService;

import javax.net.ssl.*;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.*;

import static utils.Configs.INDEXES_URL;
import static utils.Configs.TRENDS_URL;
import static utils.Utils.*;

public class Reanalyze extends HttpServlet {

    private TextRankService textRankService;

    private MySqlBasedUserService service = new MySqlBasedUserService();

    public Reanalyze(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cookie[] cookies = request.getCookies();

        if(cookies != null)
        {
            for(Cookie cookie : cookies)
            {
                if(cookie.getName().equals(IDEALIZE_SESSION))
                {
                    String token = cookie.getValue();

                    String username = service.getUsernameFromSession(token);

                    if(!username.equals(SESSION_NOT_FOUND))
                    {
                        StringBuffer sb = new StringBuffer();
                        String line = null;
                        try {
                            BufferedReader reader = request.getReader();
                            while ((line = reader.readLine()) != null)
                                sb.append(line);
                        } catch (Exception e) {
                            System.out.println("Error reading JSON request");
                            e.printStackTrace();
                        }

                        System.out.println(sb.toString());

                        try {
                            JSONObject jsonObject =  new JSONObject(sb.toString());

                            String title = jsonObject.getString("title");
                            String synopsis = jsonObject.getString("synopsis");
                            String keyword = jsonObject.getString("main_keyword");
                            JSONArray keywords = jsonObject.getJSONArray("other_keywords");
                            String country = jsonObject.getString("country");
                            String translate = String.valueOf(jsonObject.getInt("translation"));

                            try{

                                textRankService = new TextRankService(synopsis);
                                final Map<String, Double> result = textRankService.getMap();

                                if(result.isEmpty())
                                {
                                    response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                                }
                                else
                                {
                                    Set<String> res = result.keySet();

                                    String resultTrends = getTrends(res, country, translate);

                                    String compactTrends = getCompactTrends(resultTrends);

                                    if(compactTrends == null)
                                    {
                                        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                                    }
                                    else
                                    {
                                        String indexResults = getIndexes(compactTrends, result);

                                        JSONObject obj = new JSONObject();

                                        obj.put(TRENDS, compactTrends);
                                        obj.put(INDEXES, indexResults);

                                        int idauthor = service.getUserID(username);

                                        int lastID = service.saveIdea(title, idauthor, synopsis, keyword, obj.toString());

                                        if(lastID > -1)
                                        {
                                            for(int i = 0; i < keywords.length(); i++)
                                            {
                                                service.createIdeaKeyword(lastID, keywords.getString(i));
                                            }

                                            PrintWriter out = response.getWriter();

                                            out.println("{\"id\": " + lastID + "}");

                                            response.setStatus(HttpServletResponse.SC_OK);
                                        }
                                        else response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                    }
                                }

                            }catch(Exception e){
                                e.printStackTrace();
                                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                            throw new IOException("Error parsing JSON request string");
                        }

                    }
                    else
                    {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }

                    return;
                }
            }
        }

        System.out.println("Invalid user");
    }
}
