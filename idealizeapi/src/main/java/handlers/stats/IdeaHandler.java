package handlers.stats;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import services.MySqlBasedUserService;

import javax.servlet.ServletException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import static utils.Utils.*;

public class IdeaHandler extends HttpServlet
{
    MySqlBasedUserService service = new MySqlBasedUserService();

    public IdeaHandler(MySqlBasedUserService service)
    {
        this.service = service;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        Cookie[] cookies = request.getCookies();

        if(cookies != null)
        {

            for(Cookie cookie : cookies)
            {
                if(cookie.getName().equals(IDEALIZE_SESSION))
                {
                    String token = cookie.getValue();

                    String username = service.getUsernameFromSession(token);

                    if(!username.equals(SESSION_NOT_FOUND))
                    {

                        StringBuffer sb = new StringBuffer();
                        String line = null;
                        try {
                            BufferedReader reader = request.getReader();
                            while ((line = reader.readLine()) != null)
                                sb.append(line);
                        } catch (Exception e) {
                            System.out.println("Error reading JSON request");
                            e.printStackTrace();
                        }

                        System.out.println(sb.toString());

                        try {
                            JSONObject jsonObject =  new JSONObject(sb.toString());

                            String title = jsonObject.getString("title");
                            String synopsis = jsonObject.getString("synopsis");
                            String keyword = jsonObject.getString("main_keyword");
                            JSONArray keywords = jsonObject.getJSONArray("other_keywords");
                            String trendsString = jsonObject.getString("trends");
                            String country = jsonObject.getString("country");

                            int idauthor = service.getUserID(username);

                            int lastID;
                            if(country.isEmpty())
                            {
                                lastID = service.saveIdea(title, idauthor, synopsis, keyword, trendsString);
                            }
                            else
                            {
                                lastID = service.saveIdea(title, idauthor, synopsis, keyword, trendsString, country);
                            }

                            if(lastID > -1)
                            {
                                for(int i = 0; i < keywords.length(); i++)
                                {
                                    service.createIdeaKeyword(lastID, keywords.getString(i));
                                }

                                PrintWriter out = response.getWriter();

                                out.println("{\"id\": " + lastID + "}");

                                response.setStatus(HttpServletResponse.SC_OK);
                            }
                            else response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

                        } catch (JSONException e) {

                            e.printStackTrace();
                            throw new IOException("Error parsing JSON request string");
                        }

                    }
                    else
                    {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }

                    return;
                }
            }
        }
        else
        {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

        System.out.println("Invalid user");

    }

    //curl test: curl -v --cookie "IDEALIZE_SESSION=xyz" -k -X GET "https://localhost:8443/idea?ideaid=x"
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        Cookie[] cookies = request.getCookies();

        if(cookies != null)
        {

            for(Cookie cookie : cookies)
            {
                if(cookie.getName().equals(IDEALIZE_SESSION))
                {
                    String token = cookie.getValue();

                    String username = service.getUsernameFromSession(token);

                    if(!username.equals(SESSION_NOT_FOUND))
                    {

                        if(request.getParameterMap().containsKey(IDEAID))
                        {

                            try{
                                int id = Integer.parseInt(request.getParameter(IDEAID));
                                int userID = service.getUserID(username);

                                if(!service.userHasAccess(userID, id))
                                {
                                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                }
                                else
                                {
                                    String idea = service.getIdea(id);

                                    if(idea != null) {

                                        PrintWriter out = response.getWriter();
                                        out.println(idea);

                                        response.setStatus(HttpServletResponse.SC_OK);
                                    }
                                    else response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                                }

                            }catch (NumberFormatException e){
                                System.out.println("Idea id must be an integer");
                                e.printStackTrace();
                                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                            }

                        }
                        else
                        {
                            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        }

                        return;

                    }
                    else
                    {
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }

                    return;
                }
            }
        }
        else
        {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

        System.out.println("Invalid user");
    }

}
