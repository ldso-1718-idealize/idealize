package handlers.stats;

import org.json.JSONObject;
import services.TextRankService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

import static utils.Utils.*;

public class GetStats extends HttpServlet {

    private TextRankService textRankService;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(request.getParameterMap().containsKey(TEXTRANKQUERY)) {

            String text = request.getParameter(TEXTRANKQUERY);

            String country = "";

            if(request.getParameterMap().containsKey(COUNTRY))
            {
                country = request.getParameter(COUNTRY);
            }

            String translate = "0";

            if(request.getParameterMap().containsKey(TRANSLATION))
            {
                translate = request.getParameter(TRANSLATION);

            }

            try{

                textRankService = new TextRankService(text);
                Map<String, Double> result = textRankService.getMap();

                if(result.isEmpty())
                {
                    response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                }
                else
                {
                    PrintWriter out = response.getWriter();

                    Set<String> res = result.keySet();

                    String resultTrends = getTrends(res, country, translate);

                    String compactTrends = getCompactTrends(resultTrends);

                    if(compactTrends == null)
                    {
                        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                    }
                    else
                    {
                        String indexResults = getIndexes(compactTrends, result);

                        JSONObject obj = new JSONObject();

                        obj.put(TRENDS, compactTrends);
                        obj.put(INDEXES, indexResults);

                        response.setStatus(HttpServletResponse.SC_OK);

                        out.println(obj.toString());
                    }
                }

            }catch(Exception e){
                e.printStackTrace();
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
        else
        {
            System.out.println("no textrank");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
